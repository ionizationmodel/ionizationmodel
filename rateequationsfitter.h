//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef RATEEQUATIONSFITTER
#define RATEEQUATIONSFITTER

#include <rateEquationsProfile.h>
#include <voigtprofile.h>
#ifdef __linux__
//Ubuntu specific eigen include
#include <Eigen/Dense>
#include <eigen3/unsupported/Eigen/NonLinearOptimization>
#include <eigen3/unsupported/Eigen/AutoDiff>
#endif
#ifdef _WIN32 || _WIN64
//Windows specific
#include <Eigen/NonLinearOptimization>
#endif
#include <valuecontainerclass.h>
#include <iostream>
#include <string>
#include <sstream>
#include <memory>
#include <fittersystem.h>
// Generic functor
template<typename _Scalar, int NX=Eigen::Dynamic, int NY=Eigen::Dynamic>
struct rateFunctor
{
    typedef _Scalar Scalar;
    enum {
        InputsAtCompileTime = NX,
        ValuesAtCompileTime = NY
    };
    typedef Eigen::Matrix<Scalar,InputsAtCompileTime,1> InputType;
    typedef Eigen::Matrix<Scalar,ValuesAtCompileTime,1> ValueType;
    typedef Eigen::Matrix<Scalar,InputsAtCompileTime,ValuesAtCompileTime> JacobianType;

    int m_inputs, m_values;

    rateFunctor() : m_inputs(InputsAtCompileTime), m_values(ValuesAtCompileTime) {}
    rateFunctor(int inputs, int values) : m_inputs(inputs), m_values(values) {}

    int inputs() const { return m_inputs; }
    int values() const { return m_values; }
};
struct rateEquationsFitter_functor : rateFunctor<double>
{
    int numberOfVariables;
    int numberOfDataPoints;
    valueContainerClass *valueContainer;
    hyperfineCalculations *hypFi;
    std::unique_ptr<voigtProfile> AVoigt;
    Eigen::MatrixXd fjac_tmp;
    std::vector<bool> xLimitLocks;                             //Limit locks
    std::vector<double> frequencyRange;
    std::vector<double> measuredProfile;
    std::vector<std::vector<double>> alphasAndBetas;
    std::vector<int> transitionIds;
    std::vector<double> rateOuput;
    int numberOfTransitions;

    std::vector<std::unique_ptr<fitterSystem>> fitterSystems;

    //automatic differentiation
    typedef Eigen::AutoDiffScalar<Eigen::Matrix<Scalar,Eigen::Dynamic,1>> ADS;
    typedef Eigen::Matrix<ADS, Eigen::Dynamic, 1> VectorXad;

    //Set:
    void setNumberOfVariables(int numberOfVariable_v){
        numberOfVariables=numberOfVariable_v;
        xLimitLocks.resize(this->inputs());
    }
    void setNumberOfDataPoints(int numberOfDataPoints_v){
        numberOfDataPoints=numberOfDataPoints_v;
    }
    void setValueContainer(valueContainerClass *valueContainer_p){
        valueContainer=valueContainer_p;
    }
    void setHyperfineCalculations(hyperfineCalculations *hypFi_p){
        hypFi=hypFi_p;
    }
    void initialiseFitterSystems(){
        fitterSystems.clear();
        fitterSystems.resize(this->values());
        fjac_tmp.resize(this->values(), this->inputs());

    }
    void setFitterSystems(int idx, std::unique_ptr<fitterSystem> system_p){
        fitterSystems[idx]=std::move(system_p);
    }
    std::vector<double> getRateoutput(){
        return rateOuput;
    }

    void init(){
        AVoigt = std::make_unique<voigtProfile>(valueContainer);
        frequencyRange=valueContainer->getFitFrequencyRange();
        measuredProfile=valueContainer->getFitProfile();
        numberOfTransitions=valueContainer->getNumberOfAllowedTransitions();
        alphasAndBetas=hypFi->getAlphasAndBetas(numberOfTransitions);
        for(int t=0;t<numberOfTransitions;t++){
            transitionIds.push_back(t);
        }
    }
    //Get:
    int getLimited(){
        int counter=0;
        for(int i=0; i<this->inputs(); i++){
            if(xLimitLocks[i]){
                counter++;
            }
        }
        return counter;
    }

    //A functor for the Voigt functions and its derivatives
    rateEquationsFitter_functor(void) : rateFunctor<double>(this->inputs(), this->values()) {}
    int operator()(Eigen::VectorXd &x, Eigen::VectorXd &fvec)
    {
        std::vector<std::pair<double, double> > xLimits;      //Fit limits
        xLimits=valueContainer->getModel_parameterLimits();

        //Af/Ai and Bf/Bi ratio locks
        bool afAi=valueContainer->getAfAiRatioLock();
        bool bfBi=valueContainer->getBfBiRatioLock();

        if(valueContainer->getEnforceLimits()){

            //Check if the input values are beyond their limits
            for(int i=0; i<this->inputs(); i++){
                double xValue=x[i];
                double lowerLimit=xLimits[i].first;
                double upperLimit=xLimits[i].second;
                if(xValue<=lowerLimit){
                    x[i]=lowerLimit;
                    xLimitLocks[i]=true;
                }
                if(xValue>=upperLimit){
                    x[i]=upperLimit;
                    xLimitLocks[i]=true;
                }
                else{
                    xLimitLocks[i]=false;
                }
            }

            //set Af/Ai and Bf/Bi ratio locks

            if(afAi){
                x[2]=x[0]*valueContainer->getAfAiRatio();
            }
            if(bfBi){
                x[3]=x[1]*valueContainer->getBfBiRatio();
            }
        }
        fvec.setZero();
        fvec.resize(numberOfDataPoints);
        fjac_tmp.setZero();
        fjac_tmp.resize(this->values(),this->inputs());
        rateOuput.clear();
        rateOuput.resize(numberOfDataPoints);
        runRateEquations(x, fvec,fjac_tmp, measuredProfile, rateOuput, false);

        return 0;
    }

    int df(const Eigen::VectorXd &x, Eigen::MatrixXd &fjac)
    {
        //["initialA"]=x[0];
        //["initialB"]=x[1];
        //["finalA"]=x[2];
        //["finalB"]=x[3];
        //["CoG"]=x[4];
        //["gaussianWidth"]=x[5];
        //["lorentzianWidth"]=x[6];
        //["intensityScaler"]=x[7];
        //["backgroundScaler"]=x[8];
        //["laserIntensity"]=x[9];

        /////////////
        std::vector<bool> xLocks;                             //Fit locks
        xLocks=valueContainer->getModel_parameterLocks();

        //Af/Ai and Bf/Bi ratio locks
        bool afAi=valueContainer->getAfAiRatioLock();
        bool bfBi=valueContainer->getBfBiRatioLock();
        for(int idx=0; idx<this->values(); idx++){
            //Set derivative zero for a given parameter if it is either locked or limited
            for(int i=0; i<this->inputs(); i++){
                fjac(idx,i)=fjac_tmp(idx,i);
                //Apply Locks
                if(xLocks[i] || xLimitLocks[i]){
                    fjac(idx,i)=0;
                }
            }
            //set Af/Ai and Bf/Bi ratio locks
            if(afAi){
                fjac(idx,2)=0;
            }
            if(bfBi){
                fjac(idx,3)=0;
            }
        }
        return 0;
    }
    double runRateEquations(const Eigen::VectorXd &x_r, Eigen::Matrix<Scalar,Eigen::Dynamic,1> &fvec_r,Eigen::MatrixXd &fjac_r, const std::vector<double> &measuredProfile_r,std::vector<double> &output_r, bool derivative){
//#pragma omp parallel for schedule(dynamic)
        for(int idx=0;idx<this->values();idx++){
            double mP;
            double sigma;
            fitterSystems[idx]->cleanIDASSystem();
            fitterSystems[idx]->setParametersFromFitter(x_r);
            fitterSystems[idx]->setupIDASSystem();
            fitterSystems[idx]->runIDASSystem();
            mP=measuredProfile_r[idx];
            if(mP!=0){
                sigma=sqrt(mP);
            }
            else{
                sigma=1.0;
            }

            double valueAtpoint=fitterSystems[idx]->getIonizedStatePopulation()*x_r[7]+x_r[8];
            fvec_r[idx]=(valueAtpoint- mP)/sigma;
            output_r[idx]=valueAtpoint;
            //Get sensitivities(Derivatives)
            fjac_r.row(idx)=fitterSystems[idx]->getSensitivityEigenResized();
            //Set derivatives for BG in and intensity
            fjac_r.row(idx)(7)=fitterSystems[idx]->getIonizedStatePopulation()/sigma;
            fjac_r.row(idx)(8)=1/sigma;
        }
    }
    int inputs() const { return numberOfVariables; }
    int values() const { return numberOfDataPoints; }
};

class rateEquationsFitter
{
public:
    explicit rateEquationsFitter(valueContainerClass *valueContainer_p, hyperfineCalculations *hypFi_p, rateEquationsProfile *rateqProfile_p)
        :valueContainer(valueContainer_p), hypFi(hypFi_p), rateqProfile(rateqProfile_p){
    }
    //automatic differentiation
    typedef Eigen::AutoDiffScalar<Eigen::Matrix<realtype,Eigen::Dynamic,1>> ADS;
    typedef Eigen::Matrix<ADS, Eigen::Dynamic, 1> VectorXad;

    double maxwellBoltzmannStatistics(double deltaNu, valueContainerClass *valueContainer_r){
        double deltaE=deltaNu*valueContainer_r->hBar*2*valueContainer_r->pi;
        double ratio=exp(-deltaE/(valueContainer_r->k*valueContainer_r->getModel_initialPopulationTemperature()));
        return ratio;
    }
    void generateInitialSystemConditions(valueContainerClass *valueContainer_r,
                                    hyperfineCalculations *hyperFine_r,
                                    std::vector<double> &initialConditions_r,
                                    double &totalPopulation_r,
                                    std::vector<double> &absoluteTolerances_r,
                                    std::vector<double> &initialTransitionRates_r){
        totalPopulation_r=0;
        absoluteTolerances_r.clear();
        initialTransitionRates_r.clear();
        initialConditions_r.clear();
        double atol_IDA=valueContainer_r->getSundials_absoluteTolerance();
        //Set intial conditions(state vectors) vectors for each frequencypoint
        int noOfFinalFStates=hyperFine_r->getNumberOfFinalFStates();
        int noOfInitialFStates=hyperFine_r->getNumberOfInitialFStates();
        int numberOfEquations=noOfFinalFStates+noOfInitialFStates+1;
        double A_v=valueContainer_r->getInitialA();
        double B_v=valueContainer_r->getInitialB();
        initialConditions_r.resize(numberOfEquations);
        //delta nu for the lowests of the initial hyperfine states
        //The other states will be compared to this
        double iNu=hyperFine_r->getHyperFineDeltaNu(A_v, B_v, 0)*1E6;
        //foreach (QString stateId, initialStateIds.keys()) {
        int t=0;
        for(t; t<hyperFine_r->getNumberOfInitialFStates();t++){
            double deltaNu=hyperFine_r->getHyperFineDeltaNu(A_v, B_v, t)*1E6-iNu;
            double ratio=maxwellBoltzmannStatistics(deltaNu, valueContainer_r);
            initialConditions_r[t]=ratio*valueContainer_r->getModel_initialPopulationScaler();
            initialTransitionRates_r.push_back(0);
            absoluteTolerances_r.push_back(atol_IDA);
            totalPopulation_r+=ratio*valueContainer_r->getModel_initialPopulationScaler();
        }
        for(int p=t; p<hyperFine_r->getNumberOfFinalFStates()+t;p++) {
            initialConditions_r[p]=0;
            initialTransitionRates_r.push_back(0);
            absoluteTolerances_r.push_back(atol_IDA);
            totalPopulation_r+=0;
        }
        initialConditions_r[numberOfEquations-1]=0;
        initialTransitionRates_r.push_back(0);
        absoluteTolerances_r.push_back(atol_IDA);
        totalPopulation_r+=0;
    }

    void generateFitterSystems(const Eigen::VectorXd &xValues_r,
                               const std::vector<Eigen::VectorXd> &xSubValues_r,
                               const VectorXad &xADSValues_r,
                               const std::vector<VectorXad> &xADSSubValues_r,
                               valueContainerClass *valueContainer_r,
                               hyperfineCalculations *hyperFine_r,
                               const std::vector<double> &initialConditions_r,
                               const std::vector<double> &absoluteTolerances_r,
                               const std::vector<double> initialTransitionRates_r,
                               const double &totalPopulation_r,
                               rateEquationsFitter_functor &functor_r){
        int m=valueContainer_r->getNumberOfDataPoints();
        int p=valueContainer_r->getNumberOfAllowedTransitions();
        int n=valueContainer_r->getModel_numberOfStaticVariables()+p;
        int reducedn=n-2;                                               //Sensitivities for the rateq solver, intensity an bg scalers are omitted
        std::vector<double> frequencyRange=valueContainer_r->getFitFrequencyRange();
        int counter=0;
        for(const auto &freq: frequencyRange){
           std::unique_ptr<fitterSystem> system=std::make_unique<fitterSystem>();
           system->data->numberOfParameters=reducedn;
           system->data->numberOfTransitions=p;
           system->data->numberOfEquations=hyperFine_r->getNumberOfFinalFStates()+hyperFine_r->getNumberOfInitialFStates()+1;
           system->data->numberOfSensitivities=reducedn;
           system->data->numberOfStates=hyperFine_r->getNumberOfFinalFStates()+hyperFine_r->getNumberOfInitialFStates()+1;
           system->data->laserFrequency=freq;
           system->data->laserIntensities.push_back(valueContainer_r->getModel_laserIntensity());
           system->data->laserIntensities.push_back(valueContainer_r->getI_nr());
           system->data->ACoefficients.push_back(valueContainer_r->getACoefficient_1());
           system->data->spontaneousDecayRateVectors.push_back(valueContainer_r->getOrderedSpontaneousDecayRates_1());
           system->data->ionizingTransitionRate=calculateNumberOfIonizingStepTransitions(valueContainer_r);
           system->data->measuredValue=valueContainer_r->getMeasuredProfileValues()[counter];
           system->data->initialTransitionRates=initialTransitionRates_r;
           system->data->xValues=xValues_r;
           system->data->xSubValues=xSubValues_r;
           system->data->xADSValues=xADSValues_r;
           system->data->xADSSubValues=xADSSubValues_r;
           std::vector<double> step1Properties;
           std::vector<double> ionizingStepProperties;
           step1Properties.push_back(valueContainer_r->getLaserPulseWidth()*1E-9);
           step1Properties.push_back(0.0);
           ionizingStepProperties.push_back(valueContainer_r->getPW_nr()*1E-9);
           ionizingStepProperties.push_back(valueContainer_r->getTimeOffset()*1E-9);
           system->data->laserPulseProperties.push_back(step1Properties);
           system->data->laserPulseProperties.push_back(ionizingStepProperties);
           system->data->initialStateIds.push_back(hyperFine_r->getInitialStateIds());
           system->data->finalStateIds.push_back(hyperFine_r->getFinalStateIds());
           system->data->numberOfInitialStates.push_back(hyperFine_r->getNumberOfInitialFStates());
           system->data->numberOfFinalStates.push_back(hyperFine_r->getNumberOfFinalFStates());
           system->data->orderedAllowedStates.push_back(hyperFine_r->getOrderedAllowedStates());
           system->data->ionizedStateId=hyperFine_r->getIonizedStateId();
           system->data->alphasAndBetas=hyperFine_r->getAlphasAndBetas(p);
           system->data->integrationTimeScaler=valueContainer_r->getSundials_integrationTimeScaler();
           system->data->initialConditions=initialConditions_r;
           system->data->setConstraints=valueContainer_r->getSundials_useConstraints();
           system->data->setUserJacobian=valueContainer_r->getSundials_useJacobian();
           system->data->absoluteTolerances=absoluteTolerances_r;
           system->data->relativeTolerance=valueContainer_r->getSundials_relativeTolerance();
           system->data->totalPopulation=totalPopulation_r;
           system->data->timePoints=valueContainer_r->getSundials_TimePoints();
           system->data->setNonlinConvCoef=valueContainer_r->getSundials_nlConvCoeff();
           system->data->maxNumSteps=valueContainer_r->getSundials_maximumNumberOfIntegrationStep();
           system->data->reallocP(n);
           functor_r.setFitterSystems(counter, std::move(system));
           counter++;
        }
    }
    //same as above but for the ionizing non-resonant transition
    //Need cleanup to get this removed from here
    double calculateNumberOfIonizingStepTransitions(valueContainerClass *valueContainer_r){
        double laserFreq=(valueContainer_r->c/(valueContainer_r->getWl_nr()*1E-9));                                                                     //[Hz]
        double photonEnergy=laserFreq*valueContainer_r->hBar*2*valueContainer_r->pi;                                                                                    //[J]
        double photonIntensity=valueContainer_r->getI_nr()*(1E-6/photonEnergy);                                                       //[photons/cm^2]
        double transitionsPerPulse_nr=valueContainer_r->getCS_nr()*photonIntensity;                                                   //[1]
        return transitionsPerPulse_nr;
    }
    void setFitResults(const Eigen::VectorXd &x, std::vector<std::string> hashOrder_v, int n_v, int p_v){
        QHash<QString, double> bestFitParametersHash;
        QHash<QString, double> fittedIntensities;
        bestFitParametersHash["initialA"]=x[0];
        bestFitParametersHash["initialB"]=x[1];
        bestFitParametersHash["finalA"]=x[2];
        bestFitParametersHash["finalB"]=x[3];
        bestFitParametersHash["CoG"]=x[4]/1E9;
        bestFitParametersHash["gaussianWidth"]=x[5];
        bestFitParametersHash["lorentzianWidth"]=x[6];
        bestFitParametersHash["intensityScaler"]=x[7];
        bestFitParametersHash["backgroundScaler"]=x[8];
        bestFitParametersHash["laserIntensity"]=x[9];
        for(int i=0;i<p_v; i++){
            fittedIntensities[QString::fromStdString(hashOrder_v[i])]=x[(n_v-p_v)+i];
        }
        valueContainer->setModel_fittedProfileParameters(bestFitParametersHash);
        valueContainer->setFittedFreeIntensities(fittedIntensities);
    }

    void fit()
    {
        //["initialA"]=x[0];
        //["initialB"]=x[1];
        //["finalA"]=x[2];
        //["finalB"]=x[3];
        //["CoG"]=x[4];
        //["gaussianWidth"]=x[5];
        //["lorentzianWidth"]=x[6];
        //["intensityScaler"]=x[7];
        //["backgroundScaler"]=x[8;
        QStringList parameterNames;
        parameterNames.append("Initial A: ");
        parameterNames.append("Initial B: ");
        parameterNames.append("Final A: ");
        parameterNames.append("Final B: ");
        parameterNames.append("Center of gravity: ");
        parameterNames.append("Gaussian Width: ");
        parameterNames.append("Lorentzian Width: ");
        parameterNames.append("Intensity Scaler: ");
        parameterNames.append("Background Scaler: ");
        parameterNames.append("Laser Intensity: ");

        QStringList results;
        QString fitterStatus;
        QString chiSquared;

        int m=valueContainer->getNumberOfDataPoints();
        int p=valueContainer->getNumberOfAllowedTransitions();
        int n=valueContainer->getModel_numberOfStaticVariables()+p;
        //Sensitivities for the rateq solver, intensity an bg scalers are omitted but sensitivities for the
        //Relative intensities are still in
        int reducedn=n-2;
        int subN=reducedn-p+1;
        Eigen::LevenbergMarquardtSpace::Status status;
        Eigen::VectorXd x(n);                                           //Fit parameters
        Eigen::VectorXd reducedx(reducedn);                             //Fit parameters   without intensity and bg scalers
        std::vector<Eigen::VectorXd> xSubValues(p);              //Fit parameters relevant to rate-equations solver
        std::vector<double> xValues(n);                                 //Initial Values
        VectorXad xADSValues(n);
        std::vector<VectorXad> xADSSubValues(p);
        double fTol=valueContainer->getFtol();
        double xTol=valueContainer->getXtol();

        double chi, reducedChiSquared, reducedChi;
        int dof;

        std::vector<double> relativeIntensities;
        std::vector<std::string> hashOrder;
        //Fixed parameters presents in every fit
        xValues=valueContainer->getModel_parameterValues();
        x[0]=xValues[0];
        x[1]=xValues[1];
        x[2]=xValues[2];
        x[3]=xValues[3];
        x[4]=xValues[4];
        x[5]=xValues[5];
        x[6]=xValues[6];
        x[7]=xValues[7];
        x[8]=xValues[8];
        x[9]=xValues[9];
        reducedx[0]=xValues[0];
        reducedx[1]=xValues[1];
        reducedx[2]=xValues[2];
        reducedx[3]=xValues[3];
        reducedx[4]=xValues[4];
        reducedx[5]=xValues[5];
        reducedx[6]=xValues[6];
        reducedx[7]=xValues[9];
        xADSValues(0)=ADS(xValues[0], n,0);
        xADSValues(1)=ADS(xValues[1], n,1);
        xADSValues(2)=ADS(xValues[2], n,2);
        xADSValues(3)=ADS(xValues[3], n,3);
        xADSValues(4)=ADS(xValues[4], n,4);
        xADSValues(5)=ADS(xValues[5], n,5);
        xADSValues(6)=ADS(xValues[6], n,6);
        xADSValues(7)=ADS(xValues[7], n,7);
        xADSValues(8)=ADS(xValues[8], n,8);
        xADSValues(9)=ADS(xValues[9], n,9);
        //Per peak parameters, e.g. for free intensity
        relativeIntensities=valueContainer->getOrderedRelativeTransitionIntensities();
        hashOrder=valueContainer->getHashOrder();
        for(int i=0; i<p; i++){
            parameterNames.append(QString::fromStdString(hashOrder[i]));
            x[(n-p)+i]=relativeIntensities[i];
            reducedx[(reducedn-p)+i]=relativeIntensities[i];
            xADSValues((n-p)+i)=ADS(relativeIntensities[i], n,(n-p)+i);
            //Create Sub ADS matrices for each transition
            //This matrices  omit values for the intensity and background scalers
            //as well as other relative intensites other than the one related to the transition
            VectorXad xADSSubValues_tmp(subN);
            xADSSubValues_tmp(0)=ADS(xValues[0], subN,0);
            xADSSubValues_tmp(1)=ADS(xValues[1], subN,1);
            xADSSubValues_tmp(2)=ADS(xValues[2], subN,2);
            xADSSubValues_tmp(3)=ADS(xValues[3], subN,3);
            xADSSubValues_tmp(4)=ADS(xValues[4], subN,4);
            xADSSubValues_tmp(5)=ADS(xValues[5], subN,5);
            xADSSubValues_tmp(6)=ADS(xValues[6], subN,6);
            xADSSubValues_tmp(7)=ADS(xValues[9], subN,7);
            xADSSubValues_tmp(8)=ADS(relativeIntensities[i], subN,8);
            xADSSubValues[i]=xADSSubValues_tmp;
            //Create xSub vectors for each transitions on the same principle
            Eigen::VectorXd xSubValues_tmp(subN);
            xSubValues_tmp[0]=xValues[0];
            xSubValues_tmp[1]=xValues[1];
            xSubValues_tmp[2]=xValues[2];
            xSubValues_tmp[3]=xValues[3];
            xSubValues_tmp[4]=xValues[4];
            xSubValues_tmp[5]=xValues[5];
            xSubValues_tmp[6]=xValues[6];
            xSubValues_tmp[7]=xValues[9];
            xSubValues_tmp[8]=relativeIntensities[i];
            xSubValues[i]=xSubValues_tmp;
        }

        //Analytical/autodifferentiate derivative
        rateEquationsFitter_functor functor;
        functor.setValueContainer(valueContainer);
        functor.setHyperfineCalculations(hypFi);
        functor.setNumberOfDataPoints(m);
        functor.setNumberOfVariables(n);
        functor.init();

        //set-up rate equations solver
        functor.initialiseFitterSystems();
        generateInitialSystemConditions(valueContainer,
                                        hypFi,
                                        initialConditions_r,
                                        totalPopulation_r,
                                        absoluteTolerances_r,
                                        initialTransitionRates_r);
        generateFitterSystems(reducedx,
                              xSubValues,
                              xADSValues,
                              xADSSubValues,
                              valueContainer,
                              hypFi,
                              initialConditions_r,
                              absoluteTolerances_r,
                              initialTransitionRates_r,
                              totalPopulation_r,
                              functor);
        Eigen::LevenbergMarquardt<rateEquationsFitter_functor> lm(functor);
        lm.parameters.ftol = fTol;
        lm.parameters.xtol = xTol;
        lm.parameters.maxfev =valueContainer->getMaxIter(); // Max iterations
        status  = lm.minimize(x);
        // check norm and covariance
        chi = lm.fvec.norm();
        //Leave the error estimation for now
        dof=m-valueContainer->getModel_fitNumberOfLockedParameters()-functor.getLimited();
        reducedChiSquared = chi*chi/(dof);
        reducedChi=sqrt(reducedChiSquared);
        Eigen::internal::covar(lm.fjac, lm.permutation.indices(), fTol);
        //Covariant matrix
        Eigen::MatrixXd cov;
        //Scale Variance-Covariance -matrix with the reduced chi-squared
        cov =  reducedChi*lm.fjac;
        fitterStatus.append("Iterations: ");
        fitterStatus.append(QString::number(lm.iter));
        fitterStatus.append(" Return code: ");
        fitterStatus.append(QString::number(status));

        chiSquared.append("Reduced chi-squared: ");
        chiSquared.append(QString::number(reducedChiSquared));
        chiSquared.append(" DoF: ");
        chiSquared.append(QString::number(dof));

        results.append("Best fit parameters (in MHZ where applicable)");
        results.append(fitterStatus);
        results.append(chiSquared);

        for(int i=0; i<n;i++){
                double value=x[i];
                double error=sqrt(lm.fjac(i,i));
                //std::cout<<value<<" value "<<error<<" error "<<std::endl;
                QString parameterOutput;
                parameterOutput.append(parameterNames.at(i));
                parameterOutput.append(QString::number(value));
                parameterOutput.append(" +- ");
                parameterOutput.append(QString::number(error));
                results.append(parameterOutput);
        }
        valueContainer->setFitLog(results);
        setFitResults(x,hashOrder, n, p);

        //Set results graph to plot


        QPair<QVector<double>, QVector<double> > plotData;
        plotData.first=QVector<double>::fromStdVector(valueContainer->getFitFrequencyRange());
        plotData.second=QVector<double>::fromStdVector(functor.getRateoutput());
        rateqProfile->plotDataReady_SUNDIALS(plotData);
    }
private:
    valueContainerClass *valueContainer;
    hyperfineCalculations *hypFi;
    voigtProfile *AVoigt;
    rateEquationsProfile *rateqProfile;
    std::vector<std::unique_ptr<fitterSystem>> fitterSystems;
    std::vector<double> initialConditions_r;
    double totalPopulation_r;
    std::vector<double> absoluteTolerances_r;
    std::vector<double> initialTransitionRates_r;

};

#endif // RATEEQUATIONSFITTER

