//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef RELATIVEPROFILE_H
#define RELATIVEPROFILE_H

#include <QObject>
#include <voigtprofile.h>
#include <hyperfinecalculations.h>
#include <valuecontainerclass.h>
#include <relativeintensityfitter.h>
#include <unordered_map>
#include <QDebug>
#include <memory>
class relativeProfile : public QObject
{
    Q_OBJECT
public:
    explicit relativeProfile(QObject *parent, hyperfineCalculations *hypFi_p,
                             valueContainerClass *valueContainer_p,
                             std::shared_ptr<relativeIntensityFitter> &relativeFitter_p)
        :hypFi(hypFi_p), valueContainer(valueContainer_p), relativefitter(relativeFitter_p){
        this->setParent(parent);
        c= valueContainer->c;
        pi =valueContainer->pi;
        hBar=valueContainer->hBar;
        AVoigt= std::make_unique<voigtProfile>(valueContainer);
        calculateProfile();
    }
    ~relativeProfile(){
    }
    double getNumberOfTransitions_nr(){
        return calculateNumberOfIonizingStepTransitions();
    }

    void getRelativeTransitionNumbers(std::vector<std::vector<double>> &outputVector_individualTransitions_r, std::vector<double> &frequencyRange_r){
        int numberOfTransitions=hypFi->getNumberOfAllowedTransitions(0);
        std::vector<double> orderedTransitionIntensities=valueContainer->getOrderedRelativeTransitionIntensities();
        std::vector<double> orderedRelativeTransitionIntensities=hypFi->getOrderedRelativeTransitionIntensities(0);
        std::vector<double> orderedTransitionFreqeuencies=hypFi->getOrderedTransitionFrequencies(0);
        std::vector<double> parameters=valueContainer->getParameterValues();
        std::vector<std::vector<double>> alphasAndBetas=hypFi->getAlphasAndBetas(numberOfTransitions);
        calculateFrequencyRange(frequencyRange_r,orderedTransitionFreqeuencies,valueContainer->getMeasuredProfileRange());
        int numberOfDatapoints=frequencyRange_r.size();
        std::vector<double> inputVector;
        inputVector.reserve(parameters.size()+orderedTransitionIntensities.size());
        std::vector<double> measuredProfile;
        std::vector<int> transitionIds;
        outputVector_individualTransitions_r.clear();
        outputVector_individualTransitions_r.reserve(numberOfTransitions);
        outputVector_summedTransitions.clear();
        outputVector_summedTransitions.resize(numberOfDatapoints);
        for(int t=0;t<numberOfTransitions;t++){
            std::vector<double> transitionVector;
            transitionVector.reserve(numberOfDatapoints);
            outputVector_individualTransitions_r.push_back(transitionVector);
            transitionIds.push_back(t);
        }
        if(valueContainer->getFreeIntensities()){
            inputVector=parameters;
            inputVector.insert(inputVector.end(),orderedTransitionIntensities.begin(), orderedTransitionIntensities.end() );
        }
        else{
            inputVector=parameters;
            inputVector.insert(inputVector.end(),orderedRelativeTransitionIntensities.begin(), orderedRelativeTransitionIntensities.end() );
        }
        int numberOfParameters=inputVector.size();
        AVoigt->getVoigtProfile<double,std::vector<double>, std::vector<double>,std::vector<std::vector<double>>>(inputVector,
                                                                                                                  outputVector_summedTransitions,
                                                                                                                  outputVector_individualTransitions_r,
                                                                                                                  transitionIds,
                                                                                                                  frequencyRange_r,
                                                                                                                  measuredProfile,
                                                                                                                  alphasAndBetas,
                                                                                                                  false,
                                                                                                                  numberOfParameters);
    }

    std::vector< std::vector<double> > getIonizingTransitionNumberVector(){
        int numberOfDataPoints=frequencyRange.size();
        int numberOfTransitions=hypFi->getNumberOfFinalFStates();
        std::vector< std::vector<double>> ionStep;
        ionStep.reserve(numberOfDataPoints);
        double ionsNumb=getNumberOfTransitions_nr();
        for(int freqPoint; freqPoint<numberOfDataPoints; freqPoint++){
            std::vector<double> point;
            point.reserve(numberOfTransitions);
            for(int finalStateNo=0; finalStateNo<hypFi->getNumberOfFinalFStates(); finalStateNo++){
                point.push_back(ionsNumb);
            }
            ionStep.push_back(point);
        }
        return ionStep;
    }

    std::vector<std::vector<double>> getIndividualTransitions(){
        int numberOfDataPoints=frequencyRange.size();
        int numberOfTransitions=outputVector_individualTransitions.size();
        std::vector<std::vector<double>> transitionNumbersPerFrequencyPoint;
        transitionNumbersPerFrequencyPoint.reserve(numberOfDataPoints);
        for(int freqPoint; freqPoint<numberOfDataPoints; freqPoint++){
            std::vector<double> point;
            point.reserve(numberOfTransitions);
            for(const auto &trans: outputVector_individualTransitions){
                point.push_back(trans[freqPoint]);
            }
            transitionNumbersPerFrequencyPoint.push_back(point);
        }
        return transitionNumbersPerFrequencyPoint;
    }
public slots:
    void calculateProfile(){
        getRelativeTransitionNumbers(outputVector_individualTransitions, frequencyRange);
        fullRelativeProfilePair.first=frequencyRange;
        fullRelativeProfilePair.second=outputVector_summedTransitions;
        emit fullRelativeProfileReady(fullRelativeProfilePair);
        calculateSinglePeaks();
    }

    void calculateSinglePeaks(){
        if(valueContainer->getShowIndividualPeaksState()){
            parseSinglePeaks();
        }
        else{
            singleRelativePeaks.clear();
        }
        emit singleRelativePeaksReady(singleRelativePeaks);
    }

    void fitRelativeProfile(){
        if(!valueContainer->getMeasuredProfile().first.isEmpty()){
           relativefitter->fit();
        }
    }


signals:
    void fullRelativeProfileReady(std::pair<std::vector<double>, std::vector<double>> fullRelativeProfilePair_v);
    void singleRelativePeaksReady(std::unordered_map<std::string,std::pair<std::vector<double>,std::vector<double> > > singleRelativePeaks_v);

private:
    std::unique_ptr<voigtProfile> AVoigt;
    hyperfineCalculations *hypFi;
    valueContainerClass *valueContainer;
    std::shared_ptr<relativeIntensityFitter> relativefitter;

    std::vector<double> frequencyRange;
    std::vector<double> outputVector_summedTransitions;
    std::vector<std::vector<double>> outputVector_individualTransitions;
    //Fundamental Constants
    double c;
    double hBar;
    double pi;
    // QVectors storing the profile calculated with Relative intensities
    std::pair<std::vector<double>, std::vector<double>> fullRelativeProfilePair;
    std::unordered_map<std::string,std::pair<std::vector<double>,std::vector<double> > > singleRelativePeaks;

    //same as above but for the ionizing non-resonant transition
    double calculateNumberOfIonizingStepTransitions(){
        double laserFreq=(c/(valueContainer->getWl_nr()*1E-9));                                                                     //[Hz]
        double photonEnergy=laserFreq*hBar*2*pi;                                                                                    //[J]
        double photonIntensity=valueContainer->getI_nr()*(1E-6/photonEnergy);                                                       //[photons/cm^2]
        double transitionsPerPulse_nr=valueContainer->getCS_nr()*photonIntensity;                                                   //[1]
        return transitionsPerPulse_nr;
    }

    void calculateFrequencyRange(std::vector<double> &frequencyRange_r, std::vector<double> transitionFrequencies_v, std::vector<double> measuredProfileRange_v){
        //calculates the frequency range based on the minimun and maximumm transition frequency
        //Causes unnecessary fine step size for the range when (gaussianWidth+LorentzianWidth) is low.
        double CoG=valueContainer->getCoG()*1E9;             //Center of Gravity
        double maxFreq=CoG;
        double minFreq=CoG;
        double voigtFWHM=valueContainer->getVoigtFWHM();
         if(measuredProfileRange_v.empty()) {
            if(transitionFrequencies_v.empty()){
                maxFreq=CoG+(voigtFWHM)*10;
                minFreq=CoG-(voigtFWHM)*10;
            }
            else{
                //QList<double> tFreq=transitionFreqeuencies.values();
                for(const auto &val: transitionFrequencies_v){
                    if(val<minFreq){
                        minFreq=val;
                    }
                    else if(val>maxFreq){
                        maxFreq=val;
                    }
                }
                maxFreq+=(voigtFWHM)*10;
                minFreq-=(voigtFWHM)*10;
            }
            //The frequency range will consist of points with separtion of (gaussianWidth+lorentzianWidth)/10
            double pointSep=(voigtFWHM)/10;
            frequencyRange_r.clear();
            double freqPoint=minFreq;
            while(freqPoint<maxFreq){
                frequencyRange_r.push_back(freqPoint);
                freqPoint=freqPoint+pointSep;
            }
            frequencyRange_r.push_back(maxFreq);
        }
        else{
            frequencyRange_r.clear();
            frequencyRange_r=measuredProfileRange_v;
        }
    }

    void parseSinglePeaks(){
        singleRelativePeaks.clear();
        std::vector<double> orderedTransitionFreqeuencies=hypFi->getOrderedTransitionFrequencies(0);
        std::unordered_map<std::string, std::string> logFrequencies;
        int transCounter=0;
        double start=0;
        double end=0;
        double transFreq=0;
        double voigtFWHM=valueContainer->getVoigtFWHM();
        for(auto const &trans: outputVector_individualTransitions){
            std::string transId=valueContainer->getHashOrder()[transCounter];
            std::pair<std::vector<double>, std::vector<double> > peakPairs;
            std::vector<double> freqProfileSection;
            std::vector<double> freqRangeSection;
            transFreq=orderedTransitionFreqeuencies[transCounter];
            start=transFreq-voigtFWHM*5;
            end=transFreq+voigtFWHM*5;
            int freqCounter=0;
            for(auto const &laserFrequency:frequencyRange) {
                if(laserFrequency>start && laserFrequency<end){
                    freqProfileSection.push_back(trans[freqCounter]);
                    freqRangeSection.push_back(laserFrequency);
                    logFrequencies[transId]=std::to_string(transFreq);
                }
                peakPairs.first=freqRangeSection;
                peakPairs.second=freqProfileSection;
                singleRelativePeaks[transId]=peakPairs;
                freqCounter++;
            }
            transCounter++;
        }
        valueContainer->setLog(logFrequencies);

    }



};

#endif // RELATIVEPROFILE_H
