#include "ionizationstep.h"
int ionizationStep::id=0;
ionizationStep::ionizationStep(int Ji_v, int Jf_v)
{
    hypFiState=nullptr;
    Ji=Ji_v;
    Jf=Jf_v;
    aCoefficient=0;                                                     //[1/s]
    laserPulseWidth=0;                                                  //[ns]
    laserPulseDelay=0;                                                  //[ns]
    isNonResonant=0;                                                    //True for non-resonant transitions
    gaussianWidth=0;                                                    //[MHz]
    lorentzianWidth=0;                                                  //[MHz]
    laserPulseIntensity=0;                                              //[MHz]
    centerOfGravity=0;                                                  //[MHz]
    localId=id;
    id++;
}

ionizationStep::~ionizationStep(){
    id--;
//    for(hyperFineStateProperties &state : initialHyperfineStates_ordered){
//        if(state!=nullptr){
//            delete state;
//        }
//    }
}

int ionizationStep::getId() const{
    return localId;
}

void ionizationStep::setJi(int newJi){
    Ji=newJi;
}

int ionizationStep::getJi() const{
    return Ji;
}

void ionizationStep::setJf(int newJf){
    Jf=newJf;
}

int ionizationStep::getJf() const{
    return Jf;
}

void ionizationStep::setNumberOfTransitions_ordered(std::vector<std::vector<double>> newNumberOfTransitions_ordered){
    numberOfTransitions_ordered=newNumberOfTransitions_ordered;
}

std::vector<std::vector<double>> ionizationStep::getNumberOfTransitions_ordered(){
    return numberOfTransitions_ordered;
}

void ionizationStep::setACoefficient(double newACoefficient){
    aCoefficient=newACoefficient;
}

double ionizationStep::getACoefficient()  const{
    return aCoefficient;
}

void ionizationStep::setLaserPulseWidth(double newLaserPulseWidth){
    laserPulseWidth=newLaserPulseWidth;
}

double ionizationStep::getLaserPulseWidth()  const{
    return laserPulseWidth;
}

void ionizationStep::setLaserPulseDelay(double newLaserPulseDelay){
    laserPulseDelay=newLaserPulseDelay;
}

double ionizationStep::getLaserPulseDelay()  const{
    return laserPulseDelay;
}

void ionizationStep::setGaussianWidth(double newGaussianWidth){
    gaussianWidth=newGaussianWidth;
}

double ionizationStep::getGaussianWidth()  const{
    return gaussianWidth;
}

void ionizationStep::setLorentzianWidth(double newLorentzianWidth){
    lorentzianWidth=newLorentzianWidth;
}

double ionizationStep::getLorentzianWidth()  const{
    return lorentzianWidth;
}

void ionizationStep::setLaserPulseIntensity(double newLaserPulseIntensity){
    laserPulseIntensity=newLaserPulseIntensity;
}

double ionizationStep::getLaserPulseIntensity()  const{
    return laserPulseIntensity;
}

void ionizationStep::setCenterOfGravity(double newCenterOfGravity){
    centerOfGravity=newCenterOfGravity;
}

double ionizationStep::getCenterOfGravity()  const{
    return centerOfGravity;
}

void ionizationStep::setInitialHyperFineStates(std::unique_ptr<hyperFineStateProperties> state, bool replace, int idx){
    if(replace && initialHyperfineStates_ordered.size()>idx){
        initialHyperfineStates_ordered[idx]=std::move(state);
    }
    else{
        initialHyperfineStates_ordered.push_back(std::move(state));
    }
}

void ionizationStep::setFinalHyperFineStates(std::unique_ptr<hyperFineStateProperties> state, bool replace, int idx){
    if(replace && finalHyperfineStates_ordered.size()>idx){
        finalHyperfineStates_ordered[idx]=std::move(state);
    }
    else{
        finalHyperfineStates_ordered.push_back(std::move(state));
    }
}

std::unique_ptr<hyperFineStateProperties> ionizationStep::getInitialHyperFineState(int state){
    return std::move(initialHyperfineStates_ordered[state]);
}

std::unique_ptr<hyperFineStateProperties> ionizationStep::getFinalHyperFineState(int state){
    return std::move(finalHyperfineStates_ordered[state]);
}

int ionizationStep::getNumberOfInitialStates(){
    return initialHyperfineStates_ordered.size();
}

int ionizationStep::getNumberOfFinalStates(){
    return finalHyperfineStates_ordered.size();
}

std::vector<double> ionizationStep::getInitialFs(){
    std::vector<double> Fs;
    for(const auto &state: initialHyperfineStates_ordered ){
        Fs.push_back(state->getF());
    }
    return Fs;
}

std::vector<double> ionizationStep::getFinalFs(){
    std::vector<double> Fs;
    for(const auto &state: finalHyperfineStates_ordered ){
        Fs.push_back(state->getF());
    }
    return Fs;

}
void ionizationStep::setInitialAlpha(double value){
    initialAlphas.push_back(value);
}

void ionizationStep::setInitialBeta(double value){
    initialBetas.push_back(value);
}

void ionizationStep::setFinalAlpha(double value){
    finalAlphas.push_back(value);
}

void ionizationStep::setFinalBeta(double value){
    finalBetas.push_back(value);
}

double ionizationStep::getInitialAlpha(int transition){
    return initialAlphas[transition];
}

double ionizationStep::getInitialBeta(int transition){
    return initialBetas[transition];
}

double ionizationStep::getFinalAlpha(int transition){
    return finalAlphas[transition];
}

double ionizationStep::getFinalBeta(int transition){
    return finalBetas[transition];
}

double ionizationStep::getRelativeIntensity(int transition){
    return relativeIntensities_ordered[transition];
}

std::vector<double> ionizationStep::getOrderedRelativeIntensities(){
   return relativeIntensities_ordered;
}

void ionizationStep::setAllowedTransition(double relativeIntensity){
    relativeIntensities_ordered.push_back(relativeIntensity);
}

void ionizationStep::setInitialStateId(int id){
    //No duplicate states
    if(!(std::find(initialStateIds.begin(), initialStateIds.end(), id) != initialStateIds.end())){
        initialStateIds.push_back(id);
    }
}

void ionizationStep::setFinalStateId(int id){
    //No duplicate states
    if(!(std::find(finalStateIds.begin(), finalStateIds.end(), id) != finalStateIds.end())){
        finalStateIds.push_back(id);
    }
}

int ionizationStep::getInitialStateId(int transition){
    return initialStateIds[transition];
}

int ionizationStep::getFinalStateId(int transition){
    return finalStateIds[transition];
}

int ionizationStep::getNumberOfAllowedTransitions(){
    return initialAlphas.size();
}

std::vector<int> ionizationStep::getInitialStateIds(){
    return initialStateIds;
}

std::vector<int> ionizationStep::getFinalStateIds(){
    return finalStateIds;
}
