//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef HYPERFINECALCULATIONS_H
#define HYPERFINECALCULATIONS_H

#include <QObject>
#include <hyperfinestateproperties.h>
#include <sixj.h>
#include <valuecontainerclass.h>
#include <voigtprofile.h>
#include <ionizationstep.h>
#include <memory>
#include <unordered_map>

class hyperfineCalculations : public QObject
{
    Q_OBJECT
public:
    explicit hyperfineCalculations(QObject *parent = 0, valueContainerClass *valueContainer_p=0);
    ~hyperfineCalculations();
    std::vector<double> getInitialFs(int step_v=0) const;
    double getNumberOfInitialFStates(int step_v=0);
    std::vector<double> getFinalFs(int step_v=0) const;
    double getNumberOfFinalFStates(int step_v=0);
    double getHyperFineDeltaNu(double A_v, double B_v, int transition=0, int step_v=0);
    double getRelativeTransitionIntensity(int transition=0, int step_v=0);
    double getAlpha(int transition=0, int step_v=0);
    double getBeta(int transition=0, int step_v=0);
    std::vector<int> getInitialStateIds(int step_v=0);
    std::vector<int> getFinalStateIds(int step_v=0);
    std::vector<std::vector<double>> getAlphasAndBetas(int numberOfAllowedTransitions_i);
    std::vector<double> getOrderedTransitionFrequencies(int step_v=0);
    std::vector<double> getOrderedRelativeTransitionIntensities(int step_v=0);
    std::vector<std::vector<int>> getOrderedAllowedStates(int step_v=0);
    int getNumberOfAllowedTransitions(int step_v);
    int getIonizedStateId();
    void setNumberOfBoundSteps(int numberOfBoundSteps_v=2);
    void setStep(int step_v);
private:
    valueContainerClass *valueContainer;
    std::unique_ptr<voigtProfile> voigt;
    std::unique_ptr<sixJ> sixJCalc;
    int step;
    int numberOBoundSteps;
    std::vector<std::unique_ptr<ionizationStep>> ionizationSteps;
    std::vector<std::vector<double>> transitionNumbers_ordered;
    std::vector<std::string> hashOrder;
    std::vector<double> orderedTransitionFrequencies;
    std::vector<double> orderedRelativeTransitionIntensities;
    std::vector<std::vector<std::vector<int>>> orderedAllowedStatesPerStep;
    int numberOfAllowedTransitions;
    std::vector<double> initialF;   //Spins for the initial F-states
    std::vector<double> finalF;     //Spins for the final F-states
    double initialJ;        //Spin of the initial J-state
    double finalJ;          //Spin of the final J-state
    double nucSpin;         //Nuclear spin
    std::unordered_map<std::string, double> relativeTransitionFrequencies;//This includes the CoG
    std::unordered_map<std::string, double> relativeTransitionIntensities;
    std::unordered_map<std::string, std::string> transitionInfo;
    //Profile output data
    std::unordered_map<std::string, std::vector<double>  > transitionNumbers;
    std::unordered_map<std::string, std::vector<double>  > transitionNumbersWithScalers; //Includes background and intensity scalers
    std::vector<double> fullRelativeProfile;

    void calculateFStates(double Ji_v,double Jf_v, double I,
                          std::vector<double> &initialF_r, std::vector<double> &finalF_r);
    bool checkSelectionRules(double initialF_v, double finalF_v);
    double getTransitionFrequency(double CoG_v, double initialA_v,  double initialB_v,
                                  double finalA_v, double finalB_v,int transition,
                                  const std::vector<std::unique_ptr<ionizationStep>> &ionizationSteps_r,
                                  const int &step_r);
    void generateHyperfineStates(double initialJ_r,double finalJ_r,double I_r,
                                 int &numberOfAllwedTransitions_r,
                                 std::unordered_map<std::string, std::string> &transitionInfo_r,
                                 std::vector<std::unique_ptr<ionizationStep>> &ionizationSteps_r,
                                 std::vector<std::string> &hashOrder_r,
                                 std::vector<std::vector<std::vector<int>>> &orderedAllowedStatesPerStep_r,
                                 const std::vector<double> initialF_r,
                                 const std::vector<double> finalF_r,
                                 const int &step_r);
    double alpha(double F, double J, double I);
    double beta(double F, double J, double I);
    double gamma(double F, double I, double J);
    double relativeIntensities(double Fi_v, double Ff_v, double Ji_v, double Jf_v, double I);
    void parseTransitionInfo(int initialF_v, int finalF_v, std::string initialStateId, std::string finalStateId, double intensity_v, std::unordered_map<std::string, std::string> &transitionInfo_r);
    void parseTransitionFrequenciesAndIntensities(double initialA_v, double initialB_v, double finalA_v,
                                                  double finalB_v, double CoG_v,
                                                  std::unordered_map<std::string, double> &relativeTransitionFrequencies,
                                                  std::unordered_map<std::string, double> &relativeTransitionIntensities_r,
                                                  std::vector<double> &orderedTransitionFrequencies_r,
                                                  std::vector<double> &orderedRelativeTransitionIntensities_r,
                                                  const std::vector<std::string> &hashOrder_r,
                                                  const std::vector<std::unique_ptr<ionizationStep>> &ionizationSteps_r,
                                                  const int &step_r);
signals:
    //////////////////////////
    //Emit HyperFine parameters
    //////////////////////////
    void calculationsDone(std::unordered_map<std::string, std::string> transitionInfo_v);
    void emitRelativeTransitionIntensities(std::unordered_map<std::string, double> newRelativeTransitionIntensities_v);
    void emitRelativeTransitionFrequencies(std::unordered_map<std::string, double> newRelativeTransitionFrequencies_v);
    void emitHashOrder(std::vector<std::string> newHashOrder_v);
    void parsingDone();
public slots:
    void calculateHyperfineTransitions();
    void parseHyperfineParameters();
};

#endif // HYPERFINECALCULATIONS_H
