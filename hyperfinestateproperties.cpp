#include <hyperfinestateproperties.h>
hyperFineStateProperties::hyperFineStateProperties(std::string stateId_v){
    stateId=stateId_v;
}
hyperFineStateProperties::~hyperFineStateProperties(){

}

//Set:
void hyperFineStateProperties::setN(int n_v){
    n=n_v;
}
void hyperFineStateProperties::setTotalFStatesAtJ(int totalFStatesAtJ_v){
    totalFStatesAtJ=totalFStatesAtJ_v;
}
void hyperFineStateProperties::setIsInitialState(bool isInitialState_v){
    isInitialState=isInitialState_v;
}
void hyperFineStateProperties::setAlphaAndBeta(double alpha_v, double beta_v){
    alpha=alpha_v;
    beta=beta_v;
}
void hyperFineStateProperties::setFI(double F_v, double J_v){
    F=F_v;
    J=J_v;
}
void hyperFineStateProperties::addAllowedTransition(std::string finalStateId_v, double relInt_V){
    allowedTransitionIntensities[finalStateId_v]=relInt_V;
    allowedTransitionIntensities_sorted.push_back(relInt_V);
}
//Get:
std::string hyperFineStateProperties::getStateId() const{
    return stateId;
}
int hyperFineStateProperties::getN() const{
    return n;
}
int hyperFineStateProperties::getTotalFStatesAtJ() const{
    return totalFStatesAtJ;
}

int hyperFineStateProperties::getNumberOfAllowedTransitionsFromThisState(){
    return allowedTransitionIntensities_sorted.size();
}

bool hyperFineStateProperties::getIsInitialState() const{
    return isInitialState;
}
double hyperFineStateProperties::getAlpha() const{
    return alpha;
}
double hyperFineStateProperties::getBeta() const{
    return beta;
}
double hyperFineStateProperties::getF() const{
    return F;
}
double hyperFineStateProperties::getJ() const{
    return J;
}

//get strength for particular transition
double hyperFineStateProperties::getIntensity_fromMap(std::string finalStateId){
    const auto &iter = allowedTransitionIntensities.find(finalStateId);
    if (iter != allowedTransitionIntensities.end() )
    {
        return allowedTransitionIntensities[finalStateId];
    }
    else{
        return 0.0;
    }
}
double hyperFineStateProperties::getIntensity_fromVector(int transition){
    if(transition<allowedTransitionIntensities_sorted.size()){
        return allowedTransitionIntensities_sorted[transition];
    }
}

//Gett all transition strengths(relative)
std::map<std::string, double> hyperFineStateProperties::getAllowedTransitionIntensities() const{
    return allowedTransitionIntensities;
}
