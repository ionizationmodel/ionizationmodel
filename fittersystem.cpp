#include "fittersystem.h"
/////////////////
//C++->C wrappers
/////////////////

extern "C" int call_referenceFitterIDASDAESystem(realtype tres, N_Vector yy, N_Vector yp, N_Vector rr, void *user_data) // wrapper function
{
    fitterSystem* wrapper = (fitterSystem *) user_data;
    return wrapper->referenceFitterIDASDAESystem(tres, yy, yp, rr, user_data);
}

extern "C" int call_referenceFitterIDASDAESystemJacobi(long Neq, realtype tt, realtype cj, N_Vector yy, N_Vector yp, N_Vector resvec, DlsMat JJ, void *user_data, N_Vector tempv1, N_Vector tempv2, N_Vector tempv3) // wrapper function
{
    fitterSystem* wrapper = (fitterSystem *) user_data;
    return wrapper->referenceFitterIDASDAESystemJacobi(Neq,tt, cj, yy, yp, resvec, JJ, user_data, tempv1, tempv2, tempv3);
}

extern "C" int call_referenceFitterIDASDAESystemSensitivityRHS(int Ns, realtype tt,
                                                                        N_Vector yy, N_Vector yp, N_Vector resval,
                                                                        N_Vector *yyS, N_Vector *ypS, N_Vector *resvalS,
                                                                        void *user_data,
                                                                        N_Vector tmp1, N_Vector tmp2, N_Vector tmp3){
    fitterSystem* wrapper = (fitterSystem *) user_data;
    return wrapper->referenceFitterIDASDAESystemSensitivityRHS(Ns, tt, yy, yp, resval, yyS, ypS, resvalS, user_data, tmp1, tmp2, tmp3);
}

extern "C" int call_fitterIDASDAESystem(realtype tres, N_Vector yy, N_Vector yp, N_Vector rr, void *user_data) // wrapper function
{
    fitterSystem* wrapper = (fitterSystem *) user_data;
    return wrapper->fitterIDASDAESystem(tres, yy, yp, rr, user_data);
}

extern "C" int call_fitterIDASDAESystemJacobi(long Neq, realtype tt, realtype cj, N_Vector yy, N_Vector yp, N_Vector resvec, DlsMat JJ, void *user_data, N_Vector tempv1, N_Vector tempv2, N_Vector tempv3) // wrapper function
{
    fitterSystem* wrapper = (fitterSystem *) user_data;
    return wrapper->fitterIDASDAESystemJacobi(Neq,tt, cj, yy, yp, resvec, JJ, user_data, tempv1, tempv2, tempv3);
}

//extern "C" int call_DAESystem(realtype tres, N_Vector yy, N_Vector yp, N_Vector rr, void *user_data) // wrapper function
//{
//    fitterSystem* wrapper = (fitterSystem *) user_data;
//    return wrapper->IDASDAESystem(tres, yy, yp, rr, user_data);
//}

//extern "C" int call_DAESystemJacobi(long Neq, realtype tt, realtype cj, N_Vector yy, N_Vector yp, N_Vector resvec, DlsMat JJ, void *user_data, N_Vector tempv1, N_Vector tempv2, N_Vector tempv3) // wrapper function
//{
//    fitterSystem* wrapper = (fitterSystem *) user_data;
//    return wrapper->IDASDAESystemJacobi(Neq,tt, cj, yy, yp, resvec, JJ, user_data, tempv1, tempv2, tempv3);
//}

void fitterSystem::setupIDASSystem(){
    IDAS_mem = nullptr;                                                                                         //IDAS solver object.
    IDAS_y = IDAS_yp = IDAS_abstol = IDAS_constraints= nullptr;                                                 //Initial conditions for y, dy and absolute tolerences.
    IDAS_yval = IDAS_ypval = IDAS_atval = IDAS_yconst= nullptr;                                                 //N-vector representation of the data above.
    IDAS_yS, IDAS_ypS= nullptr;                                                                                 //Sensitivity vectors
    int numberOfEquations=data->numberOfEquations;
    //Setup sensitivities part 1
    int numberOfSensitivities=data->numberOfSensitivities;
    realtype pbar[numberOfSensitivities];
    sensitivitiesEigen.resize(numberOfSensitivities);
    //data->reallocP(numberOfSensitivities);
    for(int idx=0; idx<numberOfSensitivities;idx++){
            data->p[idx]=data->xValues[idx];
            //std::cout<<data->p[idx]<<"data->p[idx]"<<idx<<std::endl;
            if(data->p[idx]!=0){
                pbar[idx]=fabs(data->p[idx]);
            }
            else{
                pbar[idx]=1.0;
            }
    }
    /* Allocate N-vectors. */
    IDAS_y = N_VNew_Serial(numberOfEquations);
    IDAS_yp = N_VNew_Serial(numberOfEquations);
    IDAS_abstol = N_VNew_Serial(numberOfEquations);
    //constraints
    //IDAS_constraints=N_VNew_Serial(numberOfEquations);
    /* Create and initialize  y, y', and absolute tolerance vectors. */
    IDAS_yval  = NV_DATA_S(IDAS_y);
    IDAS_ypval = NV_DATA_S(IDAS_yp);
    IDAS_atval = NV_DATA_S(IDAS_abstol);
    //N_VConst(RCONST(1.0), IDAS_constraints);
    for(unsigned int i=0; i<numberOfEquations;i++){
        IDAS_yval[i]=data->initialConditions[i];
        IDAS_ypval[i]=data->initialTransitionRates[i];
        IDAS_atval[i]=data->absoluteTolerances[i];
    }
    /* Integration limits */
    IDAS_t0 = 0;
    IDAS_tout1 = RCONST(data->laserPulseProperties[0][0]*data->integrationTimeScaler);
    /*Relative tolerance*/
    IDAS_rtol=data->relativeTolerance;
    /* Call IDACreate and IDAInit to initialize IDA memory */
    IDAS_mem = IDACreate();
    IDAInit(IDAS_mem, call_referenceFitterIDASDAESystem, IDAS_t0, IDAS_y, IDAS_yp);
    IDASVtolerances(IDAS_mem, IDAS_rtol, IDAS_abstol);
    IDASetUserData(IDAS_mem, data);
    /* Call IDADense and set up the linear solver. */
    IDAS_retval = IDADense(IDAS_mem, numberOfEquations);
//    if(data->setUserJacobian){
        IDAS_retval = IDADlsSetDenseJacFn(IDAS_mem, call_referenceFitterIDASDAESystemJacobi);
//    }
//    if(data->setConstraints){
//        IDASetConstraints(IDAS_mem, IDAS_constraints);
//    }
    IDASetNonlinConvCoef(IDAS_mem,data->setNonlinConvCoef);
    IDASetMaxNumSteps(IDAS_mem, data->maxNumSteps);
    //Setup sensitivity analysis part 2
    IDAS_yS = N_VCloneVectorArray_Serial(numberOfSensitivities, IDAS_y);
    for (int is=0;is<numberOfSensitivities;is++) N_VConst(0, IDAS_yS[is]);

    IDAS_ypS = N_VCloneVectorArray_Serial(numberOfSensitivities, IDAS_y);
    for (int is=0;is<numberOfSensitivities;is++) N_VConst(0, IDAS_ypS[is]);

    IDASensInit(IDAS_mem, numberOfSensitivities, IDA_SIMULTANEOUS , call_referenceFitterIDASDAESystemSensitivityRHS, IDAS_yS, IDAS_ypS);
//    IDASensInit(IDAS_mem, numberOfSensitivities, IDA_SIMULTANEOUS , NULL, IDAS_yS, IDAS_ypS);

//    IDASetSensParams(IDAS_mem, data->p,pbar, NULL);
    IDASensEEtolerances(IDAS_mem);
    IDAGetSensConsistentIC(IDAS_mem, IDAS_yS, IDAS_ypS);
    //Results
    resultTimeinterval=IDAS_tout1/data->timePoints;

    allocated=true;
}
void fitterSystem::resetValuesForFitter(){

}


void fitterSystem::runIDASSystem(){
    IDAS_yval  = NV_DATA_S(IDAS_y);
    IDAS_tout=resultTimeinterval;
    long int nst;
    while(IDAS_tout<=IDAS_tout1){
        if(IDASolve(IDAS_mem, IDAS_tout, &IDAS_tret, IDAS_y, IDAS_yp, IDA_NORMAL)!=0){
            break;
        }
        IDAGetNumSteps(IDAS_mem, &nst);
        IDAS_tout+=resultTimeinterval;
    }
    ionizedStatePopulation=IDAS_yval[data->ionizedStateId];

    //IDAGetSensDky(IDAS_mem, IDAS_tout1, 1, IDAS_ypS);
    IDAGetSens(IDAS_mem, &IDAS_tout1, IDAS_ypS);

        int ionsizedState=data->ionizedStateId;
        //std::cout<<"--------------------"<<std::endl;

        for(int j=0; j<data->numberOfSensitivities;j++){
            //std::cout<<NV_DATA_S(IDAS_ypS[j])[ionsizedState]<<" sensitivity, ionized state "<<std::endl;
            sensitivitiesEigen(j)=NV_DATA_S(IDAS_ypS[j])[ionsizedState]/sigma();
        }
}
void fitterSystem::cleanIDASSystem(){
    if(allocated){
        N_VDestroy_Serial(IDAS_y);
        N_VDestroy_Serial(IDAS_yp);
        N_VDestroy_Serial(IDAS_abstol);
        //N_VDestroy_Serial(IDAS_constraints);
        N_VDestroyVectorArray_Serial(IDAS_yS,data->numberOfSensitivities);
        N_VDestroyVectorArray_Serial(IDAS_ypS,data->numberOfSensitivities);
        IDAFree(&IDAS_mem);
        allocated=false;
    }
}


////////////////
/// IDAS DAE
////////////////
int fitterSystem::referenceFitterIDASDAESystem(realtype tres, N_Vector yy, N_Vector yp, N_Vector rr, void *user_data)
{
//    x[1]->x[3]:  Initial F: 1 Final F: 1 Transition intensity: 0.5
//    x[0]->x[3]:  Initial F: 0 Final F: 1 Transition intensity: 0.25
//    x[1]->x[2]:  Initial F: 1 Final F: 0 Transition intensity: 0.25
    realtype *yval, *ypval, *rval;
    realtype t=tres;

    //New Code
    yval = NV_DATA_S(yy);
    ypval = NV_DATA_S(yp);
    rval = NV_DATA_S(rr);

    struct fitterSystem::fitterParameters* data;
    data =(struct fitterSystem::fitterParameters*) user_data;
    //["initialA"]=data->p[0];
    //["initialB"]=data->p[1];
    //["finalA"]=data->p[2];
    //["finalB"]=data->p[3]
    //["CoG"]=data->p[4];
    //["gaussianWidth"]=data->p[5];
    //["lorentzianWidth"]=xdata->p6];
    //["intensityScaler"]=data->p[7];
    //["backgroundScaler"]=data->p[8];
    //["laserIntensity"]=data->p[9];
    int numberOfParameters=data->numberOfParameters;
    int numberOfTransitions=data->numberOfTransitions;
    std::vector<std::vector<double>> alphasAndBetas=data->alphasAndBetas;
    realtype initialAlpha0=alphasAndBetas[0][0];
    realtype initialBeta0=alphasAndBetas[1][0];;
    realtype finalAlpha0=alphasAndBetas[2][0];
    realtype finalBeta0=alphasAndBetas[3][0];
    realtype initialAlpha1=alphasAndBetas[0][1];
    realtype initialBeta1=alphasAndBetas[1][1];;
    realtype finalAlpha1=alphasAndBetas[2][1];
    realtype finalBeta1=alphasAndBetas[3][1];
    realtype initialAlpha2=alphasAndBetas[0][2];
    realtype initialBeta2=alphasAndBetas[1][2];
    realtype finalAlpha2=alphasAndBetas[2][2];
    realtype finalBeta2=alphasAndBetas[3][2];

    realtype aCoefficient=data->ACoefficients[0];

    realtype relInt0=data->p[(numberOfParameters-numberOfTransitions)+0];
    realtype relInt1=data->p[(numberOfParameters-numberOfTransitions)+1];
    realtype relInt2=data->p[(numberOfParameters-numberOfTransitions)+2];

    std::vector<double> gauss;
    gauss.push_back(data->getGaussianLineShape(t, data->laserPulseProperties[0][0], data->laserPulseProperties[0][1]));
    gauss.push_back(data->getGaussianLineShape(t, data->laserPulseProperties[1][0], data->laserPulseProperties[1][1]));

//    realtype relNoOfTrans0=data->calculateNumberOfTransitions(data->laserFrequency,relInt0, data->p[6],data->p[5], data->p[7],
//            data->p[4],data->p[0], data->p[1], data->p[2],data->p[3],
//            initialAlpha0, initialBeta0, finalAlpha0, finalBeta0, aCoefficient,gauss[0]);
//    realtype relNoOfTrans1=data->calculateNumberOfTransitions(data->laserFrequency,relInt1, data->p[6],data->p[5], data->p[7],
//            data->p[4],data->p[0], data->p[1], data->p[2],data->p[3],
//            initialAlpha1, initialBeta1, finalAlpha1, finalBeta1, aCoefficient,gauss[0]);
//    realtype relNoOfTrans2=data->calculateNumberOfTransitions(data->laserFrequency,relInt2, data->p[6],data->p[5], data->p[7],
//            data->p[4],data->p[0], data->p[1], data->p[2],data->p[3],
//            initialAlpha2, initialBeta2, finalAlpha2, finalBeta2, aCoefficient,gauss[0]);

    realtype relNoOfTrans0=data->getNumberOfTransitions(data->xSubValues[0],data->laserFrequency,
            initialAlpha0, initialBeta0, finalAlpha0, finalBeta0, aCoefficient,gauss[0]);
    realtype relNoOfTrans1=data->getNumberOfTransitions(data->xSubValues[1],data->laserFrequency,
            initialAlpha1, initialBeta1, finalAlpha1, finalBeta1, aCoefficient,gauss[0]);
    realtype relNoOfTrans2=data->getNumberOfTransitions(data->xSubValues[2],data->laserFrequency,
            initialAlpha2, initialBeta2, finalAlpha2, finalBeta2, aCoefficient,gauss[0]);



    realtype spontDec1=data->spontaneousDecayRateVectors[0][0];
    realtype transRate1=relNoOfTrans0;

    realtype spontDec3=data->spontaneousDecayRateVectors[0][1];
    realtype transRate3=relNoOfTrans1;

    realtype spontDec2=data->spontaneousDecayRateVectors[0][2];
    realtype transRate2=relNoOfTrans2;

    realtype nonResonantTransitionRate1=data->ionizingTransitionRate*gauss[1];
    realtype nonResonantTransitionRate2=data->ionizingTransitionRate*gauss[1];

    //0
    rval[0]=(transRate1+spontDec1)*yval[3]-(transRate1)*yval[0]-ypval[0];
    //1
    rval[1]=(transRate3+spontDec3)*yval[2]+(transRate2+spontDec2)*yval[3]-(transRate3+transRate2)*yval[1]-ypval[1];
    //2
    rval[2]=(transRate3)*yval[1]-(transRate3+spontDec3+nonResonantTransitionRate1)*yval[2]-ypval[2];
    //3
    rval[3]=(transRate1)*yval[0]+(transRate2)*yval[1]-(transRate1+transRate2+spontDec1+spontDec2+nonResonantTransitionRate2)*yval[3]-ypval[3];
    //4
    rval[4]=yval[0]+yval[1]+yval[2]+yval[3]+yval[4]-data->totalPopulation;
    return 0;
}

int fitterSystem::referenceFitterIDASDAESystemJacobi(long Neq, realtype tt, realtype cj, N_Vector yy, N_Vector yp, N_Vector resvec, DlsMat JJ, void *user_data, N_Vector tempv1, N_Vector tempv2, N_Vector tempv3){

    //    x[1]->x[3]:  Initial F: 1 Final F: 1 Transition intensity: 0.5
    //    x[0]->x[3]:  Initial F: 0 Final F: 1 Transition intensity: 0.25
    //    x[1]->x[2]:  Initial F: 1 Final F: 0 Transition intensity: 0.25
    realtype *yval;
    realtype t=tt;
    yval = NV_DATA_S(yy);
    struct fitterSystem::fitterParameters* data;
    data =(struct fitterSystem::fitterParameters*) user_data;
    //["initialA"]=data->p[0];
    //["initialB"]=data->p[1];
    //["finalA"]=data->p[2];
    //["finalB"]=data->p[3]
    //["CoG"]=data->p[4];
    //["gaussianWidth"]=data->p[5];
    //["lorentzianWidth"]=xdata->p6];
    //["intensityScaler"]=data->p[7];
    //["backgroundScaler"]=data->p[8];
    //["laserIntensity"]=data->p[9];
    int numberOfParameters=data->numberOfParameters;
    int numberOfTransitions=data->numberOfTransitions;
    std::vector<std::vector<double>> alphasAndBetas=data->alphasAndBetas;
    realtype initialAlpha0=alphasAndBetas[0][0];
    realtype initialBeta0=alphasAndBetas[1][0];;
    realtype finalAlpha0=alphasAndBetas[2][0];
    realtype finalBeta0=alphasAndBetas[3][0];
    realtype initialAlpha1=alphasAndBetas[0][1];
    realtype initialBeta1=alphasAndBetas[1][1];;
    realtype finalAlpha1=alphasAndBetas[2][1];
    realtype finalBeta1=alphasAndBetas[3][1];
    realtype initialAlpha2=alphasAndBetas[0][2];
    realtype initialBeta2=alphasAndBetas[1][2];
    realtype finalAlpha2=alphasAndBetas[2][2];
    realtype finalBeta2=alphasAndBetas[3][2];

    realtype aCoefficient=data->ACoefficients[0];

    realtype relInt0=data->p[(numberOfParameters-numberOfTransitions)+0];
    realtype relInt1=data->p[(numberOfParameters-numberOfTransitions)+1];
    realtype relInt2=data->p[(numberOfParameters-numberOfTransitions)+2];

    std::vector<double> gauss;
    gauss.push_back(data->getGaussianLineShape(t, data->laserPulseProperties[0][0], data->laserPulseProperties[0][1]));
    gauss.push_back(data->getGaussianLineShape(t, data->laserPulseProperties[1][0], data->laserPulseProperties[1][1]));

//    realtype relNoOfTrans0=data->calculateNumberOfTransitions(data->laserFrequency,relInt0, data->p[6],data->p[5], data->p[7],
//            data->p[4],data->p[0], data->p[1], data->p[2],data->p[3],
//            initialAlpha0, initialBeta0, finalAlpha0, finalBeta0, aCoefficient,gauss[0]);
//    realtype relNoOfTrans1=data->calculateNumberOfTransitions(data->laserFrequency,relInt1, data->p[6],data->p[5], data->p[7],
//            data->p[4],data->p[0], data->p[1], data->p[2],data->p[3],
//            initialAlpha1, initialBeta1, finalAlpha1, finalBeta1, aCoefficient,gauss[0]);
//    realtype relNoOfTrans2=data->calculateNumberOfTransitions(data->laserFrequency,relInt2, data->p[6],data->p[5], data->p[7],
//            data->p[4],data->p[0], data->p[1], data->p[2],data->p[3],
//            initialAlpha2, initialBeta2, finalAlpha2, finalBeta2, aCoefficient,gauss[0]);

    realtype relNoOfTrans0=data->getNumberOfTransitions(data->xSubValues[0],data->laserFrequency,
            initialAlpha0, initialBeta0, finalAlpha0, finalBeta0, aCoefficient,gauss[0]);
    realtype relNoOfTrans1=data->getNumberOfTransitions(data->xSubValues[1],data->laserFrequency,
            initialAlpha1, initialBeta1, finalAlpha1, finalBeta1, aCoefficient,gauss[0]);
    realtype relNoOfTrans2=data->getNumberOfTransitions(data->xSubValues[2],data->laserFrequency,
            initialAlpha2, initialBeta2, finalAlpha2, finalBeta2, aCoefficient,gauss[0]);

    realtype spontDec1=data->spontaneousDecayRateVectors[0][0];
    realtype transRate1=relNoOfTrans0;

    realtype spontDec3=data->spontaneousDecayRateVectors[0][1];
    realtype transRate3=relNoOfTrans1;

    realtype spontDec2=data->spontaneousDecayRateVectors[0][2];
    realtype transRate2=relNoOfTrans2;

    realtype nonResonantTransitionRate1=data->ionizingTransitionRate*gauss[1];
    realtype nonResonantTransitionRate2=data->ionizingTransitionRate*gauss[1];
    IJth(JJ,0,0) = -transRate1 - cj;
    IJth(JJ,1,0) = 0.0;
    IJth(JJ,2,0) = 0.0;
    IJth(JJ,3,0) = transRate1;
    IJth(JJ,4,0) = 1;

    IJth(JJ,0,1) = 0.0;
    IJth(JJ,1,1) = -transRate3-transRate2- cj;
    IJth(JJ,2,1) = transRate3;
    IJth(JJ,3,1) = transRate2;
    IJth(JJ,4,1) = 1;

    IJth(JJ,0,2) = 0.0;
    IJth(JJ,1,2) = transRate3+spontDec3;
    IJth(JJ,2,2) = -transRate3-spontDec3-nonResonantTransitionRate1 - cj;
    IJth(JJ,3,2) = 0.0;
    IJth(JJ,4,2) = 1;

    IJth(JJ,0,3) = transRate1+spontDec1;
    IJth(JJ,1,3) = transRate2+spontDec2;
    IJth(JJ,2,3) = 0.0;
    IJth(JJ,3,3) = -transRate1-transRate2-spontDec1-spontDec2-nonResonantTransitionRate2-cj;
    IJth(JJ,4,3) = 1;

    IJth(JJ,0,4) = 0;
    IJth(JJ,1,4) = 0;
    IJth(JJ,2,4) = 0;
    IJth(JJ,3,4) = 0;
    IJth(JJ,4,4) = 1;
    return 0;
}
int fitterSystem::referenceFitterIDASDAESystemSensitivityRHS(int Ns, realtype tt,
                N_Vector yy, N_Vector yp, N_Vector resval,
                N_Vector *yyS, N_Vector *ypS, N_Vector *resvalS,
                void *user_data,
                N_Vector tmp1, N_Vector tmp2, N_Vector tmp3)
{
    //TODO, contonue when required
    realtype p1, p2, p3;
    realtype y0, y1, y2, y3, y4;
    realtype yp0, yp1, yp2,yp3, yp4;
    realtype s0, s1, s2,s3, s4;
    realtype sd0, sd1, sd2, sd3, sd4;
    realtype rs0, rs1, rs2, rs3, rs4;
    int is=0;
    //    x[1]->x[3]:  Initial F: 1 Final F: 1 Transition intensity: 0.5
    //    x[0]->x[3]:  Initial F: 0 Final F: 1 Transition intensity: 0.25
    //    x[1]->x[2]:  Initial F: 1 Final F: 0 Transition intensity: 0.25
    realtype *yval;
    realtype t=tt;
    yval = NV_DATA_S(yy);
    struct fitterSystem::fitterParameters* data;
    data =(struct fitterSystem::fitterParameters*) user_data;
    //["initialA"]=xADSSubValues[0];
    //["initialB"]=xADSSubValues[1];
    //["finalA"]=xADSSubValues[2];
    //["finalB"]=xADSSubValues[3]
    //["CoG"]=xADSSubValues[4];
    //["gaussianWidth"]=xADSSubValues[5];
    //["lorentzianWidth"]=xADSSubValues[6];
    //["laserIntensity"]=xADSSubValues[7];
    //["relativeIntensity"]=xADSSubValues[8];
    int numberOfParameters=data->numberOfParameters;
    int numberOfTransitions=data->numberOfTransitions;
    std::vector<std::vector<double>> alphasAndBetas=data->alphasAndBetas;
    realtype initialAlpha0=alphasAndBetas[0][0];
    realtype initialBeta0=alphasAndBetas[1][0];;
    realtype finalAlpha0=alphasAndBetas[2][0];
    realtype finalBeta0=alphasAndBetas[3][0];
    realtype initialAlpha1=alphasAndBetas[0][1];
    realtype initialBeta1=alphasAndBetas[1][1];;
    realtype finalAlpha1=alphasAndBetas[2][1];
    realtype finalBeta1=alphasAndBetas[3][1];
    realtype initialAlpha2=alphasAndBetas[0][2];
    realtype initialBeta2=alphasAndBetas[1][2];
    realtype finalAlpha2=alphasAndBetas[2][2];
    realtype finalBeta2=alphasAndBetas[3][2];

    realtype aCoefficient=data->ACoefficients[0];

    realtype relInt0=data->p[(numberOfParameters-numberOfTransitions)+0];
    realtype relInt1=data->p[(numberOfParameters-numberOfTransitions)+1];
    realtype relInt2=data->p[(numberOfParameters-numberOfTransitions)+2];

    std::vector<double> gauss;
    gauss.push_back(data->getGaussianLineShape(t, data->laserPulseProperties[0][0], data->laserPulseProperties[0][1]));
    gauss.push_back(data->getGaussianLineShape(t, data->laserPulseProperties[1][0], data->laserPulseProperties[1][1]));

//    realtype relNoOfTrans0=data->calculateNumberOfTransitions(data->laserFrequency,relInt0, data->p[6],data->p[5], data->p[9],
//            data->p[4],data->p[0], data->p[1], data->p[2],data->p[3],
//            initialAlpha0, initialBeta0, finalAlpha0, finalBeta0, aCoefficient, gauss[0]);
//    realtype relNoOfTrans1=data->calculateNumberOfTransitions(data->laserFrequency,relInt1, data->p[6],data->p[5], data->p[9],
//            data->p[4],data->p[0], data->p[1], data->p[2],data->p[3],
//            initialAlpha1, initialBeta1, finalAlpha1, finalBeta1, aCoefficient, gauss[0]);
//    realtype relNoOfTrans2=data->calculateNumberOfTransitions(data->laserFrequency,relInt2, data->p[6],data->p[5], data->p[9],
//            data->p[4],data->p[0], data->p[1], data->p[2],data->p[3],
//            initialAlpha2, initialBeta2, finalAlpha2, finalBeta2, aCoefficient, gauss[0]);

    ADS relNoOfTrans0=data->getNumberOfTransitions<ADS>(data->xADSSubValues[0],data->laserFrequency,
            initialAlpha0, initialBeta0, finalAlpha0, finalBeta0, aCoefficient,gauss[0]);
    ADS relNoOfTrans1=data->getNumberOfTransitions<ADS>(data->xADSSubValues[1],data->laserFrequency,
            initialAlpha1, initialBeta1, finalAlpha1, finalBeta1, aCoefficient,gauss[0]);
    ADS relNoOfTrans2=data->getNumberOfTransitions<ADS>(data->xADSSubValues[2],data->laserFrequency,
            initialAlpha2, initialBeta2, finalAlpha2, finalBeta2, aCoefficient,gauss[0]);

    realtype spontDec1=data->spontaneousDecayRateVectors[0][0];
    ADS transRate1=relNoOfTrans0;

    realtype spontDec3=data->spontaneousDecayRateVectors[0][1];
    ADS transRate3=relNoOfTrans1;

    realtype spontDec2=data->spontaneousDecayRateVectors[0][2];
    ADS transRate2=relNoOfTrans2;

    realtype nonResonantTransitionRate1=data->ionizingTransitionRate*gauss[1];
    realtype nonResonantTransitionRate2=data->ionizingTransitionRate*gauss[1];

    y0 = NV_Ith_S(yy,0);
    y1 = NV_Ith_S(yy,1);
    y2 = NV_Ith_S(yy,2);
    y3 = NV_Ith_S(yy,3);
    y4 = NV_Ith_S(yy,4);

    yp0 = NV_Ith_S(yp,0);
    yp1 = NV_Ith_S(yp,1);
    yp2 = NV_Ith_S(yp,2);
    yp3 = NV_Ith_S(yp,3);
    yp4 = NV_Ith_S(yp,4);

    int sens=data->numberOfSensitivities;
    int intIdx=data->numberOfSensitivities-data->numberOfEquations+1;
    int subIntIdx=0;
    for(is=0; is<sens;is++){
        s0 = NV_Ith_S(yyS[is],0);
        s1 = NV_Ith_S(yyS[is],1);
        s2 = NV_Ith_S(yyS[is],2);
        s3 = NV_Ith_S(yyS[is],3);
        s4 = NV_Ith_S(yyS[is],4);

        sd0 = NV_Ith_S(ypS[is],0);
        sd1 = NV_Ith_S(ypS[is],1);
        sd2 = NV_Ith_S(ypS[is],2);
        sd3 = NV_Ith_S(ypS[is],3);
        sd4 = NV_Ith_S(ypS[is],4);

//        //0
//        rval[0]=(transRate1+spontDec1)*yval[3]-(transRate1)*yval[0]-ypval[0];
//        //1
//        rval[1]=(transRate3+spontDec3)*yval[2]+(transRate2+spontDec2)*yval[3]-(transRate3+transRate2)*yval[1]-ypval[1];
//        //2
//        rval[2]=(transRate3)*yval[1]-(transRate3+spontDec3+nonResonantTransitionRate1)*yval[2]-ypval[2];
//        //3
//        rval[3]=(transRate1)*yval[0]+(transRate2)*yval[1]-(transRate1+transRate2+spontDec1+spontDec2+nonResonantTransitionRate2)*yval[3]-ypval[3];
//        //4
//        rval[4]=yval[0]+yval[1]+yval[2]+yval[3]+yval[4]-data->totalPopulation;

        //dF/dy and dF/dyd
        rs0=-sd0-(transRate1.value())*s0-(transRate1.value()+spontDec1)*s3;
        rs1=-sd1-(transRate3.value()+transRate2.value())*s1+(transRate3.value()+spontDec3)*s2+(transRate2.value()+spontDec2)*s3;
        rs2=-sd2+(transRate3.value())*s1-(transRate3.value()+spontDec3+nonResonantTransitionRate1)*s2;
        rs3=-sd3+(transRate1.value())*s0+(transRate2.value())*s1-(transRate1.value()+transRate2.value()+spontDec1+spontDec2+nonResonantTransitionRate2)*s3;
        rs4=s0+s1+s2+s3+s4;
        //dF/dp

        if(is<=intIdx){
            rs0+=transRate1.derivatives()[is]*y3+spontDec1*y3-transRate1.derivatives()[is]*y0;
            rs1+=transRate3.derivatives()[is]*y2+spontDec3*y2+transRate2.derivatives()[is]*y3+spontDec2*y3-transRate3.derivatives()[is]*y1-transRate2.derivatives()[is]*y1;
            rs2+=transRate3.derivatives()[is]*y1-transRate3.derivatives()[is]*y2-spontDec3*y2-nonResonantTransitionRate1*y2;
            rs3+=transRate1.derivatives()[is]*y0+transRate2.derivatives()[is]*y1-transRate1.derivatives()[is]*y3-transRate2.derivatives()[is]*y3-spontDec1*y3+spontDec2*y3-nonResonantTransitionRate2*y3;
            rs4+=0;
//            std::cout<<"--------------------------------"<<std::endl;
//            std::cout<<transRate1.derivatives()[is]<<"transRate1.derivatives()[is]"<<std::endl;
//            std::cout<<transRate2.derivatives()[is]<<"transRate2.derivatives()[is]"<<std::endl;
//            std::cout<<transRate3.derivatives()[is]<<"transRate3.derivatives()[is]"<<std::endl;

        }
        else{
            subIntIdx=intIdx+1;
            rs0+=transRate1.derivatives()[subIntIdx]*y3+spontDec1*y3-transRate1.derivatives()[subIntIdx]*y0;
            rs1+=transRate3.derivatives()[subIntIdx]*y2+spontDec3*y2+transRate2.derivatives()[subIntIdx]*y3+spontDec2*y3-transRate3.derivatives()[subIntIdx]*y1-transRate2.derivatives()[subIntIdx]*y1;
            rs2+=transRate3.derivatives()[subIntIdx]*y1-transRate3.derivatives()[subIntIdx]*y2-spontDec3*y2-nonResonantTransitionRate1*y2;
            rs3+=transRate1.derivatives()[subIntIdx]*y0+transRate2.derivatives()[subIntIdx]*y1-transRate1.derivatives()[subIntIdx]*y3-transRate2.derivatives()[subIntIdx]*y3-spontDec1*y3+spontDec2*y3-nonResonantTransitionRate2*y3;
            rs4+=0;
//            std::cout<<"================================"<<std::endl;
//            std::cout<<transRate1.derivatives()[subIntIdx]<<"transRate1.derivatives()[subIntIdx]"<<std::endl;
//            std::cout<<transRate2.derivatives()[subIntIdx]<<"transRate2.derivatives()[subIntIdx]"<<std::endl;
//            std::cout<<transRate3.derivatives()[subIntIdx]<<"transRate3.derivatives()[subIntIdx]"<<std::endl;
        }
        NV_Ith_S(resvalS[is],0) = rs0;
        NV_Ith_S(resvalS[is],1) = rs1;
        NV_Ith_S(resvalS[is],2) = rs2;
        NV_Ith_S(resvalS[is],3) = rs3;
        NV_Ith_S(resvalS[is],4) = rs4;
    }



    return 0;
}

int fitterSystem::fitterIDASDAESystem(realtype tres, N_Vector IDAS_y, N_Vector IDAS_yp, N_Vector rr, void *user_data){
    realtype *yval, *ypval, *rval;
    realtype t=tres;
    yval = NV_DATA_S(IDAS_y);
    ypval = NV_DATA_S(IDAS_yp);
    rval = NV_DATA_S(rr);

    struct fitterSystem::fitterParameters* data;
    data =(struct fitterSystem::fitterParameters*) user_data;

    std::vector< std::vector<double>> spontaneousDecayRateVectors=data->spontaneousDecayRateVectors;
    std::vector< std::vector<double>> laserPulseProperties=data->laserPulseProperties;
    std::vector<std::vector<double>> alphasAndBetas=data->alphasAndBetas;
    std::vector<int> numberOfInitialStates=data->numberOfInitialStates;
    std::vector<std::vector<int>> finalStateIds=data->finalStateIds;
    std::vector<std::vector<std::vector<int>>> orderedAllowedStates=data->orderedAllowedStates;
    int ionizisedStateNo=data->ionizedStateId;
    realtype totalPopulation=data->totalPopulation;
    realtype aCoefficient=data->ACoefficients[0];
    int numberOfParameters=data->numberOfParameters;
    int numberOfTransitions=data->numberOfTransitions;
    std::vector<double> gauss;
    for(const auto &pulse: laserPulseProperties){
        gauss.push_back(data->getGaussianLineShape(t, pulse[0], pulse[1]));
    }
    rval[ionizisedStateNo]=0;
    realtype transitionRate=0;
    realtype spontaneousDecayRate=0;
    int step=0;
    int initialStateNo=0;
    int finalStateNo=0;
    finalStateNo=numberOfInitialStates[step];
    int transitionCounter=0;
    for(const auto &finalStateId_r: finalStateIds[step]){
        finalStateNo=numberOfInitialStates[step]+finalStateId_r;
        transitionRate=data->ionizingTransitionRate*gauss[1];
        rval[finalStateNo]=0;
        rval[finalStateNo]-=transitionRate*yval[finalStateNo];
        rval[finalStateNo]-=ypval[finalStateNo];
        rval[ionizisedStateNo]+=yval[finalStateNo];
    }
    transitionCounter=0;
    //Generate transitions from initial to final F -states
    for(const auto &finalStateIds_r: orderedAllowedStates[step]){
        rval[initialStateNo]=0;
        for(const auto &finalStateId_r: finalStateIds_r){
            finalStateNo=numberOfInitialStates[step]+finalStateId_r;
            realtype initialAlpha=alphasAndBetas[0][transitionCounter];
            realtype initialBeta=alphasAndBetas[1][transitionCounter];
            realtype finalAlpha=alphasAndBetas[2][transitionCounter];
            realtype finalBeta=alphasAndBetas[3][transitionCounter];
            realtype relativeIntensity=data->p[(numberOfParameters-numberOfTransitions)+transitionCounter];
            realtype numberOfTransitions=data->calculateNumberOfTransitions(data->laserFrequency, relativeIntensity, data->p[6],data->p[5],data->p[9],
                    data->p[4],data->p[0], data->p[1], data->p[2],data->p[3],
                    initialAlpha, initialBeta, finalAlpha, finalBeta, aCoefficient, gauss[step]);
            transitionRate=numberOfTransitions;
            spontaneousDecayRate=spontaneousDecayRateVectors[step][transitionCounter];
            rval[initialStateNo]+=(transitionRate)*yval[finalStateNo]-(transitionRate)*yval[initialStateNo]+spontaneousDecayRate*yval[finalStateNo];
            rval[finalStateNo]+=(transitionRate)*yval[initialStateNo]-(transitionRate)*yval[finalStateNo]-spontaneousDecayRate*yval[finalStateNo];
            transitionCounter++;
        }
        rval[initialStateNo]-=ypval[initialStateNo];
        rval[ionizisedStateNo]+=yval[initialStateNo];
        initialStateNo++;
    }
    rval[ionizisedStateNo]+=yval[ionizisedStateNo];
    rval[ionizisedStateNo]-=totalPopulation;
    return 0;
}

int fitterSystem::fitterIDASDAESystemJacobi(long Neq, realtype tt, realtype cj, N_Vector IDAS_y, N_Vector IDAS_yp, N_Vector resvec, DlsMat JJ, void *user_data, N_Vector tempv1, N_Vector tempv2, N_Vector tempv3){
    realtype *yval;
    realtype t=tt;
    yval = NV_DATA_S(IDAS_y);

    struct fitterSystem::fitterParameters* data;
    data =(struct fitterSystem::fitterParameters*) user_data;
    std::vector< std::vector<double>> spontaneousDecayRateVectors=data->spontaneousDecayRateVectors;
    std::vector< std::vector<double>> laserPulseProperties=data->laserPulseProperties;
    std::vector<std::vector<double>> alphasAndBetas=data->alphasAndBetas;
    std::vector<int> numberOfInitialStates=data->numberOfInitialStates;
    std::vector<std::vector<int>> finalStateIds=data->finalStateIds;
    std::vector<std::vector<int>> initialStateIds=data->initialStateIds;
    std::vector<std::vector<std::vector<int>>> orderedAllowedStates=data->orderedAllowedStates;
    int ionizisedStateNo=data->ionizedStateId;
    realtype aCoefficient=data->ACoefficients[0];
    int numberOfParameters=data->numberOfParameters;
    int numberOfTransitions=data->numberOfTransitions;
    std::vector<double> gauss;
    for(const auto &pulse: laserPulseProperties){
        gauss.push_back(data->getGaussianLineShape(t, pulse[0], pulse[1]));
    }
    realtype transitionRate=0;
    realtype spontaneousDecayRate=0;
    int step=0;
    int initialStateNo=0;
    int initialStateNo_next=0;
    int finalStateNo_next=0;
    int finalStateNo=0;
    finalStateNo=numberOfInitialStates[step];
    int transitionCounter=0;
    int initialStateCounter=0;
    for(const auto &finalStateId_r: finalStateIds[step]){
        finalStateNo=numberOfInitialStates[step]+finalStateId_r;
        for(const auto &finalStateId_r_next: finalStateIds[step]){
            finalStateNo_next=numberOfInitialStates[step]+finalStateId_r_next;
            IJth(JJ,finalStateNo_next, finalStateNo)=0;
            IJth(JJ,finalStateNo, finalStateNo_next)=0;
        }
        IJth(JJ,finalStateNo, finalStateNo)=0;
        transitionRate=data->ionizingTransitionRate*gauss[1];
        IJth(JJ,finalStateNo, finalStateNo)-=transitionRate;
        IJth(JJ,finalStateNo,ionizisedStateNo)=0;
        IJth(JJ,ionizisedStateNo,finalStateNo)=1;
        IJth(JJ,finalStateNo, finalStateNo)-=cj;
    }

    transitionCounter=0;
    for(const auto &finalStateIds_r: orderedAllowedStates[step]){
        initialStateNo=initialStateCounter;
        for(const auto &initialStateNo_r_next: initialStateIds[step]){
            initialStateNo_next=initialStateNo_r_next;
            IJth(JJ,initialStateNo_next, initialStateNo)=0;
            IJth(JJ,initialStateNo, initialStateNo_next)=0;
        }
        for(const auto &finalStateId_r: finalStateIds_r){
            finalStateNo=numberOfInitialStates[step]+finalStateId_r;
            realtype initialAlpha=alphasAndBetas[0][transitionCounter];
            realtype initialBeta=alphasAndBetas[1][transitionCounter];
            realtype finalAlpha=alphasAndBetas[2][transitionCounter];
            realtype finalBeta=alphasAndBetas[3][transitionCounter];
            realtype relativeIntensity=data->p[(numberOfParameters-numberOfTransitions)+transitionCounter];
            realtype numberOfTransitions=data->calculateNumberOfTransitions(data->laserFrequency, relativeIntensity, data->p[6],data->p[5],data->p[9],
                    data->p[4],data->p[0], data->p[1], data->p[2],data->p[3],
                    initialAlpha, initialBeta, finalAlpha, finalBeta, aCoefficient, gauss[step]);
            transitionRate=numberOfTransitions;
            spontaneousDecayRate=spontaneousDecayRateVectors[step][transitionCounter];
            IJth(JJ,finalStateNo, initialStateNo)=0;
            IJth(JJ,initialStateNo, initialStateNo)-=transitionRate;
            IJth(JJ,initialStateNo, finalStateNo)+=(transitionRate+spontaneousDecayRate) ;
            IJth(JJ,finalStateNo, initialStateNo)+=(transitionRate);
            IJth(JJ,finalStateNo, finalStateNo)-=(transitionRate+spontaneousDecayRate);
            transitionCounter++;
        }
        IJth(JJ,initialStateNo,ionizisedStateNo)=0;
        IJth(JJ,ionizisedStateNo,initialStateNo)=1;
        IJth(JJ,initialStateNo, initialStateNo)-=cj;
        initialStateCounter++;
    }
    IJth(JJ,ionizisedStateNo, ionizisedStateNo)=1;
    return 0;
}

