//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef HYPERFINESTATEPROPERTIES_H
#define HYPERFINESTATEPROPERTIES_H

#include <map>
#include <vector>

//A class to store properties of a particular hyperfine state
class hyperFineStateProperties
{
public:
    explicit hyperFineStateProperties(std::string stateId_v=" ");
    ~hyperFineStateProperties();

    //Set:
    void setN(int n_v);
    void setTotalFStatesAtJ(int totalFStatesAtJ_v);
    void setIsInitialState(bool isInitialState_v);
    void setAlphaAndBeta(double alpha_v, double beta_v);
    void setFI(double F_v, double J_v);
    void addAllowedTransition(std::string finalStateId_v, double relInt_V);
    //Get:
    std::string getStateId() const;
    int getN() const;
    int getTotalFStatesAtJ() const;
    int getNumberOfAllowedTransitionsFromThisState();
    bool getIsInitialState() const;
    double getAlpha() const;
    double getBeta() const;
    double getF() const;
    double getJ() const;
    double getIntensity_fromMap(std::string finalStateId);
    double getIntensity_fromVector(int transition);

    std::map<std::string, double> getAllowedTransitionIntensities() const;
private:
    std::string stateId;    // x[0], x[1],...x[n], where n is an integer not related to F
                            //The notation is used to allow easy rate-equation consruction.
                            // the n=0 corresponds to state with F=|iI+J|
    int n;              //the n from stateId
    int totalFStatesAtJ;//The total number of F states associated with J
    bool isInitialState;//true if state belongs to the initial hyperfine states.
    double alpha;       //the calculated hyperfine components
    double beta;        //--||--
    double F;           //F
    double J;           //J
    std::map<std::string, double> allowedTransitionIntensities;  //Allowed transitions from the particular state
                                                        //to state denoted in QString (x[i]->x[f])
                                                        //and the associated relative transition strength
    std::vector<double> allowedTransitionIntensities_sorted;
};

#endif // HYPERFINESTATEPROPERTIES_H
