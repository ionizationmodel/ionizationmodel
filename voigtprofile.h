//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef VOIGTPROFILE_H
#define VOIGTPROFILE_H
#include <math.h>
#include <valuecontainerclass.h>
class voigtProfile
{
public:
    voigtProfile(valueContainerClass* valueContainer_p){
        valueContainer=valueContainer_p;
        c= valueContainer->c;
        pi =valueContainer->pi;
        hBar=valueContainer->hBar;
        ln2=valueContainer->ln2;
    }

    ~voigtProfile(){

    }
    //Voigt profile and derivatives adapted from
    //A.B. McLean, C.E.J. Mitchell, D.M. Swanston,
    //Implementation of an efficient analytical approximation to the Voigt function for photoemission lineshape analysis,
    //Journal of Electron Spectroscopy and Related Phenomena,
    //Volume 69, Issue 2, 29 September 1994, Pages 125-132, ISSN 0368-2048, 10.1016/0368-2048(94)02189-7.
    //(http://www.sciencedirect.com/science/article/pii/0368204894021897)
    // Keywords: Accuracy; Approximation; Lineshape analysis; Novel; PC; Voigt
    double getVoigtLineShape(double resonanceFrequency_v, double lorentzianWidth_v, double gaussianWidth_v, double laserFrequency_v){
        //Change values to Hz
        double resonanceFrequency=resonanceFrequency_v*1E6;
        double lorentzianWidth=lorentzianWidth_v*1E6;
        double gaussianWidth=gaussianWidth_v*1E6;
        double laserFrequency=laserFrequency_v*1E6;
        return voigtProfileFunction(resonanceFrequency, lorentzianWidth, gaussianWidth, laserFrequency);
    }

    //Inputs in MHz here
    template<typename Scalar, typename parameterVector, typename outputVector, typename individualOutputVector>
    void getVoigtProfile(const parameterVector &x_r, outputVector &fvec_r,
                         individualOutputVector &individualOuputs_r,
                         const std::vector<int> transitionIds_r,
                         const std::vector<double> &frequencyRange_r,
                         const std::vector<double> &measuredProfile_r,
                         const std::vector<std::vector<double>> &alphasAndBetas_r,
                         bool calculateResidual_b, int numberOfParameters_v){
        calculateVoigtProfile<Scalar,parameterVector,outputVector,individualOutputVector>(x_r, fvec_r,individualOuputs_r, transitionIds_r, frequencyRange_r,
                                                                                          measuredProfile_r, alphasAndBetas_r,
                                                                                          calculateResidual_b,numberOfParameters_v);
    }

    template<typename Scalar=double>
    Scalar getTransitionFrequency(Scalar CoG_v, Scalar initialA_v,  Scalar initialB_v, Scalar finalA_v, Scalar finalB_v,
                                  double initialAlpha_v,  double initialBeta_v, double finalAlpha_v, double finalBeta_v){
        Scalar transFreq=CoG_v+finalA_v*finalAlpha_v+finalB_v*finalBeta_v-initialA_v*initialAlpha_v-initialB_v*initialBeta_v;
        return transFreq;
    }
private:
    double pi;
    double ln2;
    double c;
    double hBar;
    valueContainerClass* valueContainer;
    template<typename Scalar=double>
    Scalar voigtProfileFunction(Scalar resonanceFrequency_v, Scalar lorentzianWidth_v, Scalar gaussianWidth_v, double laserFrequency_v){
        int i;
        double A[4],B[4],C[4],D[4];
        Scalar V=0;
        Scalar retVal=0;
        static double sqrtln2=sqrt(valueContainer->ln2);
        static double sqrtpi=sqrt(valueContainer->pi);
        Scalar X=(laserFrequency_v-resonanceFrequency_v)*2*sqrtln2/gaussianWidth_v;
        Scalar Y=lorentzianWidth_v*sqrtln2/gaussianWidth_v;
        A[0]=-1.2150; B[0]= 1.2359;
        A[1]=-1.3509; B[1]= 0.3786;
        A[2]=-1.2150; B[2]=-1.2359;
        A[3]=-1.3509; B[3]=-0.3786;
        C[0]=-0.3085; D[0]= 0.0210;
        C[1]= 0.5906; D[1]=-1.1858;
        C[2]=-0.3085; D[2]=-0.0210;
        C[3]= 0.5906; D[3]= 1.1858;
        for(i=0;i <= 3;i++){
            V=V+(C[i]*(Y-A[i])+D[i]*(X-B[i]))/(((Y-A[i])*(Y-A[i]))+((X-B[i])*(X-B[i])));
        }
        retVal=((lorentzianWidth_v*(1/(valueContainer->pi*lorentzianWidth_v))*sqrtpi*sqrtln2/(gaussianWidth_v))*V);
        return retVal;
    }
    //Returns number of transitions taking place per laser pulse at a giver laser frequency
    template<typename Scalar=double>
    Scalar calculateNumberOfTransitions(double laserFrequency_v, Scalar resonanceFrequency_v, Scalar relativeIntensity_v,Scalar lorentzianWidth_v, Scalar gaussianWidth_v){
        Scalar tF=resonanceFrequency_v*1E6;                                                                                        //transition frequency to Hz
        Scalar lorentzianWidth=lorentzianWidth_v*1E6;
        Scalar gaussianWidth=gaussianWidth_v*1E6;
        double laserFrequency=laserFrequency_v*1E6;
        Scalar resWl=(valueContainer->c/tF);                                                                                                        //resonance wavelength in m
        //transition rate taking into account the spontaneous emission rate stimulatedTransitionRate [1/s]",
        //laser intensity [J/m^2]  and spectralfunction(relative intensity and other inputs in MhZ's)
        double lInt= valueContainer->getModel_laserIntensity()*0.01;                                                                                      //[muJ/cm^2] to [J/m^2]
        Scalar photonEnergy=tF*hBar;                                                                                               //[J]
        Scalar photonIntensity=lInt/photonEnergy;                                                                                                   //[photons/m^2]
        double refractiveIndex=1;                                                                                                                   //not needed here atm.
        Scalar absorptionCrossSectionStatic=(valueContainer->getACoefficient_1()/(2*pi))*(pow(resWl,2)/(4*pow(refractiveIndex,2)));                            //[m^2/s]
        Scalar absorptionCrossSectionDynamic=relativeIntensity_v*voigtProfileFunction<Scalar>(tF, lorentzianWidth, gaussianWidth, laserFrequency);  //[s]
        Scalar absorptionCrossSection=absorptionCrossSectionStatic*absorptionCrossSectionDynamic;                                                   //[m^2]
        Scalar relNoOfTrans=absorptionCrossSection*photonIntensity;                                                                                 //[1] //Total number of transitions per pulse
        return relNoOfTrans;
    }
    template<typename Scalar, typename parameterVector, typename outputVector, typename individualOutputVector>
    void calculateVoigtProfile(const parameterVector &x_r, outputVector &fvec_r,individualOutputVector &individualOuputs_r,
                               const std::vector<int> transitionIds_r, const std::vector<double> &frequencyRange_r, const std::vector<double> &measuredProfile_r,
                               const std::vector<std::vector<double>> &alphasAndBetas_r, bool calculateResidual,
                               int numberOfParameters_v){
        //["initialA"]=x[0];
        //["initialB"]=x[1];
        //["finalA"]=x[2];
        //["finalB"]=x[3];
        //["CoG"]=x[4];
        //["gaussianWidth"]=x[5];
        //["lorentzianWidth"]=x[6];
        //["intensityScaler"]=x[7];
        //["backgroundScaler"]=x[8];
        double initialAlpha;
        double initialBeta;
        double finalAlpha;
        double finalBeta;
        double mP=0;
        double sigma=1;
        int frequencyPointCounter=0;
        int numberOfTransitions=transitionIds_r.size();
        int indSize=individualOuputs_r.size();
        for(const auto &laserFrequency:frequencyRange_r){
            Scalar summedTransRate=0;
            for(const auto &t: transitionIds_r){
                //Calculate transition frequency
                initialAlpha=alphasAndBetas_r[0][t];
                initialBeta=alphasAndBetas_r[1][t];;
                finalAlpha=alphasAndBetas_r[2][t];
                finalBeta=alphasAndBetas_r[3][t];
                Scalar relInt=x_r[(numberOfParameters_v-numberOfTransitions)+t];
                //Calculate number of transitions
                Scalar tF=getTransitionFrequency<Scalar>(x_r[4],x_r[0],x_r[1],x_r[2],x_r[3],
                        initialAlpha, initialBeta, finalAlpha, finalBeta);
                Scalar lorentzianWidth=x_r[6];
                Scalar gaussianWidth=x_r[5];
                Scalar relNoOfTrans=calculateNumberOfTransitions<Scalar>(laserFrequency,tF, relInt,lorentzianWidth,gaussianWidth);

                if(indSize==numberOfTransitions){
                    individualOuputs_r[t].push_back(relNoOfTrans);
                }
                summedTransRate+=relNoOfTrans;
            }
            summedTransRate=summedTransRate*x_r[7]+x_r[8];
            if(calculateResidual){
                mP=measuredProfile_r.at(frequencyPointCounter);
                if(mP!=0){
                    sigma=sqrt(mP);
                }
                else{
                    sigma=1;
                }
                fvec_r[frequencyPointCounter]=(summedTransRate-mP)/sigma;
            }
            else{
                fvec_r[frequencyPointCounter]=summedTransRate;
            }
            frequencyPointCounter++;
        }

  }


        //same as above but for the ionizing non-resonant transition
        double calculateNumberOfIonizingStepTransitions(){
            double laserFreq=(c/(valueContainer->getWl_nr()*1E-9));                                                                                     //[Hz]
            double photonEnergy=laserFreq*hBar*2*pi;                                                                                                    //[J]
            double photonIntensity=valueContainer->getI_nr()*(1E-6/photonEnergy);                                                                       //[photons/cm^2]
            double transitionsPerPulse_nr=valueContainer->getCS_nr()*photonIntensity;                                                                   //[1]
            return transitionsPerPulse_nr;
        }


    };

#endif // VOIGTPROFILE_H
