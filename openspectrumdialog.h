//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef OPENSPECTRUMDIALOG_H
#define OPENSPECTRUMDIALOG_H

#include <QDialog>
#include <mainplot.h>
#include <QFileDialog>
#include <QPair>
#include <QDebug>
#include <QListWidget>
#include <arraysort.h>
#include <valuecontainerclass.h>
#include <3rdParty/fast-cpp-csv-parser/csv.h>

namespace Ui {
class openSpectrumDialog;
}

class openSpectrumDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit openSpectrumDialog(QWidget *parent = 0,valueContainerClass *valueContainer_p=0, bool testmode_v=false);
    ~openSpectrumDialog();
    void testModeFileOpen();

signals:
    void outputScanSet(QPair<QVector<double>, QVector<double> > newOutputScan);
public slots:
    void listDirectory();
    void showUpUi();
    void hideUi();
private slots:
    void fileListItemSelectionChanged(const QString &currentText);
private:
    Ui::openSpectrumDialog *ui;
    mainPlot *hfsPlot;
    arraySort *sortArray;
    valueContainerClass *valueContainer;

    QString homeDir;
    QString path;
    QDir directory;
    QString selectedFile;

    int xCol;
    int yCol;
    int standardColumnNo;
    int rowSizeOfFirstSelection;
    QStringList columnSeparator;

    bool testmode;   //testing mode for fitter, true if on

    //The contents of the seletced files, all columns
    QVector<QVector<QStringList> > selectedFileContent;
    //If the files are similar and splittable, this holds, the splits of the selected files, all columns
    QVector<QVector<QVector<double> > >  splitsOfSelectedFilesOrig;
    //Single scan, with WL/Freq. and Counts column, harmonics applied
    QVector< QPair<QVector<double>, QVector<double> > > singleScans;
    //Combined splits, with binning if binSize>0, WL/Freq. and Counts column. harmonics applied
    QPair<QVector<double>, QVector<double> > combinedScans;
    //output vector pair
    QPair<QVector<double>, QVector<double> > measuredProfile;

    void openFiles(QString filename_v, QString sep_v);
    QVector<QVector<double> > prepareColumnVectors(int noOfColumns);
    int checkRowSize(QStringList firstRow);

    //Modifier functions
    double convertToMHz(double value);
    double setHarmonic(double value);

    //Fundamental Constants
    double c;
};

#endif // OPENSPECTRUMDIALOG_H
