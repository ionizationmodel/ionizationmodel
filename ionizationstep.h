//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef IONIZATIONSTEP_H
#define IONIZATIONSTEP_H
#include <vector>
#include <hyperfinestateproperties.h>
#include <memory>
#include <iostream>
#include <algorithm>
#include <vector>

class ionizationStep
{
public:
    //Class holding the information of a single Ji->Jf transition.
    ionizationStep(int Ji_v, int Jf_v);
    ~ionizationStep();
    int getId() const;

    void setJi(int newJi);
    int getJi() const;

    void setJf(int newJf);
    int getJf() const;

    void setNumberOfTransitions_ordered(std::vector<std::vector<double>> newNumberOfTransitions_ordered);
    std::vector<std::vector<double>> getNumberOfTransitions_ordered();

    void setACoefficient(double newACoefficient);
    double getACoefficient() const;

    void setLaserPulseWidth(double newLaserPulseWidth);
    double getLaserPulseWidth() const;

    void setLaserPulseDelay(double newLaserPulseDelay);
    double getLaserPulseDelay() const;

    void setGaussianWidth(double newGaussianWidth);
    double getGaussianWidth() const;

    void setLorentzianWidth(double newLorentzianWidth);
    double getLorentzianWidth() const;

    void setLaserPulseIntensity(double newLaserPulseIntensity);
    double getLaserPulseIntensity() const;

    void setCenterOfGravity(double newCenterOfGravity);
    double getCenterOfGravity() const;

    void setInitialHyperFineStates(std::unique_ptr<hyperFineStateProperties>state, bool replace=false, int idx=0);
    void setFinalHyperFineStates(std::unique_ptr<hyperFineStateProperties>state, bool replace=false, int idx=0);

    std::unique_ptr<hyperFineStateProperties> getInitialHyperFineState(int state);
    std::unique_ptr<hyperFineStateProperties> getFinalHyperFineState(int state);

    int getNumberOfInitialStates();
    int getNumberOfFinalStates();

    std::vector<double> getInitialFs();
    std::vector<double> getFinalFs();

    void setInitialAlpha(double value);
    void setInitialBeta(double value);
    void setFinalAlpha(double value);
    void setFinalBeta(double value);

    double getInitialAlpha(int transition);
    double getInitialBeta(int transition);
    double getFinalAlpha(int transition);
    double getFinalBeta(int transition);

    double getRelativeIntensity(int transition);
    std::vector<double> getOrderedRelativeIntensities();

    void setAllowedTransition(double relativeIntensity);

    void setInitialStateId(int id);
    void setFinalStateId(int id);

    int getInitialStateId(int transition);
    int getFinalStateId(int transition);

    int getNumberOfAllowedTransitions();

    std::vector<int> getInitialStateIds();
    std::vector<int> getFinalStateIds();

private:
    hyperFineStateProperties *hypFiState;

    int Ji;
    int Jf;
    static int id;
    int localId;

    std::vector<std::vector<double>> numberOfTransitions_ordered;           //Inner vector holds the transition numbers |"Fi1->Ffn+1",..., "Fin->Ffn+n"> for
                                                                            //hyperfine transitions.
                                                                            //These are stored in the outer vector as a function of frequency.
    std::vector<double> relativeIntensities_ordered;                        //Relative intensities for hyperfine transitions as |"Fi1->Ffn+1",..., "Fin->Ffn+n">.

    double aCoefficient;                                                    //[1/s]
    double laserPulseWidth;                                                 //[ns]
    double laserPulseDelay;                                                 //[ns]

    bool isNonResonant;                                                     //True for non-resonant transitions

    double gaussianWidth;                                                   //[MHz]
    double lorentzianWidth;                                                 //[MHz]

    double laserPulseIntensity;                                             //[MHz]

    double centerOfGravity;                                                 //[MHz]

    std::vector<std::unique_ptr<hyperFineStateProperties>>   initialHyperfineStates_ordered; //States orderred from |J-F|,...,|J+F|
    std::vector<std::unique_ptr<hyperFineStateProperties>>   finalHyperfineStates_ordered;   //States orderred from |J-F|,...,|J+F|

    std::vector<int> initialStateIds;
    std::vector<int> finalStateIds;

    std::vector<double> initialAlphas;
    std::vector<double> initialBetas;
    std::vector<double> finalAlphas;
    std::vector<double> finalBetas;
};

#endif // IONIZATIONSTEP_H
