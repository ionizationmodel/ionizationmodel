#include <genericSystem.h>

/////////////////
//C++->C wrappers
/////////////////

extern "C" int call_referenceDAESystem(realtype tres, N_Vector yy, N_Vector yp, N_Vector rr, void *user_data) // wrapper function
{
    genericSystem* wrapper = (genericSystem *) user_data;
    return wrapper->referenceIDASDAESystem(tres, yy, yp, rr, user_data);
}

extern "C" int call_referenceDAESystemJacobi(long Neq, realtype tt, realtype cj, N_Vector yy, N_Vector yp, N_Vector resvec, DlsMat JJ, void *user_data, N_Vector tempv1, N_Vector tempv2, N_Vector tempv3) // wrapper function
{
    genericSystem* wrapper = (genericSystem *) user_data;
    return wrapper->referenceIDASDAESystemJacobi(Neq,tt, cj, yy, yp, resvec, JJ, user_data, tempv1, tempv2, tempv3);
}

extern "C" int call_referenceCVODESODESystem(realtype t, N_Vector CVODES_y, N_Vector CVODES_ydot, void *user_data){
    genericSystem* wrapper = (genericSystem *) user_data;
    return wrapper->referenceCVODESODESystem(t, CVODES_y,CVODES_ydot,user_data);
}

extern "C" int call_referenceCVODESODESystemJacobi(long int N, realtype t, N_Vector y, N_Vector fy, DlsMat J, void *user_data, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3){
  genericSystem* wrapper = (genericSystem *) user_data;
  return wrapper->referenceCVODESODESystemJacobi(N, t, y, fy, J, user_data, tmp1, tmp2, tmp3);
}

extern "C" int call_DAESystem(realtype tres, N_Vector yy, N_Vector yp, N_Vector rr, void *user_data) // wrapper function
{
    genericSystem* wrapper = (genericSystem *) user_data;
    return wrapper->IDASDAESystem(tres, yy, yp, rr, user_data);
}

extern "C" int call_DAESystemJacobi(long Neq, realtype tt, realtype cj, N_Vector yy, N_Vector yp, N_Vector resvec, DlsMat JJ, void *user_data, N_Vector tempv1, N_Vector tempv2, N_Vector tempv3) // wrapper function
{
    genericSystem* wrapper = (genericSystem *) user_data;
    return wrapper->IDASDAESystemJacobi(Neq,tt, cj, yy, yp, resvec, JJ, user_data, tempv1, tempv2, tempv3);
}

extern "C" int call_CVODESODESystem(realtype t, N_Vector CVODES_y, N_Vector CVODES_ydot, void *user_data){
    genericSystem* wrapper = (genericSystem *) user_data;
    return wrapper->CVODESODESystem(t, CVODES_y,CVODES_ydot,user_data);
}

extern "C" int call_CVODESODESystemJacobi(long int N, realtype t, N_Vector y, N_Vector fy, DlsMat J, void *user_data, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3){
  genericSystem* wrapper = (genericSystem *) user_data;
  return wrapper->CVODESODESystemJacobi(N, t, y, fy, J, user_data, tmp1, tmp2, tmp3);
}

void genericSystem::setupIDASSystem(){
    IDAS_mem = nullptr;                                                                                         //IDAS solver object.
    IDAS_y = IDAS_yp = IDAS_abstol = IDAS_constraints= nullptr;                                                 //Initial conditions for y, dy and absolute tolerences.
    IDAS_yval = IDAS_ypval = IDAS_atval = IDAS_yconst= nullptr;                                                 //N-vector representation of the data above.
    IDAS_yS, IDAS_ypS= nullptr;                                                                                 //Sensitivity vectors
    int numberOfEquations=parameters->getNumberOfEquations();
    /* Allocate N-vectors. */
    IDAS_y = N_VNew_Serial(numberOfEquations);
    //if(check_flag((void *)yy, "N_VNew_Serial", 0)) return(1);
    IDAS_yp = N_VNew_Serial(numberOfEquations);
    //if(check_flag((void *)yp, "N_VNew_Serial", 0)) return(1);
    IDAS_abstol = N_VNew_Serial(numberOfEquations);
    //if(check_flag((void *)avtol, "N_VNew_Serial", 0)) return(1);
    //constraints
    IDAS_constraints=N_VNew_Serial(numberOfEquations);
    /* Create and initialize  y, y', and absolute tolerance vectors. */
    IDAS_yval  = NV_DATA_S(IDAS_y);
    IDAS_ypval = NV_DATA_S(IDAS_yp);
    IDAS_atval = NV_DATA_S(IDAS_abstol);
    N_VConst(RCONST(1.0), IDAS_constraints);
    for(unsigned int i=0; i<parameters->m_initialPopulations.size();i++){
        IDAS_yval[i]=parameters->m_initialPopulations.at(i);
        IDAS_ypval[i]=parameters->m_initialTransitionRates.at(i);
        IDAS_atval[i]=parameters->m_absoluteTolerances.at(i);
    }
    /* Integration limits */
    IDAS_t0 = 0;
    IDAS_tout1 = RCONST(parameters->m_laserPulseProperties[0][0]*parameters->m_integrationTimeScaler);
    /*Relative tolerance*/
    IDAS_rtol=parameters->m_relativeTolerance;
    /* Call IDACreate and IDAInit to initialize IDA memory */
    IDAS_mem = IDACreate();
    IDAInit(IDAS_mem, call_DAESystem, IDAS_t0, IDAS_y, IDAS_yp);
    IDASVtolerances(IDAS_mem, IDAS_rtol, IDAS_abstol);
    IDASetUserData(IDAS_mem, parameters);
    /* Call IDADense and set up the linear solver. */
    IDAS_retval = IDADense(IDAS_mem, numberOfEquations);
    if(parameters->m_setUserJacobian){
        IDAS_retval = IDADlsSetDenseJacFn(IDAS_mem, call_DAESystemJacobi);
    }
    if(parameters->m_setConstraints){
        IDASetConstraints(IDAS_mem, IDAS_constraints);
    }
    IDASetNonlinConvCoef(IDAS_mem,parameters->m_setNonlinConvCoef);
    IDASetMaxNumSteps(IDAS_mem, parameters->m_MaxNumSteps);

    //Results
    resultTimeinterval=IDAS_tout1/parameters->m_timePoints;
    timePoints.reserve(parameters->m_timePoints);
    numberOfIntegrationSteps.reserve(parameters->m_timePoints);
    timeDependentStatePopulationVectors.resize(numberOfEquations);
    statePopulations.reserve(parameters->m_timePoints);
}

void genericSystem::setupCVODESSystem(){
    CVODES_mem=nullptr;
    CVODES_y = CVODES_abstol=nullptr;
    int numberOfEquations=parameters->getNumberOfEquations();

    /* Allocate N-vectors. */
    CVODES_y=N_VNew_Serial(numberOfEquations);
    CVODES_abstol=N_VNew_Serial(numberOfEquations);

    for(unsigned int i=0; i<parameters->m_initialPopulations.size();i++){
        NV_Ith_S(CVODES_y,i)=parameters->m_initialPopulations.at(i);
        NV_Ith_S(CVODES_abstol,i)=parameters->m_absoluteTolerances.at(i);
    }
    /* Integration limits */
    CVODES_t0 = 0;
    CVODES_tout1 = RCONST(parameters->m_laserPulseProperties[0][0]*parameters->m_integrationTimeScaler);
    /*Relative tolerance*/
    CVODES_reltol=parameters->m_relativeTolerance;

    CVODES_mem = CVodeCreate(CV_BDF, CV_NEWTON);
    CVodeInit(CVODES_mem, call_CVODESODESystem, CVODES_t0, CVODES_y);
    CVodeSVtolerances(CVODES_mem, CVODES_reltol, CVODES_abstol);
    CVodeSetUserData(CVODES_mem, parameters);
    CVDense(CVODES_mem, numberOfEquations);
    if(parameters->m_setUserJacobian){
        CVDlsSetDenseJacFn(CVODES_mem, call_CVODESODESystemJacobi);
    }

    //Results
    resultTimeinterval=IDAS_tout1/parameters->m_timePoints;
    timePoints.reserve(parameters->m_timePoints);
    numberOfIntegrationSteps.reserve(parameters->m_timePoints);
    timeDependentStatePopulationVectors.resize(numberOfEquations);

    statePopulations.reserve(parameters->m_timePoints);
}

void genericSystem::runIDASSystem(){
    IDAS_yval  = NV_DATA_S(IDAS_y);
    IDAS_tout=resultTimeinterval;
    long int nst;
    int timePointCounter=0;
    while(IDAS_tout<=IDAS_tout1){
        statePopulations.clear();
        if(IDASolve(IDAS_mem, IDAS_tout, &IDAS_tret, IDAS_y, IDAS_yp, IDA_NORMAL)!=0){
            break;
        }
        int stateCounter=0;
        for(auto & stateVector : timeDependentStatePopulationVectors){
            stateVector.push_back(IDAS_yval[stateCounter]);
            stateCounter++;
        }
        IDAGetNumSteps(IDAS_mem, &nst);
        timePoints.push_back(IDAS_tret);
        numberOfIntegrationSteps.push_back(static_cast<double>(nst));
        IDAS_tout+=resultTimeinterval;
        timePointCounter++;
    }
}

void genericSystem::runCVODESSystem(){
    CVODES_tout=resultTimeinterval;
    long int nst;
    int timePointCounter=0;
    while(CVODES_tout<CVODES_tout1){
       statePopulations.clear();
       CVode(CVODES_mem, CVODES_tout, CVODES_y, &CVODES_t, CV_NORMAL);
       int stateCounter=0;
       for(auto & stateVector : timeDependentStatePopulationVectors){
           stateVector.push_back(NV_Ith_S(CVODES_y,stateCounter));
           double vv=(NV_Ith_S(CVODES_y,stateCounter));
           stateCounter++;
       }
       CVodeGetNumSteps(CVODES_mem, &nst);
       timePoints.push_back(CVODES_t);
       numberOfIntegrationSteps.push_back(static_cast<double>(nst));
       CVODES_tout+=resultTimeinterval;
       timePointCounter++;
    }
}

void genericSystem::cleanIDASSystem(){
    N_VDestroy_Serial(IDAS_y);
    N_VDestroy_Serial(IDAS_yp);
    /* Free abstol */
    N_VDestroy_Serial(IDAS_abstol);
    N_VDestroy_Serial(IDAS_constraints);
    IDAFree(&IDAS_mem);
}

void genericSystem::cleanCVODESSystem(){
    N_VDestroy_Serial(CVODES_y);
    N_VDestroy_Serial(CVODES_abstol);
    CVodeFree(&CVODES_mem);
}

////////////////
/// CVODES ODE
////////////////
int genericSystem::referenceCVODESODESystem(realtype t, N_Vector CVODES_y, N_Vector CVODES_ydot, void *user_data)
{
//    x[1]->x[3]:  Initial F: 1 Final F: 1 Transition intensity: 0.5
//    x[0]->x[3]:  Initial F: 0 Final F: 1 Transition intensity: 0.25
//    x[1]->x[2]:  Initial F: 1 Final F: 0 Transition intensity: 0.25
    realtype y0, y1, y2, y3, y4;
    y0=NV_Ith_S(CVODES_y,0);
    y1=NV_Ith_S(CVODES_y,1);
    y2=NV_Ith_S(CVODES_y,2);
    y3=NV_Ith_S(CVODES_y,3);
    y4=NV_Ith_S(CVODES_y,4);
    std::vector< std::vector<double>> numberOfTransitionsVectors=((struct genericSystem::genericParameters*)user_data)->m_numberOfTransitionsVectors;
    std::vector< std::vector<double>> spontaneousDecayRateVectors=((struct genericSystem::genericParameters*)user_data)->m_spontaneousDecayRateVectors;
    std::vector< std::vector<double>> laserPulseProperties=((struct genericSystem::genericParameters*)user_data)->m_laserPulseProperties;
    std::vector<double> gauss;
    gauss.push_back(((struct genericSystem::genericParameters*)user_data)->getGaussianLineShape(t, laserPulseProperties[0][0], laserPulseProperties[0][1]));
    gauss.push_back(((struct genericSystem::genericParameters*)user_data)->getGaussianLineShape(t, laserPulseProperties[1][0], laserPulseProperties[1][1]));
    double spontDec1=spontaneousDecayRateVectors[0][0];
    double transRate1=numberOfTransitionsVectors[0][0]*gauss[0];

    double spontDec3=spontaneousDecayRateVectors[0][1];
    double transRate3=numberOfTransitionsVectors[0][1]*gauss[0];

    double spontDec2=spontaneousDecayRateVectors[0][2];
    double transRate2=numberOfTransitionsVectors[0][2]*gauss[0];

    double nonResonantTransitionRate1=numberOfTransitionsVectors[1][0]*gauss[1];
    double nonResonantTransitionRate2=numberOfTransitionsVectors[1][1]*gauss[1];

    //0
    NV_Ith_S(CVODES_ydot,0)=(transRate1+spontDec1)*y3-(transRate1)*y0;
    //1
    NV_Ith_S(CVODES_ydot,1)=(transRate3+spontDec3)*y2+(transRate2+spontDec2)*y3-(transRate3+transRate2)*y1;
    //2
    NV_Ith_S(CVODES_ydot,2)=(transRate3)*y1-(transRate3+spontDec3+nonResonantTransitionRate1)*y2;
    //3
    NV_Ith_S(CVODES_ydot,3)=(transRate1)*y0+(transRate2)*y1-(transRate1+transRate2+spontDec1+spontDec2+nonResonantTransitionRate2)*y3;
    //4
    NV_Ith_S(CVODES_ydot,4)=nonResonantTransitionRate1*y2+nonResonantTransitionRate2*y3;
    return 0;
}

int genericSystem::referenceCVODESODESystemJacobi(long int N, realtype t, N_Vector y, N_Vector fy, DlsMat J, void *user_data, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3){
    std::vector< std::vector<double>> numberOfTransitionsVectors=((struct genericSystem::genericParameters*)user_data)->m_numberOfTransitionsVectors;
    std::vector< std::vector<double>> spontaneousDecayRateVectors=((struct genericSystem::genericParameters*)user_data)->m_spontaneousDecayRateVectors;
    std::vector< std::vector<double>> laserPulseProperties=((struct genericSystem::genericParameters*)user_data)->m_laserPulseProperties;
    std::vector<double> gauss;
    gauss.push_back(((struct genericSystem::genericParameters*)user_data)->getGaussianLineShape(t, laserPulseProperties[0][0], laserPulseProperties[0][1]));
    gauss.push_back(((struct genericSystem::genericParameters*)user_data)->getGaussianLineShape(t, laserPulseProperties[1][0], laserPulseProperties[1][1]));
    double spontDec1=spontaneousDecayRateVectors[0][0];
    double transRate1=numberOfTransitionsVectors[0][0]*gauss[0];

    double spontDec2=spontaneousDecayRateVectors[0][1];;
    double transRate2=numberOfTransitionsVectors[0][1]*gauss[0];

    double spontDec3=spontaneousDecayRateVectors[0][2];;
    double transRate3=numberOfTransitionsVectors[0][2]*gauss[0];

    double nonResonantTransitionRate1=numberOfTransitionsVectors[1][0]*gauss[1];
    double nonResonantTransitionRate2=numberOfTransitionsVectors[1][1]*gauss[1];
    IJth(J,0,0) = -transRate1;
    IJth(J,1,0) = 0.0;
    IJth(J,2,0) = 0.0;
    IJth(J,3,0) = transRate1;
    IJth(J,4,0) = 0.0;

    IJth(J,0,1) = 0.0;
    IJth(J,1,1) = -transRate3-transRate2;
    IJth(J,2,1) = transRate3;
    IJth(J,3,1) = transRate2;
    IJth(J,4,1) = 0.0;

    IJth(J,0,2) = 0.0;
    IJth(J,1,2) = transRate3+spontDec3;
    IJth(J,2,2) = -transRate3-spontDec3-nonResonantTransitionRate1;
    IJth(J,3,2) = 0.0;
    IJth(J,4,2) = nonResonantTransitionRate1;

    IJth(J,0,3) = transRate1+spontDec1;
    IJth(J,1,3) = transRate2+spontDec2;
    IJth(J,2,3) = 0.0;
    IJth(J,3,3) = -transRate1-transRate2-spontDec1-spontDec2-nonResonantTransitionRate2;
    IJth(J,4,3) = nonResonantTransitionRate2;

    IJth(J,0,4) = 0.0;
    IJth(J,1,4) = 0.0;
    IJth(J,2,4) = 0.0;
    IJth(J,3,4) = 0.0;
    IJth(J,4,4) = 0.0;
    return 0;
}

int genericSystem::CVODESODESystem(realtype t, N_Vector CVODES_y, N_Vector CVODES_ydot, void *user_data){
    std::vector< std::vector<double>> numberOfTransitionsVectors=((struct genericSystem::genericParameters*)user_data)->m_numberOfTransitionsVectors;
    std::vector< std::vector<double>> spontaneousDecayRateVectors=((struct genericSystem::genericParameters*)user_data)->m_spontaneousDecayRateVectors;
    std::vector< std::vector<double>> laserPulseProperties=((struct genericSystem::genericParameters*)user_data)->m_laserPulseProperties;
    std::vector<int> numberOfInitialStates=((struct genericSystem::genericParameters*)user_data)->m_numberOfInitialStates;
    std::vector<int> numberOfFinalStates=((struct genericSystem::genericParameters*)user_data)->m_numberOfFinalStates;
    std::vector<std::vector<int>> finalStateIds=((struct genericSystem::genericParameters*)user_data)->m_finalStateIds;
    std::vector<std::vector<std::vector<int>>> orderedAllowedStates=((struct genericSystem::genericParameters*)user_data)->m_orderedAllowedStates;
    int ionizisedStateNo=((struct genericSystem::genericParameters*)user_data)->m_ionizedStateId;
    double totalPopulation=((struct genericSystem::genericParameters*)user_data)->m_totalPopulation;
    std::vector<double> gauss;
    for(int p=0;p<laserPulseProperties.size();p++){
        gauss.push_back(((struct genericSystem::genericParameters*)user_data)->getGaussianLineShape(t, laserPulseProperties[p][0], laserPulseProperties[p][1]));
    }
    double isSpontaenousEmissionOn = ((struct genericSystem::genericParameters*)user_data)->m_isSpontaenousEmissionOn;
    double onOff;
    if(isSpontaenousEmissionOn){
        onOff=1;
    }
    else{
        onOff=0;
    }
    //Clear "ionizing" state
    NV_Ith_S(CVODES_ydot, ionizisedStateNo)=0;
    double transitionRate=0;
    double spontaneousDecayRate=0;
    int step=0;
    int initialStateNo=0;
    int finalStateNo=0;
    finalStateNo=numberOfInitialStates[step];
    int transitionCounter=0;
    int initialStateCounter=0;
    //loop to generate dxdt'd for final-> initial meaning that the listOfInitialStateIds has been changed
    // to listOfFinalStateIds and vice versa.
    for(const auto &finalStateId_r: finalStateIds[step]){
        finalStateNo=numberOfInitialStates[step]+finalStateId_r;
        transitionRate=numberOfTransitionsVectors[1][finalStateId_r]*gauss[1];
        spontaneousDecayRate=spontaneousDecayRateVectors[step][transitionCounter]*onOff;
        NV_Ith_S(CVODES_ydot,finalStateNo)=0;
        NV_Ith_S(CVODES_ydot,finalStateNo)-=(transitionRate)*NV_Ith_S(CVODES_y,finalStateNo);
        NV_Ith_S(CVODES_ydot,ionizisedStateNo)+=NV_Ith_S(CVODES_y,finalStateNo)*transitionRate;
    }
    transitionCounter=0;
    //Loop to generate rvals's for the intial->final transitions
    //Rvals ae residuals, e.g. rval[n]=func-ypval[n], where ypval[n] is the value of the differential.
    //Loops through the initial states and extract the state identifier number
    for(const auto& finalStateIds_r: orderedAllowedStates[step]){
        initialStateNo=initialStateCounter;
        NV_Ith_S(CVODES_ydot,initialStateNo)=0;
        for(const auto& finalStateId_r: finalStateIds_r){
            finalStateNo=numberOfInitialStates[step]+finalStateId_r;
            transitionRate=numberOfTransitionsVectors[step][transitionCounter]*gauss[step];
            spontaneousDecayRate=spontaneousDecayRateVectors[step][transitionCounter]*onOff;
            NV_Ith_S(CVODES_ydot,initialStateNo)+=(transitionRate)*NV_Ith_S(CVODES_y,finalStateNo)-(transitionRate)*NV_Ith_S(CVODES_y,initialStateNo)
                    +spontaneousDecayRate*NV_Ith_S(CVODES_y,finalStateNo);
            NV_Ith_S(CVODES_ydot,finalStateNo)+=(transitionRate)*NV_Ith_S(CVODES_y,initialStateNo)-(transitionRate)*NV_Ith_S(CVODES_y,finalStateNo)
                    -spontaneousDecayRate*NV_Ith_S(CVODES_y,finalStateNo);

            transitionCounter++;
        }
        initialStateCounter++;
    }
    return 0;
}

int genericSystem::CVODESODESystemJacobi(long N, realtype t, N_Vector y, N_Vector fy, DlsMat J, void *user_data, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3){
    std::vector< std::vector<double>> numberOfTransitionsVectors=((struct genericSystem::genericParameters*)user_data)->m_numberOfTransitionsVectors;
    std::vector< std::vector<double>> spontaneousDecayRateVectors=((struct genericSystem::genericParameters*)user_data)->m_spontaneousDecayRateVectors;
    std::vector< std::vector<double>> laserPulseProperties=((struct genericSystem::genericParameters*)user_data)->m_laserPulseProperties;
    std::vector<int> numberOfInitialStates=((struct genericSystem::genericParameters*)user_data)->m_numberOfInitialStates;
    std::vector<int> numberOfFinalStates=((struct genericSystem::genericParameters*)user_data)->m_numberOfFinalStates;
    std::vector<std::vector<int>> initialStateIds=((struct genericSystem::genericParameters*)user_data)->m_initialStateIds;
    std::vector<std::vector<int>> finalStateIds=((struct genericSystem::genericParameters*)user_data)->m_finalStateIds;
    std::vector<std::vector<std::vector<int>>> orderedAllowedStates=((struct genericSystem::genericParameters*)user_data)->m_orderedAllowedStates;
    int ionizisedStateNo=((struct genericSystem::genericParameters*)user_data)->m_ionizedStateId;
    double totalPopulation=((struct genericSystem::genericParameters*)user_data)->m_totalPopulation;
    std::vector<double> gauss;
    for(int p=0;p<laserPulseProperties.size();p++){
        gauss.push_back(((struct genericSystem::genericParameters*)user_data)->getGaussianLineShape(t, laserPulseProperties[p][0], laserPulseProperties[p][1]));
    }
    double isSpontaenousEmissionOn = ((struct genericSystem::genericParameters*)user_data)->m_isSpontaenousEmissionOn;
    double onOff;
    if(isSpontaenousEmissionOn){
        onOff=1;
    }
    else{
        onOff=0;
    }
    double transitionRate=0;
    int initialStateNo=0;
    int initialStateNo_next=0;
    int finalStateNo=0;
    int finalStateNo_next=0;
    double spontaneousDecayRate=0;
    int step=0;
    finalStateNo_next=numberOfInitialStates[step];
    int transitionCounter=0;
    int initialStateCounter=0;
    for(const auto &finalStateId_r: finalStateIds[step]){
        finalStateNo=numberOfInitialStates[step]+finalStateId_r;
        for(const auto &finalStateId_r_next: finalStateIds[step]){
            finalStateNo_next=numberOfInitialStates[step]+finalStateId_r_next;
            IJth(J,finalStateNo_next, finalStateNo)=0;
            IJth(J,finalStateNo, finalStateNo_next)=0;
        }
        //Loop final states
        transitionRate=numberOfTransitionsVectors[1][finalStateId_r]*gauss[1];
        IJth(J,finalStateNo,ionizisedStateNo)=0;
        IJth(J,finalStateNo, finalStateNo)-=transitionRate;
        IJth(J,ionizisedStateNo,finalStateNo)+=transitionRate;
    }
    transitionCounter=0;
    for(const auto &finalStateIds_r: orderedAllowedStates[step]){
        initialStateNo=initialStateCounter;
        for(const auto &initialStateNo_r_next: initialStateIds[step]){
            initialStateNo_next=initialStateNo_r_next;
            IJth(J,initialStateNo_next, initialStateNo)=0;
            IJth(J,initialStateNo, initialStateNo_next)=0;
        }
        for(const auto &finalStateId_r: finalStateIds_r){
            finalStateNo=numberOfInitialStates[step]+finalStateId_r;
            transitionRate=numberOfTransitionsVectors[step][transitionCounter]*gauss[step];
            spontaneousDecayRate=spontaneousDecayRateVectors[step][transitionCounter]*onOff;
            IJth(J,finalStateNo, initialStateNo)=0;
            IJth(J,initialStateNo, initialStateNo)-=transitionRate;
            IJth(J,initialStateNo, finalStateNo)+=(transitionRate+spontaneousDecayRate) ;
            IJth(J,finalStateNo, initialStateNo)+=(transitionRate);
            IJth(J,finalStateNo, finalStateNo)-=(transitionRate+spontaneousDecayRate);
            transitionCounter++;
        }
        IJth(J,initialStateNo,ionizisedStateNo)=0;
        IJth(J,ionizisedStateNo,initialStateNo)=0;
        initialStateCounter++;
    }
    IJth(J,ionizisedStateNo, ionizisedStateNo)=0;
    return 0;
}


////////////////
/// IDAS DAE
////////////////
int genericSystem::referenceIDASDAESystem(realtype tres, N_Vector yy, N_Vector yp, N_Vector rr, void *user_data)
{
//    x[1]->x[3]:  Initial F: 1 Final F: 1 Transition intensity: 0.5
//    x[0]->x[3]:  Initial F: 0 Final F: 1 Transition intensity: 0.25
//    x[1]->x[2]:  Initial F: 1 Final F: 0 Transition intensity: 0.25
    realtype *yval, *ypval, *rval;
    realtype t=tres;

    //New Code
    yval = NV_DATA_S(yy);
    ypval = NV_DATA_S(yp);
    rval = NV_DATA_S(rr);
    std::vector< std::vector<double>> numberOfTransitionsVectors=((struct genericSystem::genericParameters*)user_data)->m_numberOfTransitionsVectors;
    std::vector< std::vector<double>> spontaneousDecayRateVectors=((struct genericSystem::genericParameters*)user_data)->m_spontaneousDecayRateVectors;
    std::vector< std::vector<double>> laserPulseProperties=((struct genericSystem::genericParameters*)user_data)->m_laserPulseProperties;
    double totalPopulation=((struct genericSystem::genericParameters*)user_data)->m_totalPopulation;
    std::vector<double> gauss;
    gauss.push_back(((struct genericSystem::genericParameters*)user_data)->getGaussianLineShape(t, laserPulseProperties[0][0], laserPulseProperties[0][1]));
    gauss.push_back(((struct genericSystem::genericParameters*)user_data)->getGaussianLineShape(t, laserPulseProperties[1][0], laserPulseProperties[1][1]));

    double spontDec1=spontaneousDecayRateVectors[0][0];
    double transRate1=numberOfTransitionsVectors[0][0]*gauss[0];

    double spontDec3=spontaneousDecayRateVectors[0][1];
    double transRate3=numberOfTransitionsVectors[0][1]*gauss[0];

    double spontDec2=spontaneousDecayRateVectors[0][2];
    double transRate2=numberOfTransitionsVectors[0][2]*gauss[0];

    double nonResonantTransitionRate1=numberOfTransitionsVectors[1][0]*gauss[1];
    double nonResonantTransitionRate2=numberOfTransitionsVectors[1][1]*gauss[1];

    spontDec1=spontaneousDecayRateVectors[0][0];
    transRate1=numberOfTransitionsVectors[0][0]*gauss[0];

    spontDec3=spontaneousDecayRateVectors[0][1];
    transRate3=numberOfTransitionsVectors[0][1]*gauss[0];

    spontDec2=spontaneousDecayRateVectors[0][2];
    transRate2=numberOfTransitionsVectors[0][2]*gauss[0];

    nonResonantTransitionRate1=numberOfTransitionsVectors[1][0]*gauss[1];
    nonResonantTransitionRate2=numberOfTransitionsVectors[1][1]*gauss[1];

    //0
    rval[0]=(transRate1+spontDec1)*yval[3]-(transRate1)*yval[0]-ypval[0];
    //1
    rval[1]=(transRate3+spontDec3)*yval[2]+(transRate2+spontDec2)*yval[3]-(transRate3+transRate2)*yval[1]-ypval[1];
    //2
    rval[2]=(transRate3)*yval[1]-(transRate3+spontDec3+nonResonantTransitionRate1)*yval[2]-ypval[2];
    //3
    rval[3]=(transRate1)*yval[0]+(transRate2)*yval[1]-(transRate1+transRate2+spontDec1+spontDec2+nonResonantTransitionRate2)*yval[3]-ypval[3];
    //4
    rval[4]=yval[0]+yval[1]+yval[2]+yval[3]+yval[4]-totalPopulation;

    return 0;
}

int genericSystem::referenceIDASDAESystemJacobi(long Neq, realtype tt, realtype cj, N_Vector yy, N_Vector yp, N_Vector resvec, DlsMat JJ, void *user_data, N_Vector tempv1, N_Vector tempv2, N_Vector tempv3){

    //    x[1]->x[3]:  Initial F: 1 Final F: 1 Transition intensity: 0.5
    //    x[0]->x[3]:  Initial F: 0 Final F: 1 Transition intensity: 0.25
    //    x[1]->x[2]:  Initial F: 1 Final F: 0 Transition intensity: 0.25
    realtype *yval;
    realtype t=tt;
    yval = NV_DATA_S(yy);
    std::vector< std::vector<double>> numberOfTransitionsVectors=((struct genericSystem::genericParameters*)user_data)->m_numberOfTransitionsVectors;
    std::vector< std::vector<double>> spontaneousDecayRateVectors=((struct genericSystem::genericParameters*)user_data)->m_spontaneousDecayRateVectors;
    std::vector< std::vector<double>> laserPulseProperties=((struct genericSystem::genericParameters*)user_data)->m_laserPulseProperties;
    std::vector<double> gauss;
    gauss.push_back(((struct genericSystem::genericParameters*)user_data)->getGaussianLineShape(t, laserPulseProperties[0][0], laserPulseProperties[0][1]));
    gauss.push_back(((struct genericSystem::genericParameters*)user_data)->getGaussianLineShape(t, laserPulseProperties[1][0], laserPulseProperties[1][1]));
    double spontDec1=spontaneousDecayRateVectors[0][0];
    double transRate1=numberOfTransitionsVectors[0][0]*gauss[0];

    double spontDec2=spontaneousDecayRateVectors[0][1];
    double transRate2=numberOfTransitionsVectors[0][1]*gauss[0];

    double spontDec3=spontaneousDecayRateVectors[0][2];
    double transRate3=numberOfTransitionsVectors[0][2]*gauss[0];

    double nonResonantTransitionRate1=numberOfTransitionsVectors[1][0]*gauss[1];
    double nonResonantTransitionRate2=numberOfTransitionsVectors[1][1]*gauss[1];
    IJth(JJ,0,0) = -transRate1 - cj;
    IJth(JJ,1,0) = 0.0;
    IJth(JJ,2,0) = 0.0;
    IJth(JJ,3,0) = transRate1;
    IJth(JJ,4,0) = 1;

    IJth(JJ,0,1) = 0.0;
    IJth(JJ,1,1) = -transRate3-transRate2- cj;
    IJth(JJ,2,1) = transRate3;
    IJth(JJ,3,1) = transRate2;
    IJth(JJ,4,1) = 1;

    IJth(JJ,0,2) = 0.0;
    IJth(JJ,1,2) = transRate3+spontDec3;
    IJth(JJ,2,2) = -transRate3-spontDec3-nonResonantTransitionRate1 - cj;
    IJth(JJ,3,2) = 0.0;
    IJth(JJ,4,2) = 1;

    IJth(JJ,0,3) = transRate1+spontDec1;
    IJth(JJ,1,3) = transRate2+spontDec2;
    IJth(JJ,2,3) = 0.0;
    IJth(JJ,3,3) = -transRate1-transRate2-spontDec1-spontDec2-nonResonantTransitionRate2-cj;
    IJth(JJ,4,3) = 1;

    IJth(JJ,0,4) = 0;
    IJth(JJ,1,4) = 0;
    IJth(JJ,2,4) = 0;
    IJth(JJ,3,4) = 0;
    IJth(JJ,4,4) = 1;
    return 0;
}

int genericSystem::IDASDAESystem(realtype tres, N_Vector yy, N_Vector yp, N_Vector rr, void *user_data)
{
    realtype *yval, *ypval, *rval;
    realtype t=tres;
    yval = NV_DATA_S(yy);
    ypval = NV_DATA_S(yp);
    rval = NV_DATA_S(rr);
    std::vector< std::vector<double>> numberOfTransitionsVectors=((struct genericSystem::genericParameters*)user_data)->m_numberOfTransitionsVectors;
    std::vector< std::vector<double>> spontaneousDecayRateVectors=((struct genericSystem::genericParameters*)user_data)->m_spontaneousDecayRateVectors;
    std::vector< std::vector<double>> laserPulseProperties=((struct genericSystem::genericParameters*)user_data)->m_laserPulseProperties;
    std::vector<int> numberOfInitialStates=((struct genericSystem::genericParameters*)user_data)->m_numberOfInitialStates;
    std::vector<int> numberOfFinalStates=((struct genericSystem::genericParameters*)user_data)->m_numberOfFinalStates;
    std::vector<std::vector<int>> finalStateIds=((struct genericSystem::genericParameters*)user_data)->m_finalStateIds;
    std::vector<std::vector<std::vector<int>>> orderedAllowedStates=((struct genericSystem::genericParameters*)user_data)->m_orderedAllowedStates;
    int ionizisedStateNo=((struct genericSystem::genericParameters*)user_data)->m_ionizedStateId;
    double totalPopulation=((struct genericSystem::genericParameters*)user_data)->m_totalPopulation;
    std::vector<double> gauss;
    for(int p=0;p<laserPulseProperties.size();p++){
        gauss.push_back(((struct genericSystem::genericParameters*)user_data)->getGaussianLineShape(t, laserPulseProperties[p][0], laserPulseProperties[p][1]));
    }
    double isSpontaenousEmissionOn = ((struct genericSystem::genericParameters*)user_data)->m_isSpontaenousEmissionOn;
    double onOff;
    if(isSpontaenousEmissionOn){
        onOff=1;
    }
    else{
        onOff=0;
    }
    //Clear "ionizing" state
    rval[ionizisedStateNo]=0;
    double transitionRate=0;
    double spontaneousDecayRate=0;
    int step=0;
    int initialStateNo=0;
    int finalStateNo=0;
    finalStateNo=numberOfInitialStates[step];
    //Generate transitions from final F -states to ionizing state
    int transitionCounter=0;
    for(const auto &finalStateId_r: finalStateIds[step]){
        finalStateNo=numberOfInitialStates[step]+finalStateId_r;
        transitionRate=numberOfTransitionsVectors[1][finalStateId_r]*gauss[1];
        rval[finalStateNo]=0;
        rval[finalStateNo]-=transitionRate*yval[finalStateNo];
        rval[finalStateNo]-=ypval[finalStateNo];
        rval[ionizisedStateNo]+=yval[finalStateNo];
    }
    transitionCounter=0;
    //Generate transitions from initial to final F -states
    for(const auto &finalStateIds_r: orderedAllowedStates[step]){
        rval[initialStateNo]=0;
        for(const auto &finalStateId_r: finalStateIds_r){
            finalStateNo=numberOfInitialStates[step]+finalStateId_r;
            transitionRate=numberOfTransitionsVectors[step][transitionCounter]*gauss[step];
            spontaneousDecayRate=spontaneousDecayRateVectors[step][transitionCounter]*onOff;
            rval[initialStateNo]+=(transitionRate)*yval[finalStateNo]-(transitionRate)*yval[initialStateNo]+spontaneousDecayRate*yval[finalStateNo];
            rval[finalStateNo]+=(transitionRate)*yval[initialStateNo]-(transitionRate)*yval[finalStateNo]-spontaneousDecayRate*yval[finalStateNo];
            transitionCounter++;
        }
        rval[initialStateNo]-=ypval[initialStateNo];
        rval[ionizisedStateNo]+=yval[initialStateNo];
        initialStateNo++;
    }
    rval[ionizisedStateNo]+=yval[ionizisedStateNo];
    rval[ionizisedStateNo]-=totalPopulation;
    return 0;
}

int genericSystem::IDASDAESystemJacobi(long Neq, realtype tt, realtype cj, N_Vector yy, N_Vector yp, N_Vector resvec, DlsMat JJ, void *user_data, N_Vector tempv1, N_Vector tempv2, N_Vector tempv3){
    realtype *yval;
    realtype t=tt;
    yval = NV_DATA_S(yy);
    std::vector< std::vector<double>> numberOfTransitionsVectors=((struct genericSystem::genericParameters*)user_data)->m_numberOfTransitionsVectors;
    std::vector< std::vector<double>> spontaneousDecayRateVectors=((struct genericSystem::genericParameters*)user_data)->m_spontaneousDecayRateVectors;
    std::vector< std::vector<double>> laserPulseProperties=((struct genericSystem::genericParameters*)user_data)->m_laserPulseProperties;
    std::vector<int> numberOfInitialStates=((struct genericSystem::genericParameters*)user_data)->m_numberOfInitialStates;
    std::vector<int> numberOfFinalStates=((struct genericSystem::genericParameters*)user_data)->m_numberOfFinalStates;
    std::vector<std::vector<int>> initialStateIds=((struct genericSystem::genericParameters*)user_data)->m_initialStateIds;
    std::vector<std::vector<int>> finalStateIds=((struct genericSystem::genericParameters*)user_data)->m_finalStateIds;
    std::vector<std::vector<std::vector<int>>> orderedAllowedStates=((struct genericSystem::genericParameters*)user_data)->m_orderedAllowedStates;
    int ionizisedStateNo=((struct genericSystem::genericParameters*)user_data)->m_ionizedStateId;
    double totalPopulation=((struct genericSystem::genericParameters*)user_data)->m_totalPopulation;
    std::vector<double> gauss;
    for(int p=0;p<laserPulseProperties.size();p++){
        gauss.push_back(((struct genericSystem::genericParameters*)user_data)->getGaussianLineShape(t, laserPulseProperties[p][0], laserPulseProperties[p][1]));
    }
    double isSpontaenousEmissionOn = ((struct genericSystem::genericParameters*)user_data)->m_isSpontaenousEmissionOn;
    double onOff;
    if(isSpontaenousEmissionOn){
        onOff=1;
    }
    else{
        onOff=0;
    }
    double transitionRate=0;
    int initialStateNo=0;
    int initialStateNo_next=0;
    int finalStateNo=0;
    int finalStateNo_next=0;
    double spontaneousDecayRate=0;
    int step=0;
    finalStateNo_next=numberOfInitialStates[step];
    int transitionCounter=0;
    int initialStateCounter=0;
    for(const auto &finalStateId_r: finalStateIds[step]){
        finalStateNo=numberOfInitialStates[step]+finalStateId_r;
        for(const auto &finalStateId_r_next: finalStateIds[step]){
            finalStateNo_next=numberOfInitialStates[step]+finalStateId_r_next;
            IJth(JJ,finalStateNo_next, finalStateNo)=0;
            IJth(JJ,finalStateNo, finalStateNo_next)=0;
        }
        IJth(JJ,finalStateNo, finalStateNo)=0;
        transitionRate=numberOfTransitionsVectors[1][finalStateId_r]*gauss[1];
        IJth(JJ,finalStateNo, finalStateNo)-=transitionRate;
        IJth(JJ,finalStateNo,ionizisedStateNo)=0;
        IJth(JJ,ionizisedStateNo,finalStateNo)=1;
        IJth(JJ,finalStateNo, finalStateNo)-=cj;
    }
    transitionCounter=0;
    for(const auto &finalStateIds_r: orderedAllowedStates[step]){
        initialStateNo=initialStateCounter;
        for(const auto &initialStateNo_r_next: initialStateIds[step]){
            initialStateNo_next=initialStateNo_r_next;
            IJth(JJ,initialStateNo_next, initialStateNo)=0;
            IJth(JJ,initialStateNo, initialStateNo_next)=0;
        }
        for(const auto &finalStateId_r: finalStateIds_r){
            finalStateNo=numberOfInitialStates[step]+finalStateId_r;
            transitionRate=numberOfTransitionsVectors[step][transitionCounter]*gauss[step];
            spontaneousDecayRate=spontaneousDecayRateVectors[step][transitionCounter]*onOff;
            IJth(JJ,finalStateNo, initialStateNo)=0;
            IJth(JJ,initialStateNo, initialStateNo)-=transitionRate;
            IJth(JJ,initialStateNo, finalStateNo)+=(transitionRate+spontaneousDecayRate) ;
            IJth(JJ,finalStateNo, initialStateNo)+=(transitionRate);
            IJth(JJ,finalStateNo, finalStateNo)-=(transitionRate+spontaneousDecayRate);
            transitionCounter++;
        }
        IJth(JJ,initialStateNo,ionizisedStateNo)=0;
        IJth(JJ,ionizisedStateNo,initialStateNo)=1;
        IJth(JJ,initialStateNo, initialStateNo)-=cj;
        initialStateCounter++;
    }
    IJth(JJ,ionizisedStateNo, ionizisedStateNo)=1;
    return 0;
}
