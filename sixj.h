//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

//Part of the code, namely the selection rules and triangle condition rules derived from GSL1.16 (coupling.c - Author:  G. Jungman)

#ifndef SIXJ_H
#define SIXJ_H
#include <boost/math/special_functions/factorials.hpp>
#include <math.h>

class sixJ
{
public:
    sixJ(){
    }
    // The notation
    //  /e, a, f\
    //  \b, d, c/
    //, is from INTERNATIONAL JOURNAL OF QUANTUM CHEMISTRY, VOL. 52, 593-607 (1994),
    //Computation of Algebraic Formulas for Wigner 3-j, 6-j, and 9-j Symbols by Maple
    //, SHAN-TAO LAI
    double sixJValue(double e_v, double a_v, double f_v, double b_v, double d_v, double c_v){
        if(!conditionsFail(e_v, a_v, f_v, b_v, d_v, c_v) && !trianglesFail(e_v, a_v, f_v, b_v, d_v, c_v)){
            //Numerator
            double numerator=0;
            double phase=pow(-1,(b_v+c_v+e_v+f_v));
            double numTri1=triangleCoefficient(a_v, b_v, c_v);
            double numTri2=triangleCoefficient(a_v,e_v,f_v);
            double numTri3=triangleCoefficient(c_v, d_v, e_v);
            double numTri4=triangleCoefficient(b_v, d_v, f_v);
            double numFac1=boost::math::factorial<double>(a_v+b_v+c_v+1);
            double numFac2=boost::math::factorial<double>(b_v+d_v+f_v+1);
            numerator=phase*numTri1*numTri2*numTri3*numTri4*numFac1*numFac2;
            //Denominator
            double denominator=0;
            double denFac1=boost::math::factorial<double>(a_v+b_v-c_v);
            double denFac2=boost::math::factorial<double>(c_v-d_v+e_v);
            double denFac3=boost::math::factorial<double>(c_v+d_v-e_v);
            double denFac4=boost::math::factorial<double>(a_v-e_v+f_v);
            double denFac5=boost::math::factorial<double>(-a_v+e_v+f_v);
            double denFac6=boost::math::factorial<double>(b_v+d_v-f_v);
            denominator =denFac1*denFac2*denFac3*denFac4*denFac5*denFac6;
            return ((numerator/denominator)*sumOverZ(e_v, a_v, f_v, b_v, d_v, c_v));
        }
        else{
            return 0;
        }
    }
private:
    double triangleCoefficient(double j1_v, double j2_v, double j_v){
        double fac1=boost::math::factorial<double>(j1_v+j_v-j2_v);
        double fac2=boost::math::factorial<double>(j1_v-j_v+j2_v);
        double fac3=boost::math::factorial<double>(-j1_v+j_v+j2_v);
        double fac4=boost::math::factorial<double>(j1_v+j_v+j2_v+1);
        return sqrt((fac1*fac2*fac3)/fac4);
    }

    double zValue(double a_v, double b_v, double c_v, double d_v, double f_v){
        double firstValue=2*b_v;
        double secondValue=-a_v+b_v+c_v;
        double thirdValue=b_v-d_v+f_v;
        double minz=std::min({firstValue, secondValue, thirdValue});
        if(minz<0){
            return 0;
        }
        else{
            return minz;
        }
    }
    double sumOverZ(double e_v, double a_v, double f_v, double b_v, double d_v, double c_v){
        double z=zValue(a_v, b_v, c_v, d_v, f_v);
        //Numerator;
        double numerator=0;
        double phase=0;
        double numFac1=0;
        double numFac2=0;
        double numFac3=0;
        //Denominator
        double zFac=1;
        double denominator=1;
        double denFac1=1;
        double denFac2=1;
        double denFac3=1;
        double denFac4=1;
        double zSum=0;
        for(int i=0; i<=z; i++){
            phase=pow(-1,i);
            numFac1=boost::math::factorial<double>(2*b_v-i);
            numFac2=boost::math::factorial<double>(b_v+c_v-e_v+f_v-i);
            numFac3=boost::math::factorial<double>(b_v+c_v+e_v+f_v+1-i);
            numerator=phase*numFac1*numFac2*numFac3;
            zFac=boost::math::factorial<double>(i);
            denFac1=boost::math::factorial<double>(-a_v+b_v+c_v-i);
            denFac2=boost::math::factorial<double>(b_v-d_v+f_v-i);
            denFac3=boost::math::factorial<double>(a_v+b_v+c_v+1-i);
            denFac4=boost::math::factorial<double>(b_v+d_v+f_v+1-i);
            denominator=zFac*denFac1*denFac2*denFac3*denFac4;
            zSum=zSum+numerator/denominator;
        }
        return zSum;
    }
    bool conditionsFail(double e_v, double a_v, double f_v, double b_v, double d_v, double c_v){
        return (e_v<0 || a_v<0 || f_v<0 || b_v<0 || d_v<0 || c_v<0);
    }
    bool trianglesFail(double e_v, double a_v, double f_v, double b_v, double d_v, double c_v){
        return(isTriangle(e_v,a_v,f_v) ||
               isTriangle(e_v, d_v, c_v) ||
               isTriangle(a_v, b_v, c_v) ||
               isTriangle(d_v, b_v ,f_v));
    }
    bool isTriangle(double j1_v, double j2_v, double j_v){
        return ((j2_v<fabs(j1_v-j_v))|| (j2_v>(j1_v+j_v)) || isOdd(2*j1_v+2*j2_v+2*j_v));
    }

    bool isOdd(double number_v){
        int number=static_cast<int>(number_v);
        if(number & 1){
            return true;
        } else {
            return false;
        }
    }

};

#endif // SIXJ_H
