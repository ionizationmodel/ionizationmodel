﻿//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef RATEEQUATIONSPROFILE_H
#define RATEEQUATIONSPROFILE_H

#include <QObject>
#ifndef Q_MOC_RUN
#include <boost/thread.hpp>
#endif

#include <iostream>
#include <vector>
#include <QHash>
#include <QDebug>
#include <QVector>
#include <QStringList>
#include <genericSystem.h>
#include <relativeprofile.h>
#include <QPair>
#include <valuecontainerclass.h>
#include <hyperfinecalculations.h>
#include <arraysort.h>
#include <boost/timer/timer.hpp>
#include <omp.h>
#include <unordered_map>

class rateEquationsProfile: public QObject
{
    Q_OBJECT
public:
    rateEquationsProfile (QObject *parent, valueContainerClass *valueContainer_p,
                          hyperfineCalculations *hyperFine_p,
                          relativeProfile *relProfile_p)
        :valueContainer(valueContainer_p), hypFi(hyperFine_p), relProfile(relProfile_p){
        this->setParent(parent);

        genericSystemInstance=nullptr;

        hbar=valueContainer->hBar;
        pi=valueContainer->pi;
        k=valueContainer->k;
        initialPopulationScaler=1;

        arrayTools = new arraySort();
        lastStateToBePlotted=0;

        //SUNDIALS parameters
        totalPopulation=0;
        numberOfEquations=0;

        nullPair.first.clear();
        nullPair.second.clear();
    }
    ~rateEquationsProfile(){
        foreach (genericSystem *rS, genericSystemsPerFrequency) {
            if(rS!=nullptr){
                rS->cleanIDASSystem();
                rS->cleanCVODESSystem();
                delete rS;
                rS=nullptr;
            }
        }

        delete arrayTools;
    }

    std::vector<double> getIDAS_Fitter(){
        //Model parameters
        initialPopulationScaler=valueContainer->getModel_initialPopulationScaler();
        initialPopulationTemperature=valueContainer->getModel_initialPopulationTemperature();
        //transition parameters
        relativeIntensities=valueContainer->getRelativeTransitionIntensities();
        initialStateIds=valueContainer->getListOfInitialStateIds();
        finalStateIds=valueContainer->getListOfFinalStateIds();
        //Other
        laserFrequencyRange=valueContainer->getFrequencyRange();
        //Clear old values
        transitionRatesPerFrequency.clear();
        numberOfTransitionForFrequencyRange.clear();
        //Setup the systems
        setUpSystems_SUNDIALS(laserFrequencyRange, relativeTransitionNumbers);
        //solve the rate equations
        run_IDAS();
        return getIonizedStateVector();
    }

    std::vector<double> getCVODES_Fitter(){

    }

public slots:
    void solveRateEquations_IDAS(){
        //Model parameters
        initialPopulationScaler=valueContainer->getModel_initialPopulationScaler();
        initialPopulationTemperature=valueContainer->getModel_initialPopulationTemperature();

        //transition parameters
        relativeIntensities=valueContainer->getRelativeTransitionIntensities();
        initialStateIds=valueContainer->getListOfInitialStateIds();
        finalStateIds=valueContainer->getListOfFinalStateIds();

        //Other
        laserFrequencyRange=valueContainer->getFrequencyRange();
        //relativeTransitionNumbers=relProfile->getRelativeTransitionNumbers();

        //Clear old values
        transitionRatesPerFrequency.clear();
        numberOfTransitionForFrequencyRange.clear();

        //Setup the systems
        setUpSystems_SUNDIALS(laserFrequencyRange, relativeTransitionNumbers);

        //solve the rate equations
        run_IDAS();
        valueContainer->setSundials_maximumTimeOrFrequencyIndex(genericSystemsPerFrequency.front()->getTimePoints().size());

        valueContainer->setSundials_CurrentTimeOrFrequencyValue(genericSystemsPerFrequency.front()->getTimePoints().back()-1);

        //Emit that the data is ready
        emit rateEquationsSolved_SUNDIALS(hypFi->getNumberOfInitialFStates(), hypFi->getNumberOfFinalFStates(),numberOfEquations-1, valueContainer->getSundials_StateToBePlotted());
        plot_SUNDIALS();
    }

    void solveRateEquations_CVODES(){
        //Model parameters
        initialPopulationScaler=valueContainer->getModel_initialPopulationScaler();
        initialPopulationTemperature=valueContainer->getModel_initialPopulationTemperature();

        //transition parameters
        relativeIntensities=valueContainer->getRelativeTransitionIntensities();
        initialStateIds=valueContainer->getListOfInitialStateIds();
        finalStateIds=valueContainer->getListOfFinalStateIds();

        //Other
        laserFrequencyRange=valueContainer->getFrequencyRange();

        //relativeTransitionNumbers=relProfile->getRelativeTransitionNumbers();

        //Clear old values
        transitionRatesPerFrequency.clear();
        numberOfTransitionForFrequencyRange.clear();
        //Setup the systems
        setUpSystems_SUNDIALS(laserFrequencyRange, relativeTransitionNumbers);
        //solve the rate equations
        run_CVODES();
        //Emit that the data is ready
        emit rateEquationsSolved_SUNDIALS(hypFi->getNumberOfInitialFStates(), hypFi->getNumberOfFinalFStates(),numberOfEquations-1, valueContainer->getSundials_StateToBePlotted());
        plot_SUNDIALS();
    }



    void clearRateProfile(){
        plotDataReady_SUNDIALS(nullPair);
        emit solverProgress_SUNDIALS(0);
    }

    void plot_SUNDIALS(){
        if(genericSystemsPerFrequency.size()!=0){
            std::vector<double>  timeRange=genericSystemsPerFrequency.front()->getTimePoints();
            int timeRangeSize=timeRange.size()-1;
            QString labelText;
            int freqRangeSize=laserFrequencyRange.size()-1;
            //Get UI-values related to plotting a selected state etc...
            bool plotTime=valueContainer->getSundials_PlotAsAFunctionOfTime();
            int currentTimeOrFrequencyidx=valueContainer->getSundials_TimeOrFrequencyPointIndexToBePlotted();
             if(plotTime){
                if(currentTimeOrFrequencyidx>freqRangeSize){
                    currentTimeOrFrequencyidx=0;
                }
                valueContainer->setSundials_maximumTimeOrFrequencyIndex(freqRangeSize);
                valueContainer->setSundials_CurrentTimeOrFrequencyValue(laserFrequencyRange[currentTimeOrFrequencyidx]/1E9-1);
                labelText="Frequency";
                emit plotAsAFunctionOfTime(labelText);

            }
            else{
                if(currentTimeOrFrequencyidx>timeRangeSize ){
                    currentTimeOrFrequencyidx=0;
                }
                valueContainer->setSundials_maximumTimeOrFrequencyIndex(timeRangeSize);
                valueContainer->setSundials_CurrentTimeOrFrequencyValue(timeRange[currentTimeOrFrequencyidx]);
                labelText="Time";
                emit plotAsAFunctionOfTime(labelText);
            }

            int stateIndex=valueContainer->getSundials_StateToBePlotted();
            double totalSize=valueContainer->getFrequencyRange().size();
            plotData_SUNDIALS(plotTime,currentTimeOrFrequencyidx,stateIndex, totalSize);
        }

    }

    void plotNumberOfIntegrationSteps_IDA(int timeOrFrequencyIndex){
        integrationStepsPerFrequencyPointProfile.first=valueContainer->getFrequencyRange();
        integrationStepsPerFrequencyPointProfile.second=intergationStepsPerFrequencyPoint;
    }


signals:
    //IDA
    void rateEquationsSolved_SUNDIALS(int noOfInitialStates, int noOfFinalStates, int ionizingState, int lastIndex);
    void plotDataReady_SUNDIALS(QPair<QVector<double>, QVector<double> > plotData_v);
    void solverProgress_SUNDIALS(int newProgress);
    void plotAsAFunctionOfTime(QString text);

private:
    arraySort *arrayTools;
    valueContainerClass *valueContainer;
    hyperfineCalculations *hypFi;

    //Relative profile
    relativeProfile *relProfile;
    //QHash for storing the transition rates for the rate equation solver
    QHash<QString, double > transitionRatesPerFrequency;
    QVector<QHash<QString, double > > numberOfTransitionForFrequencyRange;
    QHash<QString, double > transitionFrequencies;
    //Number of integrationsteps takes per frequencypoint
    QPair<QVector<double>, QVector<double> > integrationStepsPerFrequencyPointProfile;
    QVector<double> intergationStepsPerFrequencyPoint;
    //results storage
    int lastStateToBePlotted;
    std::vector<std::vector<double> > resultsStorage;
    QPair<QVector<double>, QVector<double> > rateEquationProfile;
    //constants
    double T;
    double k;
    double hbar;
    double pi;
    //laser and transition parameters
    std::unordered_map<std::string, double>  relativeIntensities;
    double spontaneousTransitionRate;
    QHash<QString, int > initialStateIds;
    QHash<QString, int > finalStateIds;
    QVector<double> laserFrequencyRange;
    QHash<QString, QVector<double>  > relativeTransitionNumbers;


    //IDA
    genericSystem *genericSystemInstance;
    std::vector<genericSystem*> genericSystemsPerFrequency;
    std::vector<std::vector<double> > genericInitialConditionsForFrequencyRange;
    std::vector<double> initialTransitionRates;
    std::vector<double> absoluteTolerances;
    double totalPopulation;
    int numberOfEquations;
    double numberOfIonizingTransitions;
    //SUNDIALS results;

    std::unordered_map<int, std::vector<double> > integrationStepsPerFrequencyPoint_SUNDIALS;
    std::unordered_map<int, std::vector<std::vector<double> > > statePopulationsPerTimePerFreq_SUNDIALS;
    std::unordered_map<int, std::vector<double> > integrationTimePoints_SUNDIALS;
    QPair<QVector<double>, QVector<double> > nullPair;

    //Model parameters
    bool isSpontaneousEmissionOn;
    double initialPopulationScaler;
    double initialPopulationTemperature;

    double maxwellBoltzmannStatistics(double deltaNu){
        double deltaE=deltaNu*hbar*2*pi;
        double ratio=exp(-deltaE/(k*initialPopulationTemperature));
        return ratio;
    }
    void setUpSystems_SUNDIALS(QVector<double> laserFrequencyRange_v,QHash<QString, QVector<double>  > relativeTransitionNumbers_v ){
        foreach (genericSystem *rS, genericSystemsPerFrequency) {
            if(rS!=nullptr){
                rS->cleanIDASSystem();
                rS->cleanCVODESSystem();
                delete rS;
                rS=nullptr;
            }
        }
        genericSystemsPerFrequency.clear();
        genericInitialConditionsForFrequencyRange.clear();
        //Get and parse the number of transitions per pulse per ransition frequency;
        //Set initial conditions which act as the state vector for each solver
        //Generate a solver for each of the frequency points
        //Set constants within the solvers.
        double transNumbers=0;
        boost::timer::auto_cpu_timer t;

        for(int i=0; i<laserFrequencyRange_v.size(); i++){
            for(const auto &transId : valueContainer->getHashOrder()){
                transNumbers=relativeTransitionNumbers_v.value(QString::fromStdString(transId)).at(i);
                transitionRatesPerFrequency.insert(QString::fromStdString(transId), transNumbers);
            }
            numberOfTransitionForFrequencyRange.append(transitionRatesPerFrequency);
            genericInitialConditionsForFrequencyRange.push_back(setInitialConditions_SUNDIALS());
            genericSystemsPerFrequency.push_back(setUpGenericSystem_SUNDIALS(i));
            genericSystemsPerFrequency.at(i)->setFrequency(laserFrequencyRange_v.at(i));
            genericSystemsPerFrequency.at(i)->setupIDASSystem();
            genericSystemsPerFrequency.at(i)->setupCVODESSystem();
        }
    }
    std::vector<double> setInitialConditions_SUNDIALS(){
        totalPopulation=0;
        absoluteTolerances.clear();
        initialTransitionRates.clear();
        double atol_IDA=valueContainer->getSundials_absoluteTolerance();
        //Set intial conditions(state vectors) vectors for each frequencypoint
        int noOfFinalFStates=hypFi->getNumberOfFinalFStates();
        int noOfInitialFStates=hypFi->getNumberOfInitialFStates();
        numberOfEquations=noOfFinalFStates+noOfInitialFStates+1;
        double A_v=valueContainer->getInitialA();
        double B_v=valueContainer->getInitialB();
        std::vector<double> tempVec(numberOfEquations);
        tempVec.resize(numberOfEquations);
        //delta nu for the lowests of the initial hyperfine states
        //The other states will be compared to this
        double iNu=hypFi->getHyperFineDeltaNu(A_v, B_v, 0)*1E6;
        //foreach (QString stateId, initialStateIds.keys()) {
        int t=0;
        for(t; t<hypFi->getNumberOfInitialFStates();t++){
            double deltaNu=hypFi->getHyperFineDeltaNu(A_v, B_v, t)*1E6-iNu;
            double ratio=maxwellBoltzmannStatistics(deltaNu);
            tempVec[t]=ratio*initialPopulationScaler;
            initialTransitionRates.push_back(0);
            absoluteTolerances.push_back(atol_IDA);
            totalPopulation+=ratio*initialPopulationScaler;
        }
        for(int p=t; p<hypFi->getNumberOfFinalFStates()+t;p++) {
            tempVec[p]=0;
            initialTransitionRates.push_back(0);
            absoluteTolerances.push_back(atol_IDA);
            totalPopulation+=0;
        }
        tempVec[numberOfEquations-1]=0;
        initialTransitionRates.push_back(0);
        absoluteTolerances.push_back(atol_IDA);
        totalPopulation+=0;
        return tempVec;
    }

    genericSystem *setUpGenericSystem_SUNDIALS(int i){
        //Setup the ionization model for each frequency point
        genericSystem *system=new genericSystem();
        system->parameters->m_numberOfTransitionsVectors.push_back(relProfile->getIndividualTransitions()[i]);
        system->parameters->m_numberOfTransitionsVectors.push_back(relProfile->getIonizingTransitionNumberVector()[i]);
        system->parameters->m_spontaneousDecayRateVectors.push_back(valueContainer->getOrderedSpontaneousDecayRates_1());
        std::vector<double> step1Properties;
        std::vector<double> ionizingStepProperties;
        step1Properties.push_back(valueContainer->getLaserPulseWidth()*1E-9);
        step1Properties.push_back(0.0);
        ionizingStepProperties.push_back(valueContainer->getPW_nr()*1E-9);
        ionizingStepProperties.push_back(valueContainer->getTimeOffset()*1E-9);
        system->parameters->m_laserPulseProperties.push_back(step1Properties);
        system->parameters->m_laserPulseProperties.push_back(ionizingStepProperties);
        system->parameters->m_numberOfInitialStates.push_back(hypFi->getNumberOfInitialFStates());
        system->parameters->m_numberOfFinalStates.push_back(hypFi->getNumberOfFinalFStates());
        system->parameters->m_initialStateIds.push_back(hypFi->getInitialStateIds());
        system->parameters->m_finalStateIds.push_back(hypFi->getFinalStateIds());
        system->parameters->m_orderedAllowedStates.push_back(hypFi->getOrderedAllowedStates());
        system->parameters->m_ionizedStateId=hypFi->getIonizedStateId();
        system->parameters->m_hashOrder_1=valueContainer->getHashOrder();
        system->parameters->m_integrationTimeScaler=valueContainer->getSundials_integrationTimeScaler();
        system->parameters->m_isSpontaenousEmissionOn=valueContainer->getModel_spontaneousEmissionOn();
        system->parameters->m_initialPopulations=genericInitialConditionsForFrequencyRange[0];
        system->parameters->m_initialTransitionRates=initialTransitionRates;
        system->parameters->m_setConstraints=valueContainer->getSundials_useConstraints();
        system->parameters->m_setUserJacobian=valueContainer->getSundials_useJacobian();
        system->parameters->m_absoluteTolerances=absoluteTolerances;
        system->parameters->m_relativeTolerance=valueContainer->getSundials_relativeTolerance();
        system->parameters->m_totalPopulation=totalPopulation;
        system->parameters->m_timePoints=valueContainer->getSundials_TimePoints();
        system->parameters->m_setNonlinConvCoef=valueContainer->getSundials_nlConvCoeff();
        system->parameters->m_MaxNumSteps=valueContainer->getSundials_maximumNumberOfIntegrationStep();
        return system;
    }

    void run_IDAS(){
        boost::timer::auto_cpu_timer t;
        int totalSize=numberOfTransitionForFrequencyRange.size();
        int chunk_size = numberOfEquations/omp_get_max_threads();
        omp_set_schedule( omp_sched_dynamic , chunk_size );
        integrationStepsPerFrequencyPoint_SUNDIALS.clear();
        statePopulationsPerTimePerFreq_SUNDIALS.clear();
        integrationTimePoints_SUNDIALS.clear();
        integrationStepsPerFrequencyPoint_SUNDIALS.reserve(totalSize);
        statePopulationsPerTimePerFreq_SUNDIALS.reserve(totalSize);
        integrationTimePoints_SUNDIALS.reserve(totalSize);
        int count=0;
#pragma omp parallel for schedule(dynamic)
        for(int i=0; i<totalSize; i++){
            genericSystemsPerFrequency.at(i)->runIDASSystem();
            int progress=(count*100/totalSize);
            count++;
            if(progress<95){
                emit solverProgress_SUNDIALS(progress);
            }
            else{
                emit solverProgress_SUNDIALS(100);
            }
        }

        emit solverProgress_SUNDIALS(100);//TODO, see it the progress bar is anymore necessary
    }
    void run_CVODES(){
        boost::timer::auto_cpu_timer t;
        int totalSize=numberOfTransitionForFrequencyRange.size();
        int chunk_size = numberOfEquations/omp_get_max_threads();
        omp_set_schedule( omp_sched_dynamic , chunk_size );
        integrationStepsPerFrequencyPoint_SUNDIALS.clear();
        statePopulationsPerTimePerFreq_SUNDIALS.clear();
        integrationTimePoints_SUNDIALS.clear();
        integrationStepsPerFrequencyPoint_SUNDIALS.reserve(totalSize);
        statePopulationsPerTimePerFreq_SUNDIALS.reserve(totalSize);
        integrationTimePoints_SUNDIALS.reserve(totalSize);
        int count=0;
#pragma omp parallel for schedule(dynamic)
        for(int i=0; i<totalSize; i++){
            genericSystemsPerFrequency.at(i)->runCVODESSystem();
            int progress=(count*100/totalSize);
            count++;
            if(progress<95){
                emit solverProgress_SUNDIALS(progress);
            }
            else{
                emit solverProgress_SUNDIALS(100);
            }

        }
        emit solverProgress_SUNDIALS(100);//TODO, see it the progress bar is anymore necessary
    }

    void plotData_SUNDIALS(bool plotTime_b, int frequencyOrTimeIndex_v, int stateIndex_v, int totalSize_v){
        QVector<double> yAxis;
        QVector<double> xAxis;
        double backGroundScaler=valueContainer->getModel_backgroundScaler();
        double intensityScaler=valueContainer->getModel_intensityScaler();
        QPair<QVector<double>, QVector<double>> data;
        if(plotTime_b){
            std::vector<std::vector<double> > statePopulationPerTimePerFreq_IDA=genericSystemsPerFrequency.at(frequencyOrTimeIndex_v)->getTimeDependentStatePopulationVectors();
            std::vector<double> integrationTimes_IDA=genericSystemsPerFrequency.at(frequencyOrTimeIndex_v)->getTimePoints();
            yAxis=arrayTools->convertVector<std::vector<double>, QVector<double> >(statePopulationPerTimePerFreq_IDA[stateIndex_v]);
            xAxis=arrayTools->convertVector<std::vector<double>, QVector<double> >(integrationTimes_IDA);
            data.first=xAxis;
            data.second=yAxis;
            emit plotDataReady_SUNDIALS(data);
        }
        else{
            xAxis.resize(totalSize_v);
            yAxis.resize(totalSize_v);
            int counter=0;
            for(const auto& freqPoint2 : genericSystemsPerFrequency){
                std::vector<std::vector<double> > statePopulationPerTimePerFreq_IDA=freqPoint2->getTimeDependentStatePopulationVectors();
                yAxis[counter]= statePopulationPerTimePerFreq_IDA[stateIndex_v][frequencyOrTimeIndex_v]*intensityScaler+backGroundScaler;
                xAxis[counter]= freqPoint2->getFrequency();
                counter++;
            }
            data.first=xAxis;
            data.second=yAxis;
            emit plotDataReady_SUNDIALS(data);
        }
    }

    std::vector<double> getIonizedStateVector(){
        int totalSize=valueContainer->getFrequencyRange().size();
        std::vector<double> yAxis;
        yAxis.resize(totalSize);
        int counter=0;
        for(const auto& freqPoint2 : genericSystemsPerFrequency){
            std::vector<std::vector<double> > statePopulationPerTimePerFreq_IDA=freqPoint2->getTimeDependentStatePopulationVectors();
            yAxis[counter]= statePopulationPerTimePerFreq_IDA.back().back();
            counter++;
        }
        return yAxis;
    }
};

#endif // RATEEQUATIONSPROFILE_H
