//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#include "valuecontainerclass.h"

valueContainerClass::valueContainerClass(QObject *parent) :
    QObject(parent)
{
    initialA=6000.0;                    //Initial F state A -factor, in MHz
    initialALimits.first=-1E6;
    initialALimits.second=1E6;
    initialB=0.0;                       //Initial F state B -factor, in MHz
    initialBLimits.first=-1E6;
    initialBLimits.second=1E6;
    finalA=6000.0;                      //Final F state A -factor, in MHz
    finalALimits.first=-1E6;
    finalALimits.second=1E6;
    finalB=0.0;                         //Final F state B -factor, in MHz
    finalBLimits.first=-1E6;
    finalBLimits.second=1E6;
    CoG=1;                              //Center of Gravity, in PHz
    CoGLimits.first=-1E6;
    CoGLimits.second=1E6;

    initialJ=0.5;                       //Spin of the initial J-state, in hbar
    finalJ=0.5;                         //Spin of the final J-state, in hbar
    nucSpin=0.5;                        //Nuclear spin, in MHz

    aCoefficient_1=1E6;                 //Sponatenous transition rate (Einsteins A coefficient), in Hz
    aCoefficientLimits.first=1;
    aCoefficientLimits.second=1E12;
    model_laserIntensity=100.0;               //Laser intensity, in µJoules per cm^2
    model_laserIntensityLimits.first=1E-6;
    model_laserIntensityLimits.second=1E9;
    gaussianWidth=1500.0;               //Laser linewidth (Gaussian), in MHz
    gaussianWidthLimits.first=1E-6;
    gaussianWidthLimits.second=1E6;
    lorentzianWidth=1500.0;             //Additional broadening component (Lorentzian), in MHz
    lorentzianWidthLimits.first=1E-6;
    lorentzianWidthLimits.second=1E6;
    laserPulseWidth=20.0;               //The width of the laser pulse in ns

    intensityScaler=1.0;                //Scaling factor for the laser intensity, in a.u.
    intensityScalerLimits.first=1E-6;
    intensityScalerLimits.second=1E6;
    backgroundScaler=0.0;               //Scaling factor for a background, in a.u.
    backgroundScalerLimits.first=-1E6;
    backgroundScalerLimits.second=1E6;

    afAiRatio=1;                        //Ratio of the Af and Ai -values0.000954808
    bfBiRatio=1;                        //Ratio of the Bf and Bi -values

    calculateVoigtFWHM();
    voigtFWHM=getVoigtFWHM();           //Calculated Voigt FWHM
    fanoParameter=50;                   //Fano parameter
    asymmetryParameter=0;               //Asymmetry Parameter;

    //Misc. UI parameters
    showIndividualPeaks=false;

    //Fitter controls
    enforceLimits=false;
    freeIntensities=false;
    ftol=1E-15;
    xtol=1E-15;
    maxIter=500;

    //Fitter locks
    initialALock=false;
    initialBLock=false;
    finalALock=false;
    finalBLock=false;
    CoGLock=false;

    gaussianWidthLock=false;
    lorentzianWidthLock=false;
    aCoefficientLock=false;
    model_laserIntensityLock=false;

    intensityScalerLock=false;
    backgroundScalerLock=false;

    afAiRatioLock=false;
    bfBiRatioLock=false;

    model_backgroundScalerLock=false;
    model_intensityScalerLock=false;

    measuredProfile.first.clear();
    measuredProfile.second.clear();

    fullRelativeProfile.first.clear();
    fullRelativeProfile.second.clear();

    singleRelativePeaks.clear();

    rateEquationProfile.first.clear();
    rateEquationProfile.second.clear();

    showIndividualPeaks=false;

    //Fitter related initializations:
    fitVariablesHash["initialA"]=initialA;
    fitVariablesHash["initialB"]=initialB;
    fitVariablesHash["finalA"]=finalA;
    fitVariablesHash["finalB"]=finalB;
    fitVariablesHash["CoG"]=CoG;
    fitVariablesHash["gaussianWidth"]=gaussianWidth;
    fitVariablesHash["lorentzianWidth"]=lorentzianWidth;
    fitVariablesHash["intensityScaler"]=intensityScaler;
    fitVariablesHash["backgroundScaler"]=backgroundScaler;

    //Rate quations fitter
    //Fitter related initializations:
    model_fitVariablesHash["initialA"]=initialA;
    model_fitVariablesHash["initialB"]=initialB;
    model_fitVariablesHash["finalA"]=finalA;
    model_fitVariablesHash["finalB"]=finalB;
    model_fitVariablesHash["CoG"]=CoG;
    model_fitVariablesHash["gaussianWidth"]=gaussianWidth;
    model_fitVariablesHash["lorentzianWidth"]=lorentzianWidth;
    model_fitVariablesHash["intensityScaler"]=model_intensityScaler;
    model_fitVariablesHash["backgroundScaler"]=model_backgroundScaler;
    model_fitVariablesHash["laserIntensity"]=model_laserIntensity;


    //Non-resonant-ionization parameters;

    wL_nr=532;                                          //[nm]
    pW_nr=15;                                           //[ns]
    i_nr=5000;                                          //[muJ/cm^s]
    cS_nr=1E-17;                                        //[cm^2}
    gW_nr=1600;                                         //[MHz]
    lW_nr=100;                                          //[MHz]
    ionizingTransitionTimeOffset=0;                     //[ns]
    isNonResonant=true;

    //IDA parameters
    sundials_integrationTimeScaler=10;
    sundials_absoluteTolerance=1E-3;
    sundials_relativeTolerance=1E-3;
    sundials_nlConvCoeff=0.33;
    sundials_maximumNumberOfIntegrationSteps=10000;
    sundials_useJacobian=true;
    sundials_useConstraints=false;
    sundials_timePoints=100;

    //Model control parameters
    model_spontaneousEmissionOn=true;
    model_initialPopulationScaler=50000;
    model_initialPopulationTemperature=293;                   //[K]
    model_backgroundScaler=0;
    model_backgroundScalerLimits.first=-1E6;
    model_backgroundScalerLimits.second=1E6;
    model_intensityScaler=0.000954808;
    model_intensityScalerLimits.first=1E-6;
    model_intensityScalerLimits.second=1E9;

    //Plotting related variables
    sundials_stateToBePlotted=0;
    sundials_timeOrFrequencyPointToBePlotted=0;
    sundials_plotAsAFunctionOfTime=false;
    sundials_maximumTimeOrFrequencyIndex=0;
    sundials_CurrentTimeOrFrequencyIndex=0;
    sundials_CurrentTimeOrFrequencyValue=0;
}

//****************************************
/////*************************************
//Get
/////*************************************
//****************************************
//UI Values
double valueContainerClass::getInitialA(){
    return initialA;
}

std::pair<double, double> valueContainerClass::getInitialALimits(){
    return initialALimits;
}

double valueContainerClass::getInitialB(){
    return initialB;
}

std::pair<double, double> valueContainerClass::getInitialBLimits(){
    return initialBLimits;
}

double valueContainerClass::getFinalA(){
    return finalA;
}

std::pair<double, double> valueContainerClass::getFinalALimits(){
    return finalALimits;
}

double valueContainerClass::getFinalB(){
    return finalB;
}

std::pair<double, double> valueContainerClass::getFinalBLimits(){
    return finalBLimits;
}

double valueContainerClass::getCoG(){
    return CoG;
}

std::pair<double, double> valueContainerClass::getCoGLimits(){
    return CoGLimits;
}

double valueContainerClass::getInitialJ(){
    return initialJ;
}
double valueContainerClass::getfinalJ(){
    return finalJ;
}
double valueContainerClass::getNucSpin(){
    return nucSpin;
}

double valueContainerClass::getACoefficient_1(){
    return aCoefficient_1;
}

std::pair<double, double> valueContainerClass::getACoefficienLimits(){
    return aCoefficientLimits;
}

double valueContainerClass::getModel_laserIntensity(){
    return model_laserIntensity;
}

std::pair<double, double> valueContainerClass::getModel_laserIntensityLimits(){
    return model_laserIntensityLimits;
}

double valueContainerClass::getGaussianWidth(){
    return gaussianWidth;
}
std::pair<double, double> valueContainerClass::getGaussianWidthLimits(){
    return gaussianWidthLimits;
}

double valueContainerClass::getLorentzianWidth(){
    return lorentzianWidth;
}
std::pair<double, double> valueContainerClass::getLorentzianWidthLimits(){
    return lorentzianWidthLimits;
}

double valueContainerClass::getLaserPulseWidth(){
    return laserPulseWidth;
}

double valueContainerClass::getIntensityScaler(){
    return intensityScaler;
}
std::pair<double, double> valueContainerClass::getIntensityScalerLimits(){
    return intensityScalerLimits;
}

double valueContainerClass::getBackgroundScaler(){
    return backgroundScaler;
}

std::pair<double, double> valueContainerClass::getBackgroundScalerLimits(){
    return backgroundScalerLimits;
}

double valueContainerClass::getAfAiRatio(){
    if(afAiRatioLock){
        return afAiRatio;
    }
    else{
        if(initialA!=0){
            return finalA/initialA;
        }
        else{
            return 0;
        }
    }
}
double valueContainerClass::getBfBiRatio(){
    if(bfBiRatioLock){
        return bfBiRatio;
    }
    else{
        if(initialB!=0){
            return finalB/initialB;
        }
        else{
            return 0;
        }
    }
}

double valueContainerClass::getVoigtFWHM(){
    return voigtFWHM;
}

double valueContainerClass::getFanoParameter(){
    return fanoParameter;
}

double valueContainerClass::getAsymmetryParameter(){
    return asymmetryParameter;
}

//Get All Values
QHash<QString, double> valueContainerClass::getUiValues(){
    uIValuesHash["initialA"]=initialA;
    uIValuesHash["initialB"]=initialB;
    uIValuesHash["finalA"]=finalA;
    uIValuesHash["finalB"]=finalB;
    uIValuesHash["CoG"]=CoG;
    uIValuesHash["initialJ"]=initialJ;
    uIValuesHash["finalJ"]=finalJ;
    uIValuesHash["nucSpin"]=nucSpin;
    uIValuesHash["transRate"]=aCoefficient_1;
    uIValuesHash["laserIntensity"]=model_laserIntensity;
    uIValuesHash["gaussianWidth"]=gaussianWidth;
    uIValuesHash["lorentzianWidth"]=lorentzianWidth;
    uIValuesHash["voigtFWHM"]=voigtFWHM;
    uIValuesHash["laserPulseWidth"]=laserPulseWidth;
    uIValuesHash["intensityScaler"]=intensityScaler;
    uIValuesHash["backgroundScaler"]=backgroundScaler;
    uIValuesHash["afAiRatio"]=getAfAiRatio();
    uIValuesHash["bfBiRatio"]=getBfBiRatio();

    return uIValuesHash;
}

//Fitter locks
bool valueContainerClass::getInitialALock(){
    return initialALock;
}
bool valueContainerClass::getInitialBLock(){
    return initialBLock;
}
bool valueContainerClass::getFinalALock(){
    return finalALock;
}
bool valueContainerClass::getFinalBLock(){
    return finalBLock;
}
bool valueContainerClass::getCoGLock(){
    return CoGLock;
}

bool valueContainerClass::getGaussianWidthLock(){
    return gaussianWidthLock;
}
bool valueContainerClass::getLorentzianWidthLock(){
    return lorentzianWidthLock;
}
bool valueContainerClass::getACoefficientLock(){
    return aCoefficientLock;
}
bool valueContainerClass::getModel_laserIntensityLock(){
    return model_laserIntensityLock;
}
bool valueContainerClass::getIntensityScalerLock(){
    return intensityScalerLock;
}
bool valueContainerClass::getBackgroundScalerLock(){
    return backgroundScalerLock;
}
bool valueContainerClass::getAfAiRatioLock(){
    return afAiRatioLock;
}
bool valueContainerClass::getBfBiRatioLock(){
    return bfBiRatioLock;
}
bool valueContainerClass::getModel_backgroundScalerLock(){
    return model_backgroundScalerLock;
}
bool valueContainerClass::getModel_intensityScalerLock(){
    return model_intensityScalerLock;
}

//Get All Locks
QHash<QString, bool> valueContainerClass::getLocks(){
    locks["initialALock"]=initialALock;
    locks["initialBLock"]=initialBLock;
    locks["finalALock"]=finalALock;
    locks["finalBLock"]=finalBLock;
    locks["CoGLock"]=CoGLock;
    locks["gaussianWidthLock"]=gaussianWidthLock;
    locks["lorentzianWidthLock"]=lorentzianWidthLock;
    locks["intensityScalerLock"]=intensityScalerLock;
    locks["backgroundScalerLock"]=backgroundScalerLock;
    locks["afAiRatioLock"]=afAiRatioLock;
    locks["bfBiRatioLock"]=bfBiRatioLock;
    return locks;
}

//Get Profile Data
QPair<QVector<double>, QVector<double> > valueContainerClass::getMeasuredProfile(){
    return measuredProfile;
}

std::vector<double> valueContainerClass::getMeasuredProfileRange(){
    return measuredProfile.first.toStdVector();
}

std::vector<double> valueContainerClass::getMeasuredProfileValues(){
    return measuredProfile.second.toStdVector();
}

QPair<QVector<double>, QVector<double> > valueContainerClass::getFullRelativeProfile(){
    return fullRelativeProfile;
}

QHash<QString, QPair<QVector<double>, QVector<double> > > valueContainerClass::getSingleRelativePeaks(){
    return singleRelativePeaks;
}

QPair<QVector<double>, QVector<double> > valueContainerClass::getRateEquationProfile(){
    return rateEquationProfile;
}

QPair<QVector<double>, QVector<double> >  valueContainerClass::getResidual(QString profile_s){
    QPair<QVector<double>, QVector<double> > residual;
    QVector<double> resVector;
    QVector<double> measProf=measuredProfile.second;
    QVector<double> calcProf;
    if(profile_s=="relative"){
        calcProf=fullRelativeProfile.second;
    }
    if(profile_s=="rrate"){
        calcProf=rateEquationProfile.second;
    }
    for(int i=0; i<measProf.size(); i++){
        double res=measProf.at(i)-calcProf.at(i);
        resVector.append(res);
    }
    residual.first=measuredProfile.first;
    residual.second=resVector;
    return residual;
}

QVector<double> valueContainerClass::getFrequencyRange(){
    return fullRelativeProfile.first;
}

std::vector<double>  valueContainerClass::getFrequencyRangeStd(){
    return fullRelativeProfile.first.toStdVector();
}

double valueContainerClass::getFrequencyAtIndex(int i){
    return fullRelativeProfile.first.at(i);
}

//Get model diagnostics data
QPair<QVector<double>, QVector<double> > valueContainerClass::getIntegrationStepsPerFrequencyPoint(){
    return intergationStepsPerFrequencyPointProfile;
}

///////////////////////
//Get misc.
///////////////////////
bool valueContainerClass::getShowIndividualPeaksState(){
    return showIndividualPeaks;
}

bool valueContainerClass::getEnforceLimits(){
    return enforceLimits;
}

bool valueContainerClass::getFreeIntensities(){
    return freeIntensities;
}

double valueContainerClass::getFtol(){
    return ftol;
}

double valueContainerClass::getXtol(){
    return xtol;
}

double valueContainerClass::getMaxIter(){
    return maxIter;
}

////////
//FITTER
////////
//Set functions for the fitter, this does not emit signals
void valueContainerClass::setFittedProfileParameters(QHash<QString, double> fitVariablesHash_v){
    fitVariablesHash=fitVariablesHash_v;
    initialA=fitVariablesHash["initialA"];
    initialB=fitVariablesHash["initialB"];
    finalA=fitVariablesHash["finalA"];
    finalB=fitVariablesHash["finalB"];
    CoG=fitVariablesHash["CoG"];
    gaussianWidth=fitVariablesHash["gaussianWidth"];
    lorentzianWidth=fitVariablesHash["lorentzianWidth"];
    intensityScaler=fitVariablesHash["intensityScaler"];
    backgroundScaler=fitVariablesHash["backgroundScaler"];
    emitValuesChanged();
}

//Manually emit the valueChanged signal
void valueContainerClass::emitValuesChanged(){
    emit initialAValueChanged(initialA);
    emit initialBValueChanged(initialB);
    emit finalAValueChanged(finalA);
    emit finalBValueChanged(finalB);
    emit CoGValueChanged(CoG);
    emit gaussianWidthValueChanged(gaussianWidth);
    emit lorentzianWidthValueChanged(lorentzianWidth);
    emit intensityScalerValueChanged(intensityScaler);
    emit backgroundScalerValueChanged(backgroundScaler);
    emit readyToCalculate();
}

int valueContainerClass::getNumberOfDataPoints(){
    if(measuredProfile.first.isEmpty()){
        return 0;
    }
    else{
        return measuredProfile.first.size();
    }
}

int valueContainerClass::getNumberOfStaticVariables(){
    return fitVariablesHash.size();
}

std::vector<double> valueContainerClass::getParameterValues(){
    //Initial Fit parameters, in MHZ where applicable
    std::vector<double> parameterValues(getNumberOfStaticVariables());
    parameterValues[0]=initialA;
    parameterValues[1]=initialB;
    parameterValues[2]=finalA;
    parameterValues[3]=finalB;
    parameterValues[4]=CoG*1E9;
    parameterValues[5]=gaussianWidth;
    parameterValues[6]=lorentzianWidth;
    parameterValues[7]=intensityScaler;
    parameterValues[8]=backgroundScaler;
    return parameterValues;
}

std::vector<bool> valueContainerClass::getParameterLocks(){
    int n=getNumberOfStaticVariables()+getNumberOfAllowedTransitions();
    std::vector<bool> parameterLocks(n);
    parameterLocks[0]=initialALock;
    parameterLocks[1]=initialBLock;
    parameterLocks[2]=finalALock;
    parameterLocks[3]=finalBLock;
    parameterLocks[4]=CoGLock;
    parameterLocks[5]=gaussianWidthLock;
    parameterLocks[6]=lorentzianWidthLock;
    parameterLocks[7]=intensityScalerLock;
    parameterLocks[8]=backgroundScalerLock;
    for(int i=9;i<(n);i++){
        parameterLocks[i]=!getFreeIntensities();
    }
    return parameterLocks;
}

std::vector<std::pair<double, double> > valueContainerClass::getParameterLimits(){
    int n=getNumberOfStaticVariables()+getNumberOfAllowedTransitions();
    std::vector<std::pair<double, double> > parameterLimits(n);
    parameterLimits[0].first=initialALimits.first*1E6;
    parameterLimits[0].second=initialALimits.second*1E6;

    parameterLimits[1].first=initialBLimits.first*1E6;
    parameterLimits[1].second=initialBLimits.second*1E6;

    parameterLimits[2].first=finalALimits.first*1E6;
    parameterLimits[2].second=finalALimits.second*1E6;

    parameterLimits[3].first=finalBLimits.first*1E6;
    parameterLimits[3].second=finalBLimits.second*1E6;

    parameterLimits[4].first=CoGLimits.first*1E15;
    parameterLimits[4].second=CoGLimits.second*1E15;

    parameterLimits[5].first=gaussianWidthLimits.first*1E6;
    parameterLimits[5].second=gaussianWidthLimits.second*1E6;

    parameterLimits[6].first=lorentzianWidthLimits.first*1E6;
    parameterLimits[6].second=lorentzianWidthLimits.second*1E6;

    parameterLimits[7]=intensityScalerLimits;
    parameterLimits[8]=backgroundScalerLimits;

    for(int i=9;i<(n);i++){
        std::pair<double, double> tmp;
        tmp.first=0;
        tmp.second=1;
        parameterLimits[i]=tmp;
    }
    return parameterLimits;
}

std::vector<double> valueContainerClass::getFitProfile(){
    return measuredProfile.second.toStdVector();
}

std::vector<double> valueContainerClass::getFitFrequencyRange(){
    return measuredProfile.first.toStdVector();
}

std::vector<std::string> valueContainerClass::getHashOrder(){
    return hashOrder;
}

std::vector<double> valueContainerClass::getOrderedRelativeTransitionIntensities(){
    std::vector<double> relInt;
    for(const auto &i : hashOrder)
    {
        if(getFreeIntensities()){
            relInt.push_back(fittedFreeIntensities.value(QString::fromStdString(i)));
        }
        else{
            relInt.push_back(relativeTransitionIntensities[i]);
        }
    }
    return relInt;
}

std::vector<double> valueContainerClass::getOrderedSpontaneousDecayRates_1(){
    std::vector<double> spontDecRates;
    for(const auto & relInt: getOrderedRelativeTransitionIntensities()){
        spontDecRates.push_back(relInt*aCoefficient_1);
    }
    return spontDecRates;
}

int valueContainerClass::getfitNumberOfLockedParameters(){
    int lockCounter=0;
    std::vector<bool> fpl=getParameterLocks();
    int size=fpl.size()-getNumberOfAllowedTransitions();
    int i=0;
    for(i; i<size; i++){
        if(fpl[i]==true){
            lockCounter++;
        }
    }
    //A Hack to fix the locking of the free intensities
    for(i; i<fpl.size();i++){
        if(fpl[i]==false){
            lockCounter++;
        }
    }

    return lockCounter;
}

QHash<QString, double> valueContainerClass::getFittedFreeIntensities(){
    return fittedFreeIntensities;
}

//Hyperfine parameters
std::unordered_map<std::string, double> valueContainerClass::getRelativeTransitionIntensities(){
    return relativeTransitionIntensities;
}

std::unordered_map<std::string, double> valueContainerClass::getRelativeTransitionFrequencies(){
    return relativeTransitionFrequencies;
}

QHash<QString, int> valueContainerClass::getListOfInitialStateIds(){
    return listOfInitialStateIds;
}

QHash<QString, int> valueContainerClass::getListOfFinalStateIds(){
    return listOfFinalStateIds;
}

QStringList valueContainerClass::getListOfAllowedTransitions(){
    return listOfAllowedTransitions;
}

int valueContainerClass::getNumberOfAllowedTransitions(){
    return hashOrder.size();
}

int valueContainerClass::getNumberOfInitialFStates(){
    return listOfInitialStateIds.size();
}

int valueContainerClass::getNumberOfFinalFStates(){
    return listOfFinalStateIds.size();
}

void valueContainerClass::setModel_fittedProfileParameters(QHash<QString, double> model_fitVariablesHash_v){
    model_fitVariablesHash=model_fitVariablesHash_v;
    initialA=model_fitVariablesHash["initialA"];
    initialB=model_fitVariablesHash["initialB"];
    finalA=model_fitVariablesHash["finalA"];
    finalB=model_fitVariablesHash["finalB"];
    CoG=model_fitVariablesHash["CoG"];
    gaussianWidth=model_fitVariablesHash["gaussianWidth"];
    lorentzianWidth=model_fitVariablesHash["lorentzianWidth"];
    model_intensityScaler=model_fitVariablesHash["intensityScaler"];
    model_backgroundScaler=model_fitVariablesHash["backgroundScaler"];
    model_laserIntensity=model_fitVariablesHash["laserIntensity"];
    emitModel_valuesChanged();
}

void valueContainerClass::emitModel_valuesChanged(){
    emit initialAValueChanged(initialA);
    emit initialBValueChanged(initialB);
    emit finalAValueChanged(finalA);
    emit finalBValueChanged(finalB);
    emit CoGValueChanged(CoG);
    emit gaussianWidthValueChanged(gaussianWidth);
    emit lorentzianWidthValueChanged(lorentzianWidth);
    emit model_intensityScalerValueChanged(model_intensityScaler);
    emit model_backgroundScalerValueChanged(model_backgroundScaler);
    emit model_laserIntensityValueChanged(model_laserIntensity);
    emit readyToCalculate();
}

int valueContainerClass::getModel_numberOfStaticVariables(){
    return model_fitVariablesHash.size();
}

std::vector<double> valueContainerClass::getModel_parameterValues(){
    //Initial Fit parameters, in MHZ where applicable
    std::vector<double> parameterValues(getModel_numberOfStaticVariables());
    parameterValues[0]=initialA;
    parameterValues[1]=initialB;
    parameterValues[2]=finalA;
    parameterValues[3]=finalB;
    parameterValues[4]=CoG*1E9;
    parameterValues[5]=gaussianWidth;
    parameterValues[6]=lorentzianWidth;
    parameterValues[7]=model_intensityScaler;
    parameterValues[8]=model_backgroundScaler;
    parameterValues[9]=model_laserIntensity;
    return parameterValues;
}

std::vector<bool> valueContainerClass::getModel_parameterLocks(){
    int s=getModel_numberOfStaticVariables();
    int n=s+getNumberOfAllowedTransitions();
    std::vector<bool> parameterLocks(n);
    parameterLocks[0]=initialALock;
    parameterLocks[1]=initialBLock;
    parameterLocks[2]=finalALock;
    parameterLocks[3]=finalBLock;
    parameterLocks[4]=CoGLock;
    parameterLocks[5]=gaussianWidthLock;
    parameterLocks[6]=lorentzianWidthLock;
    parameterLocks[7]=model_intensityScalerLock;
    parameterLocks[8]=model_backgroundScalerLock;
    parameterLocks[9]=model_laserIntensityLock;
    for(int i=s;i<(n);i++){
        parameterLocks[i]=!getFreeIntensities();
    }
    return parameterLocks;
}

std::vector<std::pair<double, double> > valueContainerClass::getModel_parameterLimits(){
    int n=getModel_numberOfStaticVariables()+getNumberOfAllowedTransitions();
    std::vector<std::pair<double, double> > parameterLimits(n);
    parameterLimits[0].first=initialALimits.first*1E6;
    parameterLimits[0].second=initialALimits.second*1E6;

    parameterLimits[1].first=initialBLimits.first*1E6;
    parameterLimits[1].second=initialBLimits.second*1E6;

    parameterLimits[2].first=finalALimits.first*1E6;
    parameterLimits[2].second=finalALimits.second*1E6;

    parameterLimits[3].first=finalBLimits.first*1E6;
    parameterLimits[3].second=finalBLimits.second*1E6;

    parameterLimits[4].first=CoGLimits.first*1E15;
    parameterLimits[4].second=CoGLimits.second*1E15;

    parameterLimits[5].first=gaussianWidthLimits.first*1E6;
    parameterLimits[5].second=gaussianWidthLimits.second*1E6;

    parameterLimits[6].first=lorentzianWidthLimits.first*1E6;
    parameterLimits[6].second=lorentzianWidthLimits.second*1E6;

    parameterLimits[7]=model_intensityScalerLimits;
    parameterLimits[8]=model_backgroundScalerLimits;

    parameterLimits[9]=model_laserIntensityLimits;

    for(int i=9;i<(n);i++){
        std::pair<double, double> tmp;
        tmp.first=0;
        tmp.second=1;
        parameterLimits[i]=tmp;
    }
    return parameterLimits;
}

int valueContainerClass::getModel_fitNumberOfLockedParameters(){
    int lockCounter=0;
    std::vector<bool> fpl=getModel_parameterLocks();
    int size=fpl.size()-getNumberOfAllowedTransitions();
    int i=0;
    for(i; i<size; i++){
        if(fpl[i]==true){
            lockCounter++;
        }
    }
    //A Hack to fix the locking of the free intensities
    for(i; i<fpl.size();i++){
        if(fpl[i]==false){
            lockCounter++;
        }
    }

    return lockCounter;
}

///////////////////
//Non-resonant transition
///////////////////

double valueContainerClass::getWl_nr(){
    return wL_nr;
}

double valueContainerClass::getPW_nr(){
    return pW_nr;
}

double valueContainerClass::getI_nr(){
    return i_nr;
}

double valueContainerClass::getCS_nr(){
    return cS_nr;
}

double valueContainerClass::getGW_nr(){
    return gW_nr;
}

double valueContainerClass::getLW_nr(){
    return lW_nr;
}

double valueContainerClass::getTimeOffset(){
    return ionizingTransitionTimeOffset;
}

bool valueContainerClass::getIsNonResonant(){
    return isNonResonant;
}

////////////////////
//Get SUNDIALS parameters
////////////////////
double valueContainerClass::getSundials_integrationTimeScaler(){
    return sundials_integrationTimeScaler;
}

double valueContainerClass::getSundials_absoluteTolerance(){
    return sundials_absoluteTolerance;
}

double valueContainerClass::getSundials_relativeTolerance(){
    return sundials_relativeTolerance;
}

double valueContainerClass::getSundials_nlConvCoeff(){
    return sundials_nlConvCoeff;
}

int valueContainerClass::getSundials_maximumNumberOfIntegrationStep(){
    return sundials_maximumNumberOfIntegrationSteps;
}

bool valueContainerClass::getSundials_useJacobian(){
    return sundials_useJacobian;
}

bool valueContainerClass::getSundials_useConstraints(){
    return sundials_useConstraints;
}

int valueContainerClass::getSundials_TimePoints(){
    return sundials_timePoints;
}

///////////////////////////////
//Get model control parameters
//////////////////////////////
bool valueContainerClass::getModel_spontaneousEmissionOn(){
    return model_spontaneousEmissionOn;
}

double valueContainerClass::getModel_initialPopulationScaler(){
    return model_initialPopulationScaler;
}

double valueContainerClass::getModel_initialPopulationTemperature(){
    return model_initialPopulationTemperature;
}

double valueContainerClass::getModel_backgroundScaler(){
    return model_backgroundScaler;
}

std::pair<double, double> valueContainerClass::getModel_backgroundScalerLimits(){
    return model_backgroundScalerLimits;
}

double valueContainerClass::getModel_intensityScaler(){
    return model_intensityScaler;
}
std::pair<double, double> valueContainerClass::getModel_intensityScalerLimits(){
    return model_intensityScalerLimits;
}

/////////////////////////////////
//Get plotting related variables
/////////////////////////////////
int valueContainerClass::getSundials_StateToBePlotted(){
    return sundials_stateToBePlotted;
}

int valueContainerClass::getSundials_TimeOrFrequencyPointIndexToBePlotted(){
    return sundials_timeOrFrequencyPointToBePlotted;
}

bool valueContainerClass::getSundials_PlotAsAFunctionOfTime(){
    return sundials_plotAsAFunctionOfTime;
}

int valueContainerClass::getSundials_maximumTimeOrFrequencyIndex(){
    return sundials_maximumTimeOrFrequencyIndex;
}

int valueContainerClass::getSundials_CurrentTimeOrFrequencyIndex(){
    return sundials_CurrentTimeOrFrequencyIndex;
}

double valueContainerClass::getSundials_CurrentTimeOrFrequencyValue(){
    return sundials_CurrentTimeOrFrequencyValue;
}

//****************************************
/////*************************************
//Set
/////*************************************
//****************************************
//////////////////////////////////////////////////////////////
//Calculate Voigt FMHM, and Af/Ai and Bi/Bf ratios.
//////////////////////////////////////////////////////////////
void calculateAfAiRatio();
void calculateBfBiRatio();

void valueContainerClass::calculateVoigtFWHM(){
    double gw=gaussianWidth;
    double lw=lorentzianWidth;
    voigtFWHM=0.5346*lw+sqrt(0.2166*pow(lw,2)+pow(gw,2));
    emit voigtFWHMValueChanged(voigtFWHM);
}
double valueContainerClass::calculateAf(){
    return initialA*afAiRatio;
}
double valueContainerClass::calculateBf(){
    return initialB*bfBiRatio;
}


//UI Values
void valueContainerClass::setInitialA(double newInitialA_v){
    if(initialA!=newInitialA_v){
        if(afAiRatioLock){
            initialA=newInitialA_v;
            finalA=calculateAf();
            afAiRatio=getAfAiRatio();
            setAfAiRatio(afAiRatio);
            emit initialAValueChanged(initialA);
            emit finalAValueChanged(finalA);
            emit readyToCalculate();
        }
        else{
            initialA=newInitialA_v;
            setAfAiRatio(getAfAiRatio());
            emit initialAValueChanged(initialA);
        }
    }
}

void valueContainerClass::setInitialALimits(std::pair<double, double> newInitialALimits_v){
    initialALimits=newInitialALimits_v;
    double lower=initialALimits.first;
    double upper=initialALimits.second;
    if(lower>initialA){
        setInitialA(lower);
    }
    if(upper<initialA){
        setInitialA(upper);
    }


}

void valueContainerClass::setInitialB(double newInitialB_v){
    if(initialB!=newInitialB_v){
        if(bfBiRatioLock){
            initialB=newInitialB_v;
            finalB=calculateBf();
            bfBiRatio=getBfBiRatio();
            setBfBiRatio(bfBiRatio);
            emit initialBValueChanged(initialB);
            emit finalBValueChanged(finalB);
            emit readyToCalculate();
        }
        else{
            initialB=newInitialB_v;
            setBfBiRatio(getBfBiRatio());
            emit initialBValueChanged(initialB);
        }
        emit readyToCalculate();
    }
}

void valueContainerClass::setInitialBLimits(std::pair<double, double> newInitialBLimits_v){
    initialBLimits=newInitialBLimits_v;
    double lower=initialBLimits.first;
    double upper=initialBLimits.second;
    if(lower>initialB){
        setInitialB(lower);
    }
    if(upper<initialB){
        setInitialB(upper);
    }
}

void valueContainerClass::setFinalA(double newFinalA_v){
    if(finalA!=newFinalA_v){
        if(afAiRatioLock){
            finalA=newFinalA_v;
            if(getAfAiRatio()!=0){
                initialA=finalA/getAfAiRatio();
            }
            else{
               initialA=0;
            }
            setAfAiRatio(getAfAiRatio());
            emit finalAValueChanged(finalA);
            emit initialAValueChanged(initialA);
            emit readyToCalculate();

        }
        else{
            finalA=newFinalA_v;
            setAfAiRatio(getAfAiRatio());
            emit finalAValueChanged(newFinalA_v);
        }
    }
}

void valueContainerClass::setFinalALimits(std::pair<double, double> newFinalALimits_v){
    finalALimits=newFinalALimits_v;
    double lower=finalALimits.first;
    double upper=finalALimits.second;
    if(lower>finalA){
        setFinalA(lower);
    }
    if(upper<finalA){
        setFinalA(upper);
    }
}

void valueContainerClass::setFinalB(double newFinalB_v){
    if(finalB!=newFinalB_v){
        if(bfBiRatioLock){
            finalB=newFinalB_v;
            if(getBfBiRatio()!=0){
                setInitialB(finalB/getBfBiRatio());
            }
            else{
                initialB=0;
            }
            setBfBiRatio(getBfBiRatio());
            emit finalBValueChanged(finalB);
            emit initialBValueChanged(initialB);
            emit readyToCalculate();
        }
        else{
            finalB=newFinalB_v;
            setBfBiRatio(getBfBiRatio());
            emit finalBValueChanged(newFinalB_v);
            emit readyToCalculate();
        }
    }
}
void valueContainerClass::setFinalBLimits(std::pair<double, double> newFinalBLimits_v){
    finalBLimits=newFinalBLimits_v;
    double lower=finalBLimits.first;
    double upper=finalBLimits.second;
    if(lower>finalB){
        setFinalB(lower);
    }
    if(upper<finalB){
        setFinalB(upper);
    }
}

void valueContainerClass::setCoG(double newCoG_v){
    if(CoG!=newCoG_v){
        CoG=newCoG_v;
        emit CoGValueChanged(newCoG_v);
        emit readyToCalculate();
    }
}

void valueContainerClass::setCoGLimits(std::pair<double, double> newCoGLimits_v){
    CoGLimits=newCoGLimits_v;
    double lower=CoGLimits.first;
    double upper=CoGLimits.second;
    if(lower>CoG){
        setCoG(lower);
    }
    if(upper<CoG){
        setCoG(upper);
    }
}

void valueContainerClass::setInitialJ(double newInitialJ_v){
    if(initialJ!=newInitialJ_v){
        initialJ=newInitialJ_v;
        emit initialJValueChanged(newInitialJ_v);
        emit readyToCalculate();
    }
}
void valueContainerClass::setFinalJ(double newFinalJ_v){
    if(finalJ!=newFinalJ_v){
        finalJ=newFinalJ_v;
        emit finalJValueChanged(newFinalJ_v);
        emit readyToCalculate();
    }
}
void valueContainerClass::setNucSpin(double newNucSpin_v){
    if(nucSpin!=newNucSpin_v){
        nucSpin=newNucSpin_v;
        emit nucSpinValueChanged(newNucSpin_v);
        emit readyToCalculate();
    }
}

void valueContainerClass::setACoefficient_1(double newACoefficient_v){
    if(aCoefficient_1!=newACoefficient_v){
        aCoefficient_1=newACoefficient_v;
        emit transRateValueChanged(newACoefficient_v);
        emit readyToCalculate();
    }
}
void valueContainerClass::setACoefficienLimits(std::pair<double, double> newACoefficienLimits_v){
    aCoefficientLimits=newACoefficienLimits_v;
    double lower=aCoefficientLimits.first;
    double upper=aCoefficientLimits.second;
    if(lower>aCoefficient_1){
        setACoefficient_1(lower);
    }
    if(upper<aCoefficient_1){
        setACoefficient_1(upper);
    }
}

void valueContainerClass::setModel_laserIntensity(double newModel_laserIntensity_v){
    if(model_laserIntensity!=newModel_laserIntensity_v){
        model_laserIntensity=newModel_laserIntensity_v;
        emit laserIntensityValueChanged(newModel_laserIntensity_v);
        emit readyToCalculate();
    }
}

void valueContainerClass::setModel_laserIntensityLimits(std::pair<double, double>  newModel_laserIntensityLimits_v){
    model_laserIntensityLimits=newModel_laserIntensityLimits_v;
    double lower=model_laserIntensityLimits.first;
    double upper=model_laserIntensityLimits.second;
    if(lower>model_laserIntensity){
        setModel_laserIntensity(lower);
    }
    if(upper<model_laserIntensity){
        setModel_laserIntensity(upper);
    }
}

void valueContainerClass::setGaussianWidth(double newGaussianWidth_v){
    //if(gaussianWidth!=newGaussianWidth_v){
        gaussianWidth=newGaussianWidth_v;
        calculateVoigtFWHM();
        emit gaussianWidthValueChanged(newGaussianWidth_v);
        emit readyToCalculate();
    //}
}

void valueContainerClass::setGaussianWidthLimits(std::pair<double, double> newGaussianWidthLimits_v){
    gaussianWidthLimits=newGaussianWidthLimits_v;
    double lower=gaussianWidthLimits.first;
    double upper=gaussianWidthLimits.second;
    if(lower>gaussianWidth){
        setGaussianWidth(lower);
    }
    if(upper<gaussianWidth){
        setGaussianWidth(upper);
    }
}

void valueContainerClass::setLorentzianWidth(double newLorentzianWidth_v){
    //if(lorentzianWidth!=newLorentzianWidth_v){
        lorentzianWidth=newLorentzianWidth_v;
        calculateVoigtFWHM();
        emit lorentzianWidthValueChanged(newLorentzianWidth_v);
        emit readyToCalculate();
    //}
}

void valueContainerClass::setLorentzianWidthLimits(std::pair<double, double> newLorentzianWidthLimits_v){
    lorentzianWidthLimits=newLorentzianWidthLimits_v;
    double lower=lorentzianWidthLimits.first;
    double upper=lorentzianWidthLimits.second;
    if(lower>lorentzianWidth){
        setLorentzianWidth(lower);
    }
    if(upper<lorentzianWidth){
        setLorentzianWidth(upper);
    }
}
void valueContainerClass::setLaserPulseWidth(double newLaserPulseWidth_v){
    if(laserPulseWidth!=newLaserPulseWidth_v){
        laserPulseWidth=newLaserPulseWidth_v;
        emit laserPulseWidthValueChanged(newLaserPulseWidth_v);
        emit readyToCalculate();
    }
}

void valueContainerClass::setFanoParameter(double newFanoParameter_v){
    fanoParameter=newFanoParameter_v;
}

void valueContainerClass::setAsymmetryParameter(double newAsymmetryParameter_v){
    asymmetryParameter=newAsymmetryParameter_v;
}

void valueContainerClass::setIntensityScaler(double newIntensityScaler_v){
    if(intensityScaler!=newIntensityScaler_v){
        intensityScaler=newIntensityScaler_v;
        emit intensityScalerValueChanged(newIntensityScaler_v);
        emit readyToCalculate();
    }
}

void valueContainerClass::setIntensityScalerLimits(std::pair<double, double> newIntensityScalerLimits_v){
    intensityScalerLimits=newIntensityScalerLimits_v;
    double lower=intensityScalerLimits.first;
    double upper=intensityScalerLimits.second;
    if(lower>intensityScaler){
        setIntensityScaler(lower);
    }
    if(upper<intensityScaler){
        setIntensityScaler(upper);
    }
}

void valueContainerClass::setBackgroundScaler(double newBackgroundScaler_v){
    if(backgroundScaler!=newBackgroundScaler_v){
        backgroundScaler=newBackgroundScaler_v;
        emit backgroundScalerValueChanged(newBackgroundScaler_v);
        emit readyToCalculate();
    }
}

void valueContainerClass::setBackgroundScalerLimits(std::pair<double, double> newBackgroundScalerLimits_v){
    backgroundScalerLimits=newBackgroundScalerLimits_v;
    double lower=backgroundScalerLimits.first;
    double upper=backgroundScalerLimits.second;
    if(lower>backgroundScaler){
        setBackgroundScaler(lower);
    }
    if(upper<backgroundScaler){
        setBackgroundScaler(upper);
    }
}

void valueContainerClass::setAfAiRatio(double newAfAiRatio_v){
    if(afAiRatio!=newAfAiRatio_v){
        afAiRatio=newAfAiRatio_v;
        setFinalA(calculateAf());
        if(getAfAiRatio()!=0){
            setInitialA(finalA/getAfAiRatio());
        }
        else{
            setInitialA(0);
        }
        emit initialAValueChanged(initialA);
        emit finalAValueChanged(finalA);
        emit afAiRatioValueChanged(afAiRatio);
        if(!afAiRatioLock){
            emit readyToCalculate();
        }
    }
}
void valueContainerClass::setBfBiRatio(double newBfBiRatio_v){
    if(bfBiRatio!=newBfBiRatio_v){
        bfBiRatio=newBfBiRatio_v;
        setFinalB(calculateBf());
        if(getBfBiRatio()!=0){
            setInitialB(finalB/getBfBiRatio());
        }
        else{
            setInitialB(0);
        }
        emit initialBValueChanged(initialB);
        emit finalBValueChanged(finalB);
        emit bfBiRatioValueChanged(bfBiRatio);
        if(!bfBiRatioLock){
            emit readyToCalculate();
        }
    }
}
//Get Profile Data
void valueContainerClass::setMeasuredProfile(QPair<QVector<double>, QVector<double> > newMeasuredProfile_v){
    measuredProfile=newMeasuredProfile_v;
    emit measuredProfileChanged(measuredProfile);
}

void valueContainerClass::setFullRelativeProfile(std::pair<std::vector<double>, std::vector<double>> newFullRelativeProfilePair_v){
    fullRelativeProfile.first=QVector<double>::fromStdVector(newFullRelativeProfilePair_v.first);
    fullRelativeProfile.second=QVector<double>::fromStdVector(newFullRelativeProfilePair_v.second);
    emit relativeProfileChanged(fullRelativeProfile);
}

void valueContainerClass::setSingleRelativePeaks(std::unordered_map<std::string,std::pair<std::vector<double>,std::vector<double> > > newSingleRelativePeaks_v){
    singleRelativePeaks.clear();
    for(const auto &peak: newSingleRelativePeaks_v){
        QVector<double> x=QVector<double>::fromStdVector(peak.second.first);
        QVector<double> y=QVector<double>::fromStdVector(peak.second.second);
        QPair<QVector<double>,QVector<double> > pair;
        pair.first=x;
        pair.second=y;
        singleRelativePeaks[QString::fromStdString(peak.first)]=pair;
    }

    emit singleRelativePeaksChanged(singleRelativePeaks);
}

void valueContainerClass::setRateEquationProfile(QPair<QVector<double>, QVector<double> > newRateEquationProfile_v){
    rateEquationProfile=newRateEquationProfile_v;
    emit rateEquationPofileChanged(rateEquationProfile);
}

//Get model diagnostics data
void valueContainerClass::setIntegrationStepsPerFrequencyPoint(QPair<QVector<double>, QVector<double> > newIntergationStepsPerFrequencyPoint_v){
    intergationStepsPerFrequencyPointProfile=newIntergationStepsPerFrequencyPoint_v;
    emit intergationStepsPerFrequencyPointChanged(intergationStepsPerFrequencyPointProfile);
}

/////
//Log
/////
void valueContainerClass::setLog(std::unordered_map<std::string, std::string> log_v){
    QString line;
    QString separator;
    separator.append("=============================");
    emit logValueChanged(separator);
    for(const auto &pair : log_v){
        line= QString::fromStdString(pair.first) + QString::fromStdString(pair.second);
        //line.insert(0, pair );
        emit logValueChanged(line);
    }
}

void valueContainerClass::setFitLog(QStringList log_v){
    fitResults=log_v;
    QString separator;
    separator.append("=============================");
    emit logValueChanged(separator);
    foreach (QString line, fitResults) {
        emit logValueChanged(line);
    }
}

//////
//Misc
//////
void valueContainerClass::setShowIndividualPeaks(bool newShowIndividualPeaksState_b){
    showIndividualPeaks=newShowIndividualPeaksState_b;
    emit showIndividualPeaksChanged(showIndividualPeaks);
}

//////////////
//Fitter controls
//////////////
void valueContainerClass::setEnforceLimits(bool newEnforceLimitsState_b){
    enforceLimits=newEnforceLimitsState_b;
}

void valueContainerClass::setFreeIntensities(bool newFreeIntensitiesState_b){
    freeIntensities=newFreeIntensitiesState_b;
}

void valueContainerClass::setFtol(double newFtol_v){
    ftol=newFtol_v;
}

void valueContainerClass::setXtol(double newXtol_v){
    xtol=newXtol_v;
}

void valueContainerClass::setMaxIter(double newMaxIter_v){
    maxIter=newMaxIter_v;
}

/////////////////////
//Set fitter input
/////////////////////
void valueContainerClass::setHashOrder(std::vector<std::string> newFitHashOrder_v){
    hashOrder=newFitHashOrder_v;
}

void valueContainerClass::setFittedFreeIntensities(QHash<QString, double> newFittedFreeIntensities_v){
    fittedFreeIntensities=newFittedFreeIntensities_v;
}

//////////////
//Fitter locks
//////////////
void valueContainerClass::setInitialALock(bool newInitialALockState_b){
    initialALock=newInitialALockState_b;
    emit initialALockStateChanged(newInitialALockState_b);
}
void valueContainerClass::setInitialBLock(bool newInitialBLockState_b){
    initialBLock=newInitialBLockState_b;
    emit initialBLockStateChanged(newInitialBLockState_b);
}
void valueContainerClass::setFinalALock(bool newFinalALockState_b){
    finalALock=newFinalALockState_b;
    emit finalALockStateChanged(newFinalALockState_b);
}
void valueContainerClass::setFinalBLock(bool newFinalBLockState_b){
    finalBLock=newFinalBLockState_b;
    emit finalBLockStateChanged(newFinalBLockState_b);
}
void valueContainerClass::setCoGLock(bool newCoGLockState_b){
    CoGLock=newCoGLockState_b;
    emit CoGLockStateChanged(newCoGLockState_b);
}

void valueContainerClass::setGaussianWidthLock(bool newGaussianWidthLockState_b){
    gaussianWidthLock=newGaussianWidthLockState_b;
    emit gaussianWidthLockStateChanged(newGaussianWidthLockState_b);
}
void valueContainerClass::setLorentzianWidthLock(bool newLorentzianWidthLockState_b){
    lorentzianWidthLock=newLorentzianWidthLockState_b;
    emit lorentzianWidthLockStateChanged(newLorentzianWidthLockState_b);
}
void valueContainerClass::setACoefficientLock(bool newACoefficientLock_b){
    aCoefficientLock=newACoefficientLock_b;
}
void valueContainerClass::setModel_laserIntensityLock(bool newModel_laserIntensityLock_b){
    model_laserIntensityLock=newModel_laserIntensityLock_b;
    emit model_laserIntensityLockStateChanged(newModel_laserIntensityLock_b);
}
void valueContainerClass::setIntensityScalerLock(bool newIntensityScalerLockState_b){
    intensityScalerLock=newIntensityScalerLockState_b;
    emit intensityScalerLockStateChanged(newIntensityScalerLockState_b);
}
void valueContainerClass::setBackgroundScalerLock(bool newBackgroundScalerLockState_b){
    backgroundScalerLock=newBackgroundScalerLockState_b;
    emit backgroundScalerLockStateChanged(newBackgroundScalerLockState_b);
}

void valueContainerClass::setAfAiRatioLock(bool newAfAiRatioLockLockState_b){
    afAiRatioLock=newAfAiRatioLockLockState_b;
    emit afAiRatioLockStateChanged(newAfAiRatioLockLockState_b);
}
void valueContainerClass::setBfBiRatioLock(bool newBfBiRatioLockLockState_b){
    bfBiRatioLock=newBfBiRatioLockLockState_b;
    emit bfBiRatioLockStateChanged(newBfBiRatioLockLockState_b);
}
void valueContainerClass::setModel_backgroundScalerLock(bool newModel_backgroundScalerLock_b){
    model_backgroundScalerLock=newModel_backgroundScalerLock_b;
}
void valueContainerClass::setModel_intensityScalerLock(bool newModel_intensityScalerLock_b){
    model_intensityScalerLock=newModel_intensityScalerLock_b;
}
//////////////////////////
//Set HyperFine parameters
//////////////////////////
void valueContainerClass::setRelativeTransitionIntensities(std::unordered_map<std::string, double> newRelativeTransitionIntensities_v){
    relativeTransitionIntensities=newRelativeTransitionIntensities_v;
}

void valueContainerClass::setRelativeTransitionFrequencies(std::unordered_map<std::string, double> newRelativeTransitionFrequencies_v){
    relativeTransitionFrequencies=newRelativeTransitionFrequencies_v;
}

void valueContainerClass::setListOfInitialStateIds(QHash<QString, int> newListOfInitialStateIds_v){
    listOfInitialStateIds=newListOfInitialStateIds_v;
}

void valueContainerClass::setListOfFinalStateIds(QHash<QString, int> newListOfFinalStateIds_v){
    listOfFinalStateIds=newListOfFinalStateIds_v;
}

void valueContainerClass::setListOfAllowedTransitions(QStringList newListOfAllowedTransitions_v){
    listOfAllowedTransitions=newListOfAllowedTransitions_v;
}

///////////////////////////////////
//Set non-resonant step properties
//////////////////////////////////
void valueContainerClass::setWl_nr(double newWl_nr){
    wL_nr=newWl_nr;
}

void valueContainerClass::setPW_nr(double newPW_nr){
    pW_nr=newPW_nr;
}

void valueContainerClass::setI_nr(double newI_nr){
    i_nr=newI_nr;
}

void valueContainerClass::setCS_nr(double newCS_nr){
    cS_nr=newCS_nr;
}

void valueContainerClass::setGW_nr(double newGW_nr){
    gW_nr=newGW_nr;
}

void valueContainerClass::setLW_nr(double newlW_nr){
    lW_nr=newlW_nr;
}

void valueContainerClass::setTimeOffset(double newTimeOffset){
    ionizingTransitionTimeOffset=newTimeOffset;
}

void valueContainerClass::setIsNonResonant(bool newIsNonResonant){
    isNonResonant=newIsNonResonant;
}

////////////////////
//Set SUNDIALS parameters
////////////////////
void valueContainerClass::setSundials_integrationTimeScaler(double newSundials_integrationTimeScaler_v){
    sundials_integrationTimeScaler=newSundials_integrationTimeScaler_v;
}

void valueContainerClass::setSundials_absoluteTolerance(double newSundials_absoluteTolerance_v){
    sundials_absoluteTolerance=newSundials_absoluteTolerance_v;
}

void valueContainerClass::setSundials_relativeTolerance(double newSundials_relativeTolerance_v){
    sundials_relativeTolerance=newSundials_relativeTolerance_v;
}

void valueContainerClass::setSundials_nlConvCoeff(double newSundials_nlConvCoeff_v){
    sundials_nlConvCoeff=newSundials_nlConvCoeff_v;
}

void valueContainerClass::setSundials_maximumNumberOfIntegrationStep(double newIdas_maximumNumberOfIntegrationStep_v){
    sundials_maximumNumberOfIntegrationSteps=static_cast<int>(newIdas_maximumNumberOfIntegrationStep_v+0.5);
}

void valueContainerClass::setSundials_userJacobian(bool newSundials_useJacobian_b){
    sundials_useJacobian=newSundials_useJacobian_b;
}

void valueContainerClass::setSundials_useConstraints(bool newSundials_useConstraints_b){
    sundials_useConstraints=newSundials_useConstraints_b;
}

void valueContainerClass::setSundials_TimePoints(double newSundials_timePoints_v){
    sundials_timePoints=static_cast<int>(newSundials_timePoints_v+0.5);
}
///////////////////////////
//Set model parameters
///////////////////////////
void valueContainerClass::setModel_spontanousEmissionOn(bool newModel_spontaneousEmissionOn_b){
    model_spontaneousEmissionOn=newModel_spontaneousEmissionOn_b;
}

void valueContainerClass::setModel_initialPopulationScaler(double newModel_initialPopulationScaler_v){
    model_initialPopulationScaler=newModel_initialPopulationScaler_v;
}

void valueContainerClass::setModelInitial_populationTemperature(double newModel_initialPopulationTemperature_v){
    model_initialPopulationTemperature=newModel_initialPopulationTemperature_v;
}

double valueContainerClass::setModel_backgroundScaler(double newModel_backgroudScaler_v){
    model_backgroundScaler=newModel_backgroudScaler_v;
}

void valueContainerClass::setModel_backgroundScalerLimits(std::pair<double, double> newModel_backgroundScalerLimits_v){
    model_backgroundScalerLimits=newModel_backgroundScalerLimits_v;
}

double valueContainerClass::setModel_intensityScaler(double newModel_intensityScaler_v){
    model_intensityScaler=newModel_intensityScaler_v;
}

void valueContainerClass::setModel_intensityScalerLimits(std::pair<double, double> newModel_intensityScalerLimits_v){
    model_intensityScalerLimits=newModel_intensityScalerLimits_v;
}


/////////////////////////////////
//Set plotting related variables
/////////////////////////////////
void valueContainerClass::setSundials_StateToBePlotted(int newSundials_StateToBePlotted_v){
    sundials_stateToBePlotted=newSundials_StateToBePlotted_v;
}

void valueContainerClass::setSundials_TimeOrFrequencyPointToBePlotted(double newSundials_TimePointToBePlotted_v){
    sundials_timeOrFrequencyPointToBePlotted=static_cast<int>(newSundials_TimePointToBePlotted_v+0.5);
}

void valueContainerClass::setSundials_PlotAsAFunctionOfTime(bool newSundials_PlotAsAFunctionOfTime_b){
    sundials_plotAsAFunctionOfTime=newSundials_PlotAsAFunctionOfTime_b;
}
void valueContainerClass::setSundials_maximumTimeOrFrequencyIndex(int newSundials_maximumTimeOrFrequencyIndex_v){
    sundials_maximumTimeOrFrequencyIndex=static_cast<int>(newSundials_maximumTimeOrFrequencyIndex_v);
    emit sundials_maximumTimeOrFrequencyIndexChanged(sundials_maximumTimeOrFrequencyIndex);
}

void valueContainerClass::setSundias_CurrentTimeOrFrequencyIndex(double newSundials_CurrentTimeOrFrequencyIndex_v){
    sundials_CurrentTimeOrFrequencyIndex=static_cast<int>(newSundials_CurrentTimeOrFrequencyIndex_v);
    emit sundials_CurrentTimeOrFrequencyIndexChanged(sundials_CurrentTimeOrFrequencyIndex);
}
void valueContainerClass::setSundials_CurrentTimeOrFrequencyValue(double newSundials_CurrentTimeOrFrequencyValue_v){
    sundials_CurrentTimeOrFrequencyValue=newSundials_CurrentTimeOrFrequencyValue_v;
    emit sundials_CurrentTimeOrFrequencyValueChanged(sundials_CurrentTimeOrFrequencyValue);
}
