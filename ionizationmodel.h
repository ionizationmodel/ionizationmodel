//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef IONIZATIONMODEL_H
#define IONIZATIONMODEL_H

#include <QMainWindow>
#include <mainplot.h>
#include <QKeyEvent>
#include <openspectrumdialog.h>
#include <QPair>
#include <valuecontainerclass.h>
#include <rateEquationsProfile.h>
#include <hyperfinecalculations.h>
#include <relativeprofile.h>
#include <hyperfinecalculations.h>
#include <relativeintensityfitter.h>
#include <rateequationsfitter.h>
#include <memory>
#include <iostream>
#include <fstream>


namespace Ui {
class ionizationModel;
}

class ionizationModel : public QMainWindow
{
    Q_OBJECT

public:
    explicit ionizationModel(QWidget *parent = 0);
    ~ionizationModel();
private:
    Ui::ionizationModel *ui;
    valueContainerClass *valueContainer;
    rateEquationsProfile *rateqProfile;
    hyperfineCalculations *hyperFine;
    relativeProfile *relProfile;
    mainPlot *hfsPlot;
    mainPlot *diagnosticsPlot;
    openSpectrumDialog * openSpec;
    std::shared_ptr<relativeIntensityFitter> relativeFitter;
    std::shared_ptr<rateEquationsFitter> rateFitter;

protected:
    void keyPressEvent(QKeyEvent *event ); //keypresses

public slots:
    void setInitialALimits();
    void setInitialBLimits();
    void setFinalALimits();
    void setFinalBLimits();
    void setCoGLimits();
    void setGaussianWidthLimits();
    void setLorentzianWidthLimits();
    void setIntensityScalerLimits();
    void setBackgroundScalerLimits();
    void setModel_intensityScalerLimits();
    void setModel_backgroundScalerLimits();
    void setModel_laserIntensityLimits();
    void statesReadyForPlotting(int noOfInitialStates, int noOfFinalStates, int ionizingStateNo, int lastState);
    void IDASplotAsAFunctionOfTIme(bool plot);
    void clearSpectraClicked();
    void runRateEquationsFitter();
    void saveData();
    void ainittest(double value);

signals:
    void initialALimitsChanged(std::pair<double, double> newInitialALimits_v);
    void initialBLimitsChanged(std::pair<double, double> newInitialBLimits_v);
    void finalALimitsChanged(std::pair<double, double> newFinalALimits_v);
    void finalBLimitsChanged(std::pair<double, double> newFinalBLimits_v);
    void CoGLimitsChanged(std::pair<double, double> newCoGLimits_v);
    void gaussianWidthLimitsChanged(std::pair<double, double> newGaussianWidthLimits_v);
    void lorentzianWidthLimitsChanged(std::pair<double, double> newLorentzianWidthLimits_v);
    void intensityScalerLimitsChanged(std::pair<double, double> newIntensityScalerLimits_v);
    void backgroundScalerLimitsChanged(std::pair<double, double> newBackgroundScalerLimits_v);
    void model_intensityScalerLimitsChanged(std::pair<double, double> newModel_intensityScalerLimits_v);
    void model_backgroundScalerLimitsChanged(std::pair<double, double> newModel_backgroundScalerLimits_v);
    void model_laserIntensityLimitsChanged(std::pair<double, double> newModel_laserIntensityLimits_v);
};


#endif // IONIZATIONMODEL_H
