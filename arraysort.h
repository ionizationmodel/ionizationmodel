//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef ARRAYSORT_H
#define ARRAYSORT_H

#include <QVector>
#include <QPair>
#include <algorithm>
#include <vector>
#include <iostream>
#include <boost/unordered_map.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <typeinfo>
#include <unordered_map>
#include <QDebug>

class arraySort
{
public:
    arraySort(){
    }
    ~arraySort(){
    }
    QPair<QVector<double>, QVector<double> > sortVectorPair(QPair<QVector<double>, QVector<double> > vectorPair_v){
        vectorPair=vectorPair_v;
        QVectorToArray();
        std::sort(stdVectorPair.begin(), stdVectorPair.end(),compare);
        return sortedVectorPair;
    }

    template<class inputHash, class outputHash>
    outputHash convertHash(inputHash hash){
        outputHash tempHash;
        tempHash.rehash(hash.size());
        auto end = hash.cend();
        std::string tempString;
        for(auto it = hash.cbegin(); it != end; ++it) {
            tempString=it.key().toStdString();
            tempHash[tempString]=it.value();
        }
        return tempHash;
    }
    template<class inputHash, class outputContainer>
    outputContainer extractValueFromHash(inputHash hash){
        outputContainer tempContainer;
        auto end = hash.cend();
        for(auto it = hash.cbegin(); it != end; ++it) {
            tempContainer.push_back(*it);
        }
        return tempContainer;
    }
    template<class inputVector, class outputVector>
    outputVector convertVector(inputVector vector_v){
        outputVector tempVector;
        tempVector.reserve(vector_v.size());
        auto end=vector_v.cend();
        for(auto it=vector_v.cbegin(); it!=end;it++){
            tempVector.push_back(static_cast<double>(*it));
        }
        return tempVector;
    }

    std::vector<double> convolve(const std::vector<double>& a, const std::vector<double>& b)
    {
        int n_a = a.size();
        int n_b = b.size();
        std::vector<double> result(n_a + n_b - 1);
        for (int i = 0; i < n_a + n_b - 1; ++i) {
        double sum = 0.0;
        for (int j = 0; j <= i; ++j) {
        sum += ((j < n_a) && (i-j < n_b)) ? a[j]*b[i-j] : 0.0;
           }
            result[i] = sum;
        }
        return result;
    }

private:
    void QVectorToArray(){
        xColSize=vectorPair.first.size();
        stdVectorPair.clear();
        for(int i=0; i<vectorPair.first.size(); i++){
            QVector<double> row;
            double wl;
            double count;
            wl=vectorPair.first.at(i);
            count=vectorPair.second.at(i);
            row.append(wl);
            row.append(count);
            std::vector<double> rowVector=row.toStdVector();
            stdVectorPair.push_back(rowVector);
        }
    }
    void arrayToQVector(){
        QVector<double> qXCol;
        QVector<double> qYCol;
        sortedVectorPair.first.clear();
        sortedVectorPair.second.clear();
        for(unsigned int i=0; i<stdVectorPair.size(); i++ ) {
            qDebug()<<stdVectorPair[i][1];
            qXCol.append(stdVectorPair[i][0]);
            qYCol.append(stdVectorPair[i][1]);
        }
        sortedVectorPair.first=qXCol;
        sortedVectorPair.second=qYCol;
    }

    template <class THash>
    boost::unordered_map<std::string, THash> QHashToBoost(QHash<QString, THash> QHash_v){
        boost::unordered_map<std::string, THash> boostMap;
        foreach (QString key, QHash_v.keys()) {
            boostMap[key.toStdString()] =QHash_v.value(key);
        }
        return boostMap;
    }
    template <class TVector>
    boost::numeric::ublas::vector<TVector> QVectorToBoost(QVector<TVector> QVector_v){
        int qVsize=QVector_v.size();
        boost::numeric::ublas::vector<TVector> boostVector (qVsize);
        for(int i=0;i<qVsize;i++){
            boostVector.insert_element(i, QVector_v.at(i));
        }
        return boostVector;
    }



    static bool compare(const std::vector<double> &xA, const std::vector<double>&yA){
            return xA[0] < yA[0];
    }
    std::vector<std::vector<double> > stdVectorPair;
    QPair<QVector<double>, QVector<double> > vectorPair;
    QPair<QVector<double>, QVector<double> >sortedVectorPair;
    int xColSize;
};

#endif
