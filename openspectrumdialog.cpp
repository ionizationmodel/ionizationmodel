//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#include "openspectrumdialog.h"
#include "ui_openspectrumdialog.h"

openSpectrumDialog::openSpectrumDialog(QWidget *parent, valueContainerClass *valueContainer_p, bool testmode_v) :
    QDialog(parent),
    ui(new Ui::openSpectrumDialog)
{
    setParent(parent);
    ui->setupUi(this);
    //Setup the plotwidget
    hfsPlot=new mainPlot(this);
    hfsPlot->setMinimumSize(580,500);
    hfsPlot->setMaximumSize(580,500);
    hfsPlot->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    ui->plotLo->addWidget(hfsPlot,Qt::AlignTop);
    valueContainer=valueContainer_p;
    testmode=testmode_v;
    //Array sorter
    sortArray = new arraySort;
    //Number of columns in a typical Tisa:scan file
    standardColumnNo=7;
    //column separators
    columnSeparator.append("\t");
    columnSeparator.append(" ");
    columnSeparator.append(",");
    c= 2.99792458E8;

    //listDirectory();
    connect(ui->openDir, &QPushButton::clicked, this, &openSpectrumDialog::listDirectory);
    connect(ui->fileList, &QListWidget::currentTextChanged, this, &openSpectrumDialog::fileListItemSelectionChanged);
}

openSpectrumDialog::~openSpectrumDialog()
{
    //delete sortArray;
}

void openSpectrumDialog::showUpUi(){
    this->show();
}

void openSpectrumDialog::hideUi(){
    this->hide();
}



void openSpectrumDialog::openFiles(QString filename_v, QString sep_v){
    QFile file;
    QString line;
    QStringList rowContent;
    QVector<double> xCol;
    QVector<double> yCol;
    if(!path.isEmpty() && !filename_v.isEmpty()){
        file.setFileName(filename_v);
        if(file.open(QIODevice::ReadWrite)){
            while(!file.atEnd()){
                line=file.readLine();
                rowContent=line.split(sep_v,QString::SkipEmptyParts);
                double x;
                double y;
                x=convertToMHz(rowContent.at(0).toDouble())*ui->harmonicSb->value();
                y=rowContent.at(1).toDouble();
                xCol.append(x);
                yCol.append(y);
            }
            measuredProfile.first=xCol;
            measuredProfile.second=yCol;
        }

        file.close();

    }
}

QVector<QVector<double> > openSpectrumDialog::prepareColumnVectors(int noOfColumns){
    QVector<QVector<double> > singleFileSpectrum;
    for (int columnNo=0; columnNo<noOfColumns; columnNo++){       //Create needed number of QVector columns
        QVector<double> singleFileColumn;
        singleFileSpectrum.append(singleFileColumn);         //append the columns to thesinglefilecolumn container
    }
    return singleFileSpectrum;
}

int openSpectrumDialog::checkRowSize(QStringList firstRow){
    return firstRow.size();
}

//Modifier functions
double openSpectrumDialog::convertToMHz(double value){
    return (c/(value*1E-9)/1E6);
}

double openSpectrumDialog::setHarmonic(double value){
    return value*ui->harmonicSb->value();
}


//Slots
//Open file dialog to select the directory with the spectra files
//These files are then put into the ui
void openSpectrumDialog::listDirectory(){
    homeDir= QDir::home().absolutePath();
    QStringList dataExtensions;
    dataExtensions<<"*.dat"<<"*.txt";
    QFileDialog selectFolder(this);
    path= selectFolder.getExistingDirectory(this,tr("Open Directory"),homeDir, QFileDialog::DontResolveSymlinks);

    if (!path.isEmpty())
    {
        directory.setPath(path);
        directory.setNameFilters(dataExtensions);
        ui->fileList->clear();
        ui->fileList->addItems(directory.entryList());
    }
    else{
        ui->fileList->clear();
        path.clear();
        directory.setPath(path);
    }
}

void openSpectrumDialog::fileListItemSelectionChanged(const QString &currentText)
{
    selectedFile.clear();
    selectedFile.append(path + QDir::separator()+currentText);
    openFiles(selectedFile, "\t");
    valueContainer->setMeasuredProfile(measuredProfile);
}

