//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef FITTERSYSTEM_H
#define FITTERSYSTEM_H
#include <vector>
#include <idas/idas.h>
#include <idas/idas_dense.h>
#include <cvodes/cvodes.h>
#include <cvodes/cvodes_dense.h>
#include <nvector/nvector_serial.h>
#include <sundials/sundials_math.h>
#include <sundials/sundials_types.h>
#include <math.h>
#include <iostream>
#ifdef __linux__
//Ubuntu specific eigen include
#include <Eigen/Dense>
#include <eigen3/unsupported/Eigen/NonLinearOptimization>
#include <eigen3/unsupported/Eigen/AutoDiff>
#endif
#define IJth(A,i,j) DENSE_ELEM(A,i,j)

class fitterSystem
{
public:
    explicit fitterSystem(){
        data=new fitterParameters();
        allocated=false;
    }
    ~fitterSystem(){
        cleanIDASSystem();
        delete data;
    }
    //automatic differentiation
    typedef Eigen::AutoDiffScalar<Eigen::Matrix<realtype,Eigen::Dynamic,1>> ADS;
    typedef Eigen::Matrix<ADS, Eigen::Dynamic, 1> VectorXad;
    struct  fitterParameters{

        static constexpr double c= 2.99792458E8;                    //[m/s]
        static constexpr double pi = 3.14159265358979323846;
        static constexpr double hBar=6.6260755e-34/(2*pi);          //[Js]
        static constexpr double ln2=0.6931471805599453;
        static constexpr double k=1.3806488e-23;                    //[J/K]
        //["initialA"]=xP[0];
        //["initialB"]=xP[1];
        //["finalA"]=xP[2];
        //["finalB"]=xP[3];
        //["CoG"]=xP[4];
        //["gaussianWidth"]=xP[5];
        //["lorentzianWidth"]=xP[6];
        //["intensityScaler"]=xP[7];
        //["backgroundScaler"]=xP[8];
        int numberOfParameters;
        int numberOfTransitions;
        int numberOfEquations;
        int numberOfSensitivities;
        int numberOfStates;
        double laserFrequency;
        double ionizingTransitionRate;
        double measuredValue;
        std::vector<double> initialTransitionRates;
        std::vector<double> laserIntensities;
        std::vector<double> ACoefficients;
        std::vector< std::vector<double>> spontaneousDecayRateVectors;
        Eigen::VectorXd xValues;
        std::vector<Eigen::VectorXd> xSubValues;
        VectorXad xADSValues;
        std::vector<VectorXad> xADSSubValues;
        std::vector< std::vector<double>> laserPulseProperties;
        std::vector<std::vector<int>> initialStateIds;
        std::vector<std::vector<int>> finalStateIds;
        std::vector<int> numberOfInitialStates;
        std::vector<int> numberOfFinalStates;
        std::vector<std::vector<std::vector<int>>> orderedAllowedStates;

        int ionizedStateId;

        std::vector<std::vector<double>> alphasAndBetas;
        std::vector<double> initialConditions;
        std::vector<int> initialTransitionNumbers;
        std::vector<double> initialValues;

        double integrationTimeScaler;

        //control parameters
        bool setConstraints=false;
        bool setUserJacobian=true;
        double relativeTolerance;
        std::vector<double> absoluteTolerances;
        double totalPopulation;
        int timePoints;
        double setNonlinConvCoef;
        int maxNumSteps;


        realtype* p= (realtype*) realloc (NULL, 10*sizeof(realtype));
        void reallocP(int size){
            p= (realtype*) realloc (NULL, size*sizeof(realtype));
        }

        realtype getGaussianLineShape(const realtype t, const realtype laserPulseWidth_v, const realtype laserPulseDelay_v=0){
            realtype x=laserPulseProperties[0][0]*integrationTimeScaler/2+laserPulseDelay_v;
            realtype w=laserPulseWidth_v/(2*sqrt(log(4)));
            realtype gaussianLine_v=(1/(w*sqrt(2*pi)))*exp(-pow((t-x),2)/(2*pow(w, 2)));
            return gaussianLine_v;
        }
        template<typename Scalar=realtype>
        Scalar voigtProfileFunction(Scalar transitionFrequency_v, Scalar lorentzianWidth_v, Scalar gaussianWidth_v, realtype laserFrequency_v){
            int i;
            Scalar A[4],B[4],C[4],D[4];
            Scalar V=0;
            Scalar retVal=0;
            static Scalar sqrtln2=sqrt(ln2);
            static Scalar sqrtpi=sqrt(pi);
            Scalar X=(laserFrequency_v-transitionFrequency_v)*2*sqrtln2/gaussianWidth_v;
            Scalar Y=lorentzianWidth_v*sqrtln2/gaussianWidth_v;
            A[0]=-1.2150; B[0]= 1.2359;
            A[1]=-1.3509; B[1]= 0.3786;
            A[2]=-1.2150; B[2]=-1.2359;
            A[3]=-1.3509; B[3]=-0.3786;
            C[0]=-0.3085; D[0]= 0.0210;
            C[1]= 0.5906; D[1]=-1.1858;
            C[2]=-0.3085; D[2]=-0.0210;
            C[3]= 0.5906; D[3]= 1.1858;
            for(i=0;i <= 3;i++){
                V=V+(C[i]*(Y-A[i])+D[i]*(X-B[i]))/(((Y-A[i])*(Y-A[i]))+((X-B[i])*(X-B[i])));
            }
            retVal=((lorentzianWidth_v*(1/(pi*lorentzianWidth_v))*sqrtpi*sqrtln2/(gaussianWidth_v))*V);
            return retVal;
        }
        template<typename Scalar=realtype>
        Scalar getTransitionFrequency(Scalar CoG_v, Scalar initialA_v,  Scalar initialB_v, Scalar finalA_v, Scalar finalB_v,
                                      realtype initialAlpha_v,  realtype initialBeta_v, realtype finalAlpha_v, realtype finalBeta_v){
            Scalar transFreq=CoG_v+finalA_v*finalAlpha_v+finalB_v*finalBeta_v-initialA_v*initialAlpha_v-initialB_v*initialBeta_v;
            return transFreq;
        }

        template<typename Scalar=realtype>
        Scalar calculateNumberOfTransitions(realtype laserFrequency_v,
                                            Scalar relativeIntensity_v,Scalar lorentzianWidth_v,
                                            Scalar gaussianWidth_v, Scalar laserIntensity_v,
                                            Scalar CoG_v, Scalar initialA_v,  Scalar initialB_v,
                                            Scalar finalA_v, Scalar finalB_v,
                                            realtype initialAlpha_v,  realtype initialBeta_v,
                                            realtype finalAlpha_v, realtype finalBeta_v,
                                            realtype ACoefficient_v,
                                            realtype gauss_v){

            Scalar tF=getTransitionFrequency<Scalar>(CoG_v,initialA_v,initialB_v,finalA_v,
                                                     finalB_v,initialAlpha_v, initialBeta_v,
                                                     finalAlpha_v,finalBeta_v) *1E6;                                           //transition frequency to Hz
            Scalar lorentzianWidth=lorentzianWidth_v*1E6;
            Scalar gaussianWidth=gaussianWidth_v*1E6;
            realtype laserFrequency=laserFrequency_v*1E6;
            Scalar resWl=(c/tF);                                                                                                        //resonance wavelength in m
            //transition rate taking into account the spontaneous emission rate stimulatedTransitionRate [1/s]",
            //laser intensity [J/m^2]  and spectralfunction(relative intensity and other inputs in MhZ's)
            Scalar lInt= laserIntensity_v*0.01;                                                                                      //[muJ/cm^2] to [J/m^2]
            Scalar photonEnergy=tF*hBar;                                                                                               //[J]
            Scalar photonIntensity=lInt/photonEnergy;                                                                                                   //[photons/m^2]
            realtype refractiveIndex=1;                                                                                                                   //not needed here atm.
            Scalar absorptionCrossSectionStatic=(ACoefficient_v/(2*pi))*(pow(resWl,2)/(4*pow(refractiveIndex,2)));                            //[m^2/s]
            Scalar absorptionCrossSectionDynamic=relativeIntensity_v*voigtProfileFunction<Scalar>(tF, lorentzianWidth, gaussianWidth, laserFrequency);  //[s]
            Scalar absorptionCrossSection=absorptionCrossSectionStatic*absorptionCrossSectionDynamic;                                                   //[m^2]
            Scalar relNoOfTrans=absorptionCrossSection*photonIntensity*gauss_v;                                                                                 //[1] //Total number of transitions per pulse
            return relNoOfTrans;
        }
        template<typename Scalar=realtype>
        Scalar getNumberOfTransitions(const Eigen::Matrix<Scalar,Eigen::Dynamic,1> &xValuesSub_r,
                                      realtype laserFrequency_v,
                                      realtype initialAlpha_v,
                                      realtype initialBeta_v,
                                      realtype finalAlpha_v,
                                      realtype finalBeta_v,
                                      realtype ACoefficient_v,
                                      realtype gauss_v)
        {
            //["initialA"]=xValuesSub_r[0];
            //["initialB"]=xValuesSub_r[1];
            //["finalA"]=xValuesSub_r[2];
            //["finalB"]=xValuesSub_r[3]
            //["CoG"]=xValuesSub_r[4];
            //["gaussianWidth"]=xValuesSub_r[5];
            //["lorentzianWidth"]=xValuesSub_r[6];
            //["laserIntensity"]=xValuesSub_r[7];
            //["relativeIntensity"]=xValuesSub_r[8];
        return calculateNumberOfTransitions<Scalar>(laserFrequency_v, xValuesSub_r[8],
                xValuesSub_r[6],
                xValuesSub_r[5],
                xValuesSub_r[7],
                xValuesSub_r[4],
                xValuesSub_r[0],
                xValuesSub_r[1],
                xValuesSub_r[2],
                xValuesSub_r[3],
                initialAlpha_v,
                initialBeta_v,
                finalAlpha_v,
                finalBeta_v,
                ACoefficient_v,
                gauss_v);
        }

    };
    fitterParameters *data;
    void setupIDASSystem();
    void resetValuesForFitter();
    void runIDASSystem();
    void cleanIDASSystem();
    int referenceFitterIDASDAESystem(realtype tres, N_Vector IDAS_y, N_Vector IDAS_yp,
                                     N_Vector rr, void *user_data);
    int referenceFitterIDASDAESystemJacobi(long Neq, realtype tt, realtype cj, N_Vector IDAS_y,
                                           N_Vector IDAS_yp, N_Vector resvec, DlsMat JJ,
                                           void *user_data, N_Vector tempv1, N_Vector tempv2,
                                           N_Vector tempv3);
    int referenceFitterIDASDAESystemSensitivityRHS(int Ns, realtype tt,
                    N_Vector yy, N_Vector yp, N_Vector resval,
                    N_Vector *yyS, N_Vector *ypS, N_Vector *resvalS,
                    void *user_data,
                    N_Vector tmp1, N_Vector tmp2, N_Vector tmp3);

    int fitterIDASDAESystem(realtype tres, N_Vector IDAS_y, N_Vector IDAS_yp, N_Vector rr,
                            void *user_data);
    int fitterIDASDAESystemJacobi(long Neq, realtype tt, realtype cj, N_Vector IDAS_y,
                                  N_Vector IDAS_yp, N_Vector resvec, DlsMat JJ, void *user_data,
                                  N_Vector tempv1, N_Vector tempv2, N_Vector tempv3);

    //int referenceCVODESODESystem(realtype t, N_Vector CVODES_y, N_Vector CVODES_ydot, void *user_data);
    //int referenceCVODESODESystemJacobi(long int N, realtype t, N_Vector y, N_Vector fy, DlsMat J, void *user_data, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3);

    double getIonizedStatePopulation(){
        return ionizedStatePopulation;
    }

    Eigen::VectorXd getSensitivityDkyEigen(){
        return sensitivitiesEigen;
    }
    //Return a resized sensitivity vector to account for the lack of intensity and Background scalers
    Eigen::VectorXd getSensitivityEigenResized(){
        Eigen::VectorXd resizedSens;
        resizedSens.resize(data->numberOfSensitivities+2);
        resizedSens(0)=sensitivitiesEigen[0];
        resizedSens(1)=sensitivitiesEigen[1];
        resizedSens(2)=sensitivitiesEigen[2];
        resizedSens(3)=sensitivitiesEigen[3];
        resizedSens(4)=sensitivitiesEigen[4];
        resizedSens(5)=sensitivitiesEigen[5];
        resizedSens(6)=sensitivitiesEigen[6];
        resizedSens(7)=0.0;
        resizedSens(8)=0.0;
        resizedSens(9)=sensitivitiesEigen[7];
        int noFofEq=data->numberOfEquations;
        for(int eq=1;eq<data->numberOfEquations;eq++){
            resizedSens((data->numberOfSensitivities-noFofEq)+eq+2)=sensitivitiesEigen[(data->numberOfSensitivities-noFofEq)+eq];
        }
        return resizedSens;
    }
    //Removes intensity and background scalers
    void setParametersFromFitter(const Eigen::VectorXd &x_r){
        data->xValues[0]=x_r[0];
        data->xValues[1]=x_r[1];
        data->xValues[2]=x_r[2];
        data->xValues[3]=x_r[3];
        data->xValues[4]=x_r[4];
        data->xValues[5]=x_r[5];
        data->xValues[6]=x_r[6];
        data->xValues[7]=x_r[9];
        int noFofEq=data->numberOfEquations;
        for(int eq=1;eq<noFofEq;eq++){
            data->xValues[(data->numberOfSensitivities-noFofEq)+eq]=x_r[(data->numberOfSensitivities-noFofEq)+eq+2];
        }
    }

private:
    ///////////
    /// IDAS
    ///////////
    void *IDAS_mem;
    N_Vector IDAS_y, IDAS_yp, IDAS_abstol, IDAS_constraints;
    N_Vector *IDAS_yS, *IDAS_ypS;
    realtype IDAS_rtol, *IDAS_yval, *IDAS_ypval, *IDAS_atval, *IDAS_yconst;
    realtype IDAS_t0, IDAS_tout1, IDAS_tout, IDAS_tret;
    int IDAS_iout, IDAS_retval, IDAS_retvalr;
    bool allocated;
    double resultTimeinterval;
    //Results
    double ionizedStatePopulation;
    std::vector<double> sensitivityDky;
    Eigen::VectorXd sensitivitiesEigen;

    ///////////

    int sigma(){
        if(data->measuredValue=!0){
            return sqrt(data->measuredValue);
        }
        else{
            return 1;
        }
    }

};

#endif // FITTERSYSTEM_H
