#include <hyperfinecalculations.h>
hyperfineCalculations::hyperfineCalculations(QObject *parent, valueContainerClass *valueContainer_p){
    this->setParent(parent);
    sixJCalc = std::make_unique<sixJ>();
    valueContainer=valueContainer_p;
    voigt=std::make_unique<voigtProfile>(valueContainer);
    step=0;
    numberOBoundSteps=1;
    calculateHyperfineTransitions();
    parseHyperfineParameters();
}

hyperfineCalculations::~hyperfineCalculations(){
}

//Get
//returns the spins of the F states
std::vector<double> hyperfineCalculations::getInitialFs(int step_v) const{
    return ionizationSteps[step_v]->getInitialFs();
}

double hyperfineCalculations::getNumberOfInitialFStates(int step_v){
    return ionizationSteps[step_v]->getInitialFs().size();
}

std::vector<double> hyperfineCalculations::getFinalFs(int step_v) const{
    return ionizationSteps[step_v]->getFinalFs();
}
double hyperfineCalculations::getNumberOfFinalFStates(int step_v){
   return ionizationSteps[step_v]->getFinalFs().size();
}

std::vector<int> hyperfineCalculations::getInitialStateIds(int step_v){
    return ionizationSteps[step_v]->getInitialStateIds();
}

std::vector<int> hyperfineCalculations::getFinalStateIds(int step_v){
    return ionizationSteps[step_v]->getFinalStateIds();
}

double hyperfineCalculations::getRelativeTransitionIntensity(int transition, int step_v){
    return ionizationSteps[step_v]->getRelativeIntensity(transition);
}

double hyperfineCalculations::getAlpha(int transition, int step_v){
    if(transition<getNumberOfInitialFStates(step_v)){
        double alpha=ionizationSteps[step_v]->getInitialAlpha(transition);
        return alpha;
    }
    else{
        double alpha=ionizationSteps[step_v]->getFinalAlpha(transition);
        return alpha;
    };
}

double hyperfineCalculations::getBeta(int transition, int step_v){
    if(transition<getNumberOfInitialFStates(step_v)){
        double beta=ionizationSteps[step_v]->getInitialBeta(transition);
        return beta;
    }
    else{
        double beta=ionizationSteps[step_v]->getFinalBeta(transition);
        return beta;
    }
}

std::vector<std::vector<double>> hyperfineCalculations::getAlphasAndBetas(int numberOfAllowedTransitions_i){
    std::vector<std::vector<double>> alphasAndBetas;
    std::vector<double> initialAlpha;
    std::vector<double> initialBeta;
    std::vector<double> finalAlpha;
    std::vector<double> finalBeta;
    for(int idx=0; idx<numberOfAllowedTransitions_i; idx++){
        initialAlpha.push_back(ionizationSteps[step]->getInitialAlpha(idx));
        initialBeta.push_back(ionizationSteps[step]->getInitialBeta(idx));
        finalAlpha.push_back(ionizationSteps[step]->getFinalAlpha(idx));
        finalBeta.push_back(ionizationSteps[step]->getFinalBeta(idx));
    }
    alphasAndBetas.push_back(initialAlpha);
    alphasAndBetas.push_back(initialBeta);
    alphasAndBetas.push_back(finalAlpha);
    alphasAndBetas.push_back(finalBeta);
    return alphasAndBetas;
}


double hyperfineCalculations::getHyperFineDeltaNu(double A_v, double B_v, int transition, int step_v){ //frequency
    double alpha_v=getAlpha(transition, step_v);
    double beta_v=getBeta(transition, step_v);
    double deltaNu=alpha_v*A_v+beta_v*B_v;
    return deltaNu;
}
std::vector<double> hyperfineCalculations::getOrderedTransitionFrequencies(int step_v){
    std::vector<double> orderedTransitionFrequencies_tmp;
    int noas=ionizationSteps[step_v]->getNumberOfAllowedTransitions();
    double frequency;
    for(int t=0;t<noas;t++){
        frequency=getTransitionFrequency(valueContainer->getCoG()*1E9,valueContainer->getInitialA(),
                                         valueContainer->getInitialB(),valueContainer->getFinalA(),
                                         valueContainer->getFinalB(),t,ionizationSteps,step_v);
        orderedTransitionFrequencies_tmp.push_back(frequency);
    }
    return orderedTransitionFrequencies_tmp;
}

std::vector<double> hyperfineCalculations::getOrderedRelativeTransitionIntensities(int step_v){
    return ionizationSteps[step_v]->getOrderedRelativeIntensities();
}

std::vector<std::vector<int>> hyperfineCalculations::getOrderedAllowedStates(int step_v){
    return orderedAllowedStatesPerStep[step_v];
}

int hyperfineCalculations::getNumberOfAllowedTransitions(int step_v){
    return ionizationSteps[step_v]->getNumberOfAllowedTransitions();
}

int hyperfineCalculations::getIonizedStateId(){
    int id=0;
    for(int s=0;s<numberOBoundSteps;s++){
        id+=getNumberOfInitialFStates(s);
        id+=getNumberOfFinalFStates(s);
    }
    return id;
}

void hyperfineCalculations::setNumberOfBoundSteps(int numberOfBoundSteps_v){
    numberOBoundSteps=numberOfBoundSteps_v;
}

void hyperfineCalculations::setStep(int step_v){

    step=step_v;
}


//Calculates the F-states for initial and final J
void hyperfineCalculations::calculateFStates(double Ji_v,double Jf_v, double I, std::vector<double> &initialF_r, std::vector<double> &finalF_r){
    //states arranged in initialF and finalF
    //starting from |I-J|;
    double ImJl;
    double IpJl;
    double ImJu;
    double IpJu;
    if (!checkSelectionRules(Ji_v, Jf_v)){
        initialF_r.clear();
        finalF_r.clear();
    }
    else{
        initialF_r.clear();
        ImJl=fabs(I-Ji_v);
        IpJl=I+Ji_v;
        while(ImJl<=IpJl){
            initialF_r.push_back(ImJl);
            ImJl=ImJl+1;
        }
        finalF_r.clear();
        ImJu=fabs(I-Jf_v);
        IpJu=I+Jf_v;
        while(ImJu<=IpJu){
            finalF_r.push_back(ImJu);
            ImJu=ImJu+1;
        }
    }
}

double hyperfineCalculations::getTransitionFrequency(double CoG_v, double initialA_v,
                                                     double initialB_v, double finalA_v,
                                                     double finalB_v, int transition,
                                                     const std::vector<std::unique_ptr<ionizationStep> > &ionizationSteps_r,
                                                     const int &step_r){
    double initialAlpha=ionizationSteps_r[step_r]->getInitialAlpha(transition);
    double initialBeta=ionizationSteps_r[step_r]->getInitialBeta(transition);
    double finalAlpha=ionizationSteps_r[step_r]->getFinalAlpha(transition);
    double finalBeta=ionizationSteps_r[step_r]->getFinalBeta(transition);
    return voigt->getTransitionFrequency<double>(CoG_v, initialA_v, initialB_v, finalA_v, finalB_v,
                                         initialAlpha, initialBeta, finalAlpha, finalBeta);
}

void hyperfineCalculations::parseTransitionInfo(int initialF_v, int finalF_v, std::string initialStateId, std::string finalStateId, double intensity_v, std::unordered_map<std::string, std::string> &transitionInfo_r){
    std::string transitionsInfoString;
    std::string tmp=initialStateId+"->"+finalStateId;
    transitionsInfoString.append(": ");
    transitionsInfoString.append(" Initial F: ");
    transitionsInfoString.append(std::to_string(initialF_v));
    transitionsInfoString.append(" Final F: ");
    transitionsInfoString.append(std::to_string(finalF_v));
    transitionsInfoString.append(" Transition intensity: ");
    transitionsInfoString.append(std::to_string(intensity_v));
    transitionInfo_r[tmp]=transitionsInfoString;
}

bool hyperfineCalculations::checkSelectionRules(double initialSpin_v, double finalSpin_v){
    if(((fabs(initialSpin_v-finalSpin_v)==1 || fabs(initialSpin_v-finalSpin_v)==0) ) && !(initialSpin_v==0 && finalSpin_v==0)){
        return true;
    }
    else{
        return false;
    }
}


//Generates hyperFineStatesProperties -instances for each of the F states
//Also fills them with relevant properties
void hyperfineCalculations::generateHyperfineStates(double initialJ_r, double finalJ_r, double I_r,
                                                    int &numberOfAllowedTransitions_r,
                                                    std::unordered_map<std::string, std::string> &transitionInfo_r,
                                                    std::vector<std::unique_ptr<ionizationStep>> &ionizationSteps_r,
                                                    std::vector<std::string> &hashOrder_r,
                                                    std::vector<std::vector<std::vector<int> > > &orderedAllowedStatesPerStep_r,
                                                    const std::vector<double> initialF_r,
                                                    const std::vector<double> finalF_r,
                                                    const int &step_r){
    hashOrder_r.clear();
    transitionInfo_r.clear();
    bool isInitialState=true;
    std::string initialStateId;
    std::string finalStateId;
    double intensity=0;
    int initialStateIdCounter=0; //counts the initial state id n
    int finalStateIdCounter=0; //counts the final state id n
    numberOfAllowedTransitions_r=0;
    std::vector<std::vector<int>> orderedInitialAllowedStates;
    //Hyperfine state are generated as F=|I-intialJ|,...,F=I+finalJ.
    if(!initialF_r.empty() && !finalF_r.empty()){
        for (const auto &F_v_i: initialF_r) {
            double initialAlpha=alpha(F_v_i, initialJ_r, I_r);
            double initialBeta=beta(F_v_i, initialJ_r, I_r);
            std::vector<int> orderedFinalAllowedStates;
            initialStateId="x["+std::to_string(initialStateIdCounter)+"]";
            auto newInitialState = std::make_unique<hyperFineStateProperties>(initialStateId);
            newInitialState->setN(initialStateIdCounter);
            newInitialState->setTotalFStatesAtJ(initialF_r.size());
            newInitialState->setIsInitialState(isInitialState);
            newInitialState->setAlphaAndBeta(initialAlpha, initialBeta);
            newInitialState->setFI(F_v_i, initialJ);
            finalStateIdCounter=0;
            for (const auto &F_v_f: finalF_r){
                double finalAlpha=alpha(F_v_f, finalJ_r, I_r);
                double finalBeta=beta(F_v_f, finalJ_r, I_r);
                std::unique_ptr<hyperFineStateProperties> newFinalState;
                bool secondLoop;
                finalStateId="x["+std::to_string(initialF_r.size()+finalStateIdCounter)+"]";
                if(ionizationSteps_r[step_r]->getNumberOfFinalStates()<finalF_r.size()){
                    newFinalState = std::make_unique<hyperFineStateProperties>(finalStateId);
                    secondLoop=false;
                }
                else{
                    newFinalState=ionizationSteps_r[step_r]->getFinalHyperFineState(finalStateIdCounter);
                    secondLoop=true;
                }
                newFinalState->setN(initialStateIdCounter+initialF_r.size());
                newFinalState->setTotalFStatesAtJ(finalF_r.size());
                newFinalState->setIsInitialState(false);
                newFinalState->setAlphaAndBeta(alpha(F_v_f, finalJ_r, I_r), beta(F_v_f, finalJ_r, I_r));
                newFinalState->setFI(F_v_f, finalJ);
                //Check if a transition is possible
                if(checkSelectionRules(F_v_i, F_v_f)){
                    intensity=relativeIntensities(F_v_i,F_v_f, initialJ_r, finalJ_r, I_r);
                    newFinalState->addAllowedTransition(finalStateId+"->"+initialStateId,intensity);
                    newInitialState->addAllowedTransition(initialStateId+"->"+finalStateId,intensity);
                    ionizationSteps_r[step_r]->setAllowedTransition(intensity);
                    ionizationSteps_r[step_r]->setInitialStateId(initialStateIdCounter);
                    ionizationSteps_r[step_r]->setInitialAlpha(initialAlpha);
                    ionizationSteps_r[step_r]->setInitialBeta(initialBeta);
                    ionizationSteps_r[step_r]->setFinalAlpha(finalAlpha);
                    ionizationSteps_r[step_r]->setFinalBeta(finalBeta);
                    ionizationSteps_r[step_r]->setFinalStateId(finalStateIdCounter);
                    numberOfAllowedTransitions_r++;
                    hashOrder_r.push_back(initialStateId+"->"+finalStateId);
                    orderedFinalAllowedStates.push_back(finalStateIdCounter);
                    parseTransitionInfo(F_v_i, F_v_f, initialStateId, finalStateId, intensity, transitionInfo_r);
                }
                if(secondLoop){
                    ionizationSteps_r[step_r]->setFinalHyperFineStates(std::move(newFinalState),true, finalStateIdCounter);
                }
                else{
                    ionizationSteps_r[step_r]->setFinalHyperFineStates(std::move(newFinalState));
                }
                finalStateIdCounter++;
            }
            ionizationSteps_r[step_r]->setInitialHyperFineStates(std::move(newInitialState));
            orderedInitialAllowedStates.push_back(orderedFinalAllowedStates);
            initialStateIdCounter++;
        }
    }
    orderedAllowedStatesPerStep_r[step_r]=orderedInitialAllowedStates;
}


//Functions related to calculating the relative transition intensities
double hyperfineCalculations::alpha(double F, double J, double I){
    if(I==0 || J==0){
        return 0;
    }
    else{
        return gamma(F,I,J)/2.0;
    }
}

double hyperfineCalculations::beta(double F, double J, double I){
    if(I==0 || I==0.5 || J==0 || J==0.5){
        return 0;
    }
    else{
        double C=gamma(F,I,J);
        return (3*C*(C+1) - 4*I*(I+1)*J*(J+1))/(8*I*(2*I-1)*J*(2*J-1));
    }
}

double hyperfineCalculations::gamma(double F, double I, double J){
    return (F*(F+1) - I*(I+1) - J*(J+1));
}

double hyperfineCalculations::relativeIntensities(double Fi_v,double Ff_v, double Ji_v, double Jf_v, double I){
    //    The equation for the transition rates between the ground and excited hfs states from
    //    Hyperfine Interactions (2005) 162:3–14
    //    Laser Spectroscopic Investigation of the Element Fermium (Z = 100)
    //    H. Backe, A. Dretzke, St. Fritzsche, R. G. Haire, P. Kunz, W. Lauth, M. Sewtz, N. Trautmann
    double sixj=sixJCalc->sixJValue(Jf_v,Ff_v, I, Fi_v, Ji_v,1);
    return ((2*Fi_v+1)*(2*Ff_v+1)/(2*I+1))*pow(sixj,2);
}

void hyperfineCalculations::parseTransitionFrequenciesAndIntensities(double initialA_v, double initialB_v,
                                                                     double finalA_v, double finalB_v, double CoG_v,
                                                                     std::unordered_map<std::string, double> &relativeTransitionFrequencies_r,
                                                                     std::unordered_map<std::string, double> &relativeTransitionIntensities_r,
                                                                     std::vector<double> &orderedTransitionFrequencies_r,
                                                                     std::vector<double> &orderedRelativeTransitionIntensities_r,
                                                                     const std::vector<std::string> &hashOrder_r, const std::vector<std::unique_ptr<ionizationStep> > &ionizationSteps_r, const int &step_r){
    relativeTransitionIntensities_r.clear();
    relativeTransitionFrequencies_r.clear();
    orderedTransitionFrequencies_r.clear();
    orderedRelativeTransitionIntensities_r.clear();
    double tI;
    double tF;
    int transitionCounter=0;
    for (const auto &transId: hashOrder_r) {
        tI=getRelativeTransitionIntensity(transitionCounter);
        tF=getTransitionFrequency(CoG_v,initialA_v,initialB_v,finalA_v,finalB_v, transitionCounter, ionizationSteps_r, step_r);
        relativeTransitionIntensities_r[transId]=tI;
        orderedRelativeTransitionIntensities_r.push_back(tI);
        relativeTransitionFrequencies_r[transId]=tF;
        orderedTransitionFrequencies_r.push_back(tF);
        transitionCounter++;
    }
}

//Public slots
void hyperfineCalculations::calculateHyperfineTransitions(){
    initialJ=valueContainer->getInitialJ();
    finalJ=valueContainer->getfinalJ();
    nucSpin=valueContainer->getNucSpin();
    ionizationSteps.clear();
    ionizationSteps.push_back(std::make_unique<ionizationStep>(initialJ, finalJ));
    orderedAllowedStatesPerStep.resize(10000);
    calculateFStates(initialJ, finalJ, nucSpin,initialF, finalF);
    generateHyperfineStates(initialJ, finalJ, nucSpin,
                            numberOfAllowedTransitions,transitionInfo,
                            ionizationSteps,hashOrder,orderedAllowedStatesPerStep, initialF,finalF,step);
    parseHyperfineParameters();
    emit calculationsDone(transitionInfo);
}
void hyperfineCalculations::parseHyperfineParameters(){
    parseTransitionFrequenciesAndIntensities(valueContainer->getInitialA(),valueContainer->getInitialB(),
            valueContainer->getFinalA(),valueContainer->getFinalB(),valueContainer->getCoG()*1E9,
            relativeTransitionFrequencies,relativeTransitionIntensities,
            orderedTransitionFrequencies,orderedRelativeTransitionIntensities, hashOrder,
            ionizationSteps, step);
    ionizationSteps[step]->setACoefficient(valueContainer->getACoefficient_1());
    ionizationSteps[step]->setLaserPulseWidth(valueContainer->getLaserPulseWidth());
    ionizationSteps[step]->setLaserPulseDelay(0.0);
    ionizationSteps[step]->setGaussianWidth(valueContainer->getGaussianWidth());
    ionizationSteps[step]->setLorentzianWidth(valueContainer->getLorentzianWidth());
    ionizationSteps[step]->setLaserPulseIntensity(valueContainer->getModel_laserIntensity());
    ionizationSteps[step]->setCenterOfGravity(valueContainer->getCoG());
    emit emitRelativeTransitionIntensities(relativeTransitionIntensities);
    emit emitRelativeTransitionFrequencies(relativeTransitionFrequencies);
    emit emitHashOrder(hashOrder);
    emit parsingDone();
}
