#-------------------------------------------------
#
# Project created by QtCreator 2013-03-22T09:52:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QT += printsupport
CONFIG += qwt
TARGET = ionizationModel
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++14 -fopenmp
QMAKE_CXXFLAGS +=-march=native -Og -pipe -mpreferred-stack-boundary=4 -finline-small-functions -momit-leaf-frame-pointer -lpthread

SOURCES += main.cpp\
        ionizationmodel.cpp \
    mainplot.cpp \
    openspectrumdialog.cpp \
    valuecontainerclass.cpp \
    genericSystem.cpp \
    ionizationstep.cpp \
    hyperfinestateproperties.cpp \
    hyperfinecalculations.cpp \
    fittersystem.cpp

HEADERS  += ionizationmodel.h \
    hyperfinecalculations.h \
    voigtprofile.h \
    mainplot.h \
    hyperfinestateproperties.h \
    sixj.h \
    relativeintensityfitter.h \
    openspectrumdialog.h \
    arraysort.h \
    relativeprofile.h \
    valuecontainerclass.h \
    rateEquationsProfile.h \
    genericSystem.h \
    ionizationstep.h \
    rateequationsfitter.h \
    fittersystem.h \
    3rdParty/fast-cpp-csv-parser/csv.h

FORMS    += ionizationmodel.ui \
    openspectrumdialog.ui

#BOOST
unix|win32: INCLUDEPATH += $(BOOST)

#QWT and other required libraries
             LIBS +=-L$(QWT)/lib -lqwt \
                    -L$(BOOST)/lib -lboost_thread -lboost_system -lboost_timer -lboost_chrono \
                    -fopenmp
unix|win32: INCLUDEPATH += $(QWT)/include/
unix|win32: DEPENDPATH += $(QWT)/include/


#EIGEN
INCLUDEPATH += $(EIGEN)
#EIGEN UNSUPPORTED
INCLUDEPATH += $(EIGEN)\unsupported


win32:QMAKE_POST_LINK += copy /Y $(QWT)\lib\qwt.dll $(DESTDIR) &
win32:QMAKE_POST_LINK += copy /Y $(QTDIR)\bin\Qt5Core.dll $(DESTDIR) &
win32:QMAKE_POST_LINK  += copy /Y $(QTDIR)\bin\Qt5Gui.dll $(DESTDIR) &
win32:QMAKE_POST_LINK  += copy /Y $(QTDIR)\bin\Qt5Svg.dll $(DESTDIR) &

OTHER_FILES += \
    LICENCE.txt \
    README.txt \
    testi.dat

#Sundials
unix:!macx: LIBS += -L$(SUNDIALS)/lib -lsundials_idas -lsundials_nvecserial -lsundials_cvodes
INCLUDEPATH += $(SUNDIALS)/include
DEPENDPATH += $(SUNDIALS)/include
unix:!macx: PRE_TARGETDEPS += $(SUNDIALS)/lib/libsundials_idas.a
