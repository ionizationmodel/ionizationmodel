//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef MAINPLOT_H
//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#define MAINPLOT_H
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_series_data.h>
#include <qwt_math.h>
#include <qwt_plot_zoomer.h>
#include <qwt_plot_renderer.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_panner.h>
#include <qwt_plot_magnifier.h>
#include <qwt_plot_picker.h>
#include <qwt_picker_machine.h>
#include <qwt_plot_dict.h>
#include <qwt_curve_fitter.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_multi_barchart.h>
#include <QHash>
#include <algorithm>
#include <QPrinter>
#include <QDir>
#include <QPair>
class mainPlot : public QwtPlot
{
    Q_OBJECT
public:
    explicit mainPlot(QWidget *parent = 0);
    ~mainPlot();


    void resetView();

private:
    QwtPlotCurve *calculatedProfile;
    QwtPlotCurve *experimentalspectrum;
    QwtPlotCurve *rateEquationsProfile;
    QwtPlotMultiBarChart *diagnosticsBarPlot;
    QwtPlotCurve *diagnosticsLinePlot;
    QwtPlotMagnifier *zoom;
    QwtPlotPanner *pan;
    QwtPlotGrid *grid;
    QwtPlotPicker *picker;

    QVector<double> calcXData;
    QVector<double> calcYData;
    QHash<QString, double> transFreqs;
    QVector<double> expXData;
    QVector<double> expYData;
    QVector<double> rateYData;

    QHash<QString,QPair<QVector<double>,QVector<double> > >  calcSingleRelativePeakPairs;
    QwtPlotCurve *singleRelativePeak;
    QList<QwtPlotCurve*> singleRelativePeaks;
    QwtPlotMarker *transitionLabel;
    QList<QwtPlotMarker*> transitionLabels;

    QVector<double> convertToPHz(QVector<double> inMHz){
        QVector<double> inPHz;
        foreach (double value, inMHz) {
            inPHz.append(value/1E9);
        }
        return inPHz;
    }


signals:
    
public slots:
    void setFullRelativeProfilePair(QPair<QVector<double>, QVector<double> > newFullRelativeProfilePair_v);
    void setRateEquationsProfilePair(QPair<QVector<double>, QVector<double> > newRateEquationsProfilePair_v);
    void setSingleRelativePeakPairs(QHash<QString,QPair<QVector<double>,QVector<double> > > newSingleRelativePeakPairs_v);
    void setMeasuredProfilePair(QPair<QVector<double>, QVector<double> > newMeasuredProfilePair_v);
    void setNumberOfIntegrationStepsPlot(QPair<QVector<double>, QVector<double> > newIntegrationStepsPerFrequencyPointProfile_v);
    void hideCalculatedSpectra(bool hide);
    void savePlot();

private:
    void setCalculatedProfile(QVector<double> calcXData_v, QVector<double> calcYData_v);
    void setExperimentalProfile(QVector<double> expXData_v, QVector<double> expYData_v);
    void setCalculatedSinglePeaks(QHash<QString,QPair<QVector<double>,QVector<double> > > singleRelativePeaks_v);
    void setRateEquationsProfile(QVector<double> rateXData_v, QVector<double> rateYData_v);
    void setDiagnosticsPlot(QVector<double> diagXData_v, QVector<double> diagYData_v, bool isBarPlot=true);
};

#endif // MAINPLOT_H
