//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef GENERICSYSTEM_H
#define GENERICSYSTEM_H

#include <unordered_map>
#include <vector>
#include <idas/idas.h>
#include <idas/idas_dense.h>
#include <cvodes/cvodes.h>
#include <cvodes/cvodes_dense.h>
#include <nvector/nvector_serial.h>
#include <sundials/sundials_math.h>
#include <sundials/sundials_types.h>
#include <math.h>
#include <qdebug.h>
#include <iostream>
//Class for the operator responsible for solving the rate-equations
//The equations are derived from those given in Backe et al., Laser Spectroscopic Investigation
//of the Element Fermium (Z=100), Hyp. Int (2005) 162:3-14
//The basic strucutre adapted from idaRoberts_dns.c
//* Programmer(s): Allan Taylor, Alan Hindmarsh and
//*                Radu Serban @ LLNL

/* Macro to define dense matrix elements */
#define IJth(A,i,j) DENSE_ELEM(A,i,j)
/* Prototypes of functions called by IDA */

class genericSystem{
public:
    explicit genericSystem(){
        parameters=new genericParameters();
    }
    ~genericSystem(){
        delete parameters;
    }
    struct  genericParameters{
        //New code to use bare vectors rather than maps
        //These store the number of transitions and relative intensities of the given ionization step at a single frequencypoint
        std::vector< std::vector<double>> m_numberOfTransitionsVectors;
        std::vector< std::vector<double>> m_spontaneousDecayRateVectors;
        std::vector< std::vector<double>> m_laserPulseProperties; //Inner vector holds the laser pulse properties; pulseWidth, pulseDelay
        std::vector<int> m_numberOfInitialStates;
        std::vector<int> m_numberOfFinalStates;
        std::vector<std::vector<int>> m_initialStateIds;
        std::vector<std::vector<int>> m_finalStateIds;
        std::vector<std::vector<std::vector<int>>> m_orderedAllowedStates;
        int m_ionizedStateId;
        std::vector<std::string> m_hashOrder_1;
        std::vector<realtype> problemParameters_1;
        //General variables for  calculating the gaussian laser pulse
        double m_integrationTimeScaler;

        //Model control variables
        bool m_isSpontaenousEmissionOn=false;

        //Set-up parameters
        int m_numberOfEquations;
        std::vector<double> m_initialPopulations;
        std::vector<double> m_initialTransitionRates;

        //control parameters
        bool m_setConstraints=false;
        bool m_setUserJacobian=true;
        double m_relativeTolerance;
        std::vector<double> m_absoluteTolerances;
        double m_totalPopulation;
        int m_timePoints;

        double m_setNonlinConvCoef;
        int m_MaxNumSteps;

        double getGaussianLineShape(const double t, const double laserPulseWidth_v, const double laserPulseDelay_v=0){
            double x=m_laserPulseProperties[0][0]*m_integrationTimeScaler/2+laserPulseDelay_v;
            double w=laserPulseWidth_v/(2*sqrt(log(4)));
            double gaussianLine_v=(1/(w*sqrt(2*3.14)))*exp(-pow((t-x),2)/(2*pow(w, 2)));
            return gaussianLine_v;
        }

        int getNumberOfEquations(int step_v=0){
            int iniS=m_numberOfInitialStates[step_v];
            int finS=m_numberOfFinalStates[step_v];
            return iniS+finS+1;
        }

        int getNumberOfTransitions(int step_v=0){
            int transitions=m_numberOfTransitionsVectors[step_v].size();
            int ionizingTransitions=m_numberOfTransitionsVectors.back().size();
            return transitions+ionizingTransitions;
        }

        int getNumberOfResonantTransitions(int step_v=0){
            int transitions=m_numberOfTransitionsVectors[step_v].size();
            return transitions;
        }

        int getNumberOfStates(int step=0){
            return m_initialStateIds[step].size()+m_finalStateIds[step].size();
        }

    };
    genericParameters *parameters;
    void setupIDASSystem();
    void setupCVODESSystem();

    void runIDASSystem();
    void runCVODESSystem();

    void cleanIDASSystem();
    void cleanCVODESSystem();

    int referenceIDASDAESystem(realtype tres, N_Vector IDAS_y, N_Vector IDAS_yp, N_Vector rr, void *user_data);
    int referenceIDASDAESystemJacobi(long Neq, realtype tt, realtype cj, N_Vector IDAS_y, N_Vector IDAS_yp, N_Vector resvec, DlsMat JJ, void *user_data, N_Vector tempv1, N_Vector tempv2, N_Vector tempv3);

    int referenceCVODESODESystem(realtype t, N_Vector CVODES_y, N_Vector CVODES_ydot, void *user_data);
    int referenceCVODESODESystemJacobi(long int N, realtype t, N_Vector y, N_Vector fy, DlsMat J, void *user_data, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3);

    int IDASDAESystem(realtype tres, N_Vector IDAS_y, N_Vector IDAS_yp, N_Vector rr, void *user_data);
    int IDASDAESystemJacobi(long Neq, realtype tt, realtype cj, N_Vector IDAS_y, N_Vector IDAS_yp, N_Vector resvec, DlsMat JJ, void *user_data, N_Vector tempv1, N_Vector tempv2, N_Vector tempv3);

    int CVODESODESystem(realtype t, N_Vector CVODES_y, N_Vector CVODES_ydot, void *user_data);
    int CVODESODESystemJacobi(long int N, realtype t, N_Vector y, N_Vector fy, DlsMat J, void *user_data, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3);


    //GET
    std::vector<double> getStatePopulations(){
        return statePopulations;
    }

    std::vector<double> getNumberOfIntegrationSteps(){
        return numberOfIntegrationSteps;
    }

    std::vector<std::vector<double> >getTimeDependentStatePopulationVectors(){
        return timeDependentStatePopulationVectors;
    }

    std::vector<double> getIonizedStateSensitivityDky(){
        std::vector<double> Dky;
        int ionsizedState=parameters->getNumberOfStates();
        for(int j=0; j<parameters->getNumberOfResonantTransitions();j++){
            Dky.push_back(sensitivityDky[j][ionsizedState]);
        }
        return Dky;
    }

    std::vector<double> getTimePoints(){
        return timePoints;
    }

    double getFrequency(){
        return frequency;
    }

    //SET
    void setFrequency(double frequency_v){
        frequency=frequency_v;
    }

private:
    double frequency;

    ///////////
    /// IDAS
    ///////////
    void *IDAS_mem;
    N_Vector IDAS_y, IDAS_yp, IDAS_abstol, IDAS_constraints;
    N_Vector *IDAS_yS, *IDAS_ypS;
    realtype IDAS_rtol, *IDAS_yval, *IDAS_ypval, *IDAS_atval, *IDAS_yconst;
    realtype IDAS_t0, IDAS_tout1, IDAS_tout, IDAS_tret;
    int IDAS_iout, IDAS_retval, IDAS_retvalr;
    double resultTimeinterval;
    ///////////
    /// CVODES
    ///////////
    void *CVODES_mem;
    realtype CVODES_reltol, CVODES_t, CVODES_t0, CVODES_tout, CVODES_tout1;
    N_Vector CVODES_y, CVODES_abstol;
    int CVODES_flag, CVODES_flagr, CVODES_iout;

    ///////////
    std::vector<double> statePopulations;
    std::vector<double> numberOfIntegrationSteps;
    std::vector<std::vector<double> > timeDependentStatePopulationVectors;
    std::vector<double> timePoints;
    std::vector<std::vector<double> > sensitivityDky;
    ///////////

};

#endif // GENERICSYSTEM_H
