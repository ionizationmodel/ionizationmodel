//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef RELATIVEINTENSITYFITTER_H
#define RELATIVEINTENSITYFITTER_H

#include <relativeprofile.h>
#include <voigtprofile.h>
#ifdef __linux__
//Ubuntu specific eigen include
#include <Eigen/Dense>
#include <eigen3/unsupported/Eigen/NonLinearOptimization>
#include <eigen3/unsupported/Eigen/AutoDiff>
#endif
#ifdef _WIN32 || _WIN64
//Windows specific
#include <Eigen/NonLinearOptimization>
#endif
#include <valuecontainerclass.h>
#include <iostream>
#include <string>
#include <sstream>
#include <memory>



// Generic functor
template<typename _Scalar, int NX=Eigen::Dynamic, int NY=Eigen::Dynamic>
struct relativeFunctor
{
    typedef _Scalar Scalar;
    enum {
        InputsAtCompileTime = NX,
        ValuesAtCompileTime = NY
    };
    typedef Eigen::Matrix<Scalar,InputsAtCompileTime,1> InputType;
    typedef Eigen::Matrix<Scalar,ValuesAtCompileTime,1> ValueType;
    typedef Eigen::Matrix<Scalar,InputsAtCompileTime,ValuesAtCompileTime> JacobianType;

    int m_inputs, m_values;

    relativeFunctor() : m_inputs(InputsAtCompileTime), m_values(ValuesAtCompileTime) {}
    relativeFunctor(int inputs, int values) : m_inputs(inputs), m_values(values) {}

    int inputs() const { return m_inputs; }
    int values() const { return m_values; }

};
struct relativeFitter_functor : relativeFunctor<double>
{
    int numberOfVariables;
    int numberOfDataPoints;
    valueContainerClass *valueContainer;
    hyperfineCalculations *hypFi;
    std::unique_ptr<voigtProfile> AVoigt;

    std::vector<bool> xLimitLocks;                             //Limit locks
    std::vector<double> frequencyRange;
    std::vector<double> measuredProfile;
    std::vector<std::vector<double>> alphasAndBetas;
    std::vector<int> transitionIds;
    std::vector<std::vector<Scalar>> individualOuputs;
    int numberOfTransitions;

    //automatic differentiation
    typedef Eigen::AutoDiffScalar<Eigen::Matrix<Scalar,Eigen::Dynamic,1>> ADS;
    typedef Eigen::Matrix<ADS, Eigen::Dynamic, 1> VectorXad;

    //Eigen::VectorXd<
    //Set:
    void setNumberOfVariables(int numberOfVariable_v){
        numberOfVariables=numberOfVariable_v;
        xLimitLocks.resize(numberOfVariables);
    }
    void setNumberOfDataPoints(int numberOfDataPoints_v){
        numberOfDataPoints=numberOfDataPoints_v;
    }
    void setValueContainer(valueContainerClass *valueContainer_p){
        valueContainer=valueContainer_p;
    }
    void setHyperfineCalculations(hyperfineCalculations *hypFi_p){
        hypFi=hypFi_p;
    }
    void init(){
        AVoigt = std::make_unique<voigtProfile>(valueContainer);
        frequencyRange=valueContainer->getFitFrequencyRange();
        measuredProfile=valueContainer->getFitProfile();
        numberOfTransitions=valueContainer->getNumberOfAllowedTransitions();
        alphasAndBetas=hypFi->getAlphasAndBetas(numberOfTransitions);
        for(int t=0;t<numberOfTransitions;t++){
            transitionIds.push_back(t);
        }
    }

    //Get:
    int getLimited(){
        int counter=0;
        for(int i=0; i<this->inputs(); i++){
            if(xLimitLocks[i]){
                counter++;
            }
        }
        return counter;
    }
    //A functor for the Voigt functions and its derivatives

    relativeFitter_functor(void) : relativeFunctor<double>(numberOfVariables, numberOfDataPoints) {}
    int operator()(Eigen::VectorXd &x, Eigen::VectorXd &fvec)
    {
        std::vector<std::pair<double, double> > xLimits;      //Fit limits
        xLimits=valueContainer->getParameterLimits();

        //Af/Ai and Bf/Bi ratio locks
        bool afAi=valueContainer->getAfAiRatioLock();
        bool bfBi=valueContainer->getBfBiRatioLock();

        if(valueContainer->getEnforceLimits()){

            //Check if the input values are beyond their limits
            for(int i=0; i<this->inputs(); i++){
                double xValue=x[i];
                double lowerLimit=xLimits[i].first;
                double upperLimit=xLimits[i].second;
                if(xValue<=lowerLimit){
                    x[i]=lowerLimit;
                    xLimitLocks[i]=true;
                }
                if(xValue>=upperLimit){
                    x[i]=upperLimit;
                    xLimitLocks[i]=true;
                }
                else{
                    xLimitLocks[i]=false;
                }
            }

            //set Af/Ai and Bf/Bi ratio locks

            if(afAi){
                x[2]=x[0]*valueContainer->getAfAiRatio();
            }
            if(bfBi){
                x[3]=x[1]*valueContainer->getBfBiRatio();
            }
        }
        fvec.setZero();

        AVoigt->getVoigtProfile<Scalar,Eigen::Matrix<Scalar,Eigen::Dynamic,1>, Eigen::Matrix<Scalar,Eigen::Dynamic,1>,std::vector<std::vector<Scalar>>>(x, fvec,
                                                                                                                       individualOuputs,
                                                                                                                       transitionIds,
                                                                                                                       frequencyRange,
                                                                                                                       measuredProfile,
                                                                                                                       alphasAndBetas,
                                                                                                                       true, this->inputs());
        return 0;
    }

    int df(const Eigen::VectorXd &x, Eigen::MatrixXd &fjac)
    {
        //["initialA"]=x[0];
        //["initialB"]=x[1];
        //["finalA"]=x[2];
        //["finalB"]=x[3];
        //["CoG"]=x[4];
        //["gaussianWidth"]=x[5];
        //["lorentzianWidth"]=x[6];
        //["intensityScaler"]=x[7];
        //["backgroundScaler"]=x[8;

        /////////////
        std::vector<bool> xLocks;                             //Fit locks
        xLocks=valueContainer->getParameterLocks();

        //Af/Ai and Bf/Bi ratio locks
        bool afAi=valueContainer->getAfAiRatioLock();
        bool bfBi=valueContainer->getBfBiRatioLock();

        VectorXad inputVector(this->inputs());
        for(int i=0;i<this->inputs();i++){
            inputVector(i)=ADS(x(i), this->inputs(),i);
        }
        VectorXad v(this->values());
        std::vector<std::vector<ADS>> individualOuputs2;
        AVoigt->getVoigtProfile<ADS,Eigen::Matrix<ADS,Eigen::Dynamic,1>, Eigen::Matrix<ADS,Eigen::Dynamic,1>,std::vector<std::vector<ADS>>>(inputVector, v,
                                                                                                                                            individualOuputs2,
                                                                                                                                            transitionIds,
                                                                                                                                            frequencyRange,
                                                                                                                                            measuredProfile,
                                                                                                                                            alphasAndBetas,
                                                                                                                                            true, this->inputs());
        for(int idx=0; idx<this->values(); idx++){
            fjac.row(idx)=v(idx).derivatives();
            //Set derivative zero for a given parameter if it is either locked or limited
            for(int i=0; i<this->inputs(); i++){
                if(xLocks[i] || xLimitLocks[i]){
                    fjac(idx,i)=0;
                }
            }
            //set Af/Ai and Bf/Bi ratio locks
            if(afAi){
                fjac(idx,2)=0;
            }
            if(bfBi){
                fjac(idx,3)=0;
            }
        }
        return 0;
    }
   int inputs() const { return numberOfVariables; }
   int values() const { return numberOfDataPoints; }
};

class relativeIntensityFitter
{
public:
    explicit relativeIntensityFitter(valueContainerClass *valueContainer_p, hyperfineCalculations *hypFi_p)
        :valueContainer(valueContainer_p), hypFi(hypFi_p)    {
    }

    void setFitResults(const Eigen::VectorXd &x_r, const std::vector<std::string> &hashOrder_r, int n_v, int p_v){
        QHash<QString, double> bestFitParametersHash;
        QHash<QString, double> fittedIntensities;
        bestFitParametersHash["initialA"]=x_r[0];
        bestFitParametersHash["initialB"]=x_r[1];
        bestFitParametersHash["finalA"]=x_r[2];
        bestFitParametersHash["finalB"]=x_r[3];
        bestFitParametersHash["CoG"]=x_r[4]/1E9;
        bestFitParametersHash["gaussianWidth"]=x_r[5];
        bestFitParametersHash["lorentzianWidth"]=x_r[6];
        bestFitParametersHash["intensityScaler"]=x_r[7];
        bestFitParametersHash["backgroundScaler"]=x_r[8];
        for(int i=0;i<p_v; i++){
            fittedIntensities[QString::fromStdString(hashOrder_r[i])]=x_r[(n_v-p_v)+i];
        }
        valueContainer->setFittedProfileParameters(bestFitParametersHash);
        valueContainer->setFittedFreeIntensities(fittedIntensities);
    }

    void fit()
    {
        //["initialA"]=x[0];
        //["initialB"]=x[1];
        //["finalA"]=x[2];
        //["finalB"]=x[3];
        //["CoG"]=x[4];
        //["gaussianWidth"]=x[5];
        //["lorentzianWidth"]=x[6];
        //["intensityScaler"]=x[7];
        //["backgroundScaler"]=x[8;

        QStringList parameterNames;
        parameterNames.append("Initial A: ");
        parameterNames.append("Initial B: ");
        parameterNames.append("Final A: ");
        parameterNames.append("Final B: ");
        parameterNames.append("Center of gravity: ");
        parameterNames.append("Gaussian Width: ");
        parameterNames.append("Lorentzian Width: ");
        parameterNames.append("Intensity Scaler: ");
        parameterNames.append("Background Scaler: ");

        QStringList results;
        QString fitterStatus;
        QString chiSquared;
        QString juliaString;
        QString juliaArrayString1;
        QString juliaArrayString2;

        int m=valueContainer->getNumberOfDataPoints();
        int p=valueContainer->getNumberOfAllowedTransitions();
        int s=valueContainer->getNumberOfStaticVariables();
        int n=valueContainer->getNumberOfStaticVariables()+p;

        Eigen::LevenbergMarquardtSpace::Status status;
        Eigen::VectorXd x(n);                                           //Fit parameters
        std::vector<double> xValues(n);                                 //Initial Values

        double fTol=valueContainer->getFtol();
        double xTol=valueContainer->getXtol();

        double chi, reducedChiSquared, reducedChi;
        int dof;

        std::vector<double> relativeIntensities;
        std::vector<std::string> hashOrder;

        //Fixed parameters presents in every fit
        xValues=valueContainer->getParameterValues();
        x[0]=xValues[0];
        x[1]=xValues[1];
        x[2]=xValues[2];
        x[3]=xValues[3];
        x[4]=xValues[4];
        x[5]=xValues[5];
        x[6]=xValues[6];
        x[7]=xValues[7];
        x[8]=xValues[8];
        //Per peak parameters, e.g. for free intensity
        relativeIntensities=valueContainer->getOrderedRelativeTransitionIntensities();
        hashOrder=valueContainer->getHashOrder();
        for(int i=0; i<p; i++){
            parameterNames.append(QString::fromStdString(hashOrder[i]));
            x[(n-p)+i]=relativeIntensities[i];
        }
        //Analytical/autodifferentiate derivative
        relativeFitter_functor functor;
        functor.setValueContainer(valueContainer);
        functor.setHyperfineCalculations(hypFi);
        functor.setNumberOfDataPoints(m);
        functor.setNumberOfVariables(n);
        functor.init();
        Eigen::LevenbergMarquardt<relativeFitter_functor> lm(functor);

        lm.parameters.ftol = fTol;
        lm.parameters.xtol = xTol;
        lm.parameters.maxfev = valueContainer->getMaxIter(); // Max iterations
        status  = lm.minimize(x);
        // check norm and covariance
        chi = lm.fvec.norm();
        //TODO, fix DoF, at the moment all the A and B values are included eveni if, for example, there
        //is now hyperfine structure.
        dof=m-valueContainer->getModel_fitNumberOfLockedParameters()-functor.getLimited();
        reducedChiSquared = chi*chi/(dof);
        reducedChi=sqrt(reducedChiSquared);
        Eigen::internal::covar(lm.fjac, lm.permutation.indices(), fTol);
        //Covariant matrix
        Eigen::MatrixXd cov;
        //Scale Variance-Covariance -matrix with the reduced chi-squared
        cov =  reducedChi*lm.fjac;

        fitterStatus.append("Iterations: ");
        fitterStatus.append(QString::number(lm.iter));
        fitterStatus.append(" Return code: ");
        fitterStatus.append(QString::number(status));

        chiSquared.append("Reduced chi-squared: ");
        chiSquared.append(QString::number(reducedChiSquared));
        chiSquared.append(" DoF: ");
        chiSquared.append(QString::number(dof));

        results.append("Best fit parameters (in MHz where applicable)");
        results.append(fitterStatus);
        results.append(chiSquared);
        juliaString="fitResults("+QString::number(lm.iter)+","+QString::number(status)+","+QString::number(reducedChiSquared)+","+QString::number(dof);
        juliaArrayString1="[";
        juliaArrayString2="[";

        for(int i=0; i<n;i++){
                double value=x[i];
                double error=sqrt(cov(i,i));
                QString parameterOutput;
                parameterOutput.append(parameterNames.at(i));
                parameterOutput.append(QString::number(value,'f', 9));
                parameterOutput.append(" +- ");
                parameterOutput.append(QString::number(error,'f', 9));
                results.append(parameterOutput);
                if(i<s){
                   juliaString+=","+QString::number(value,'f', 9)+","+QString::number(error,'f', 9);
                }
                else{
                    juliaArrayString1+=QString::number(value,'f', 9)+",";
                    juliaArrayString2+=QString::number(error,'f', 9)+",";
                }
        }
        juliaArrayString1.remove(-1,1);
        juliaArrayString2.remove(-1,1);
        juliaArrayString1+="]";
        juliaArrayString2+="]";
        juliaString+=","+juliaArrayString1+","+juliaArrayString2+")";
        results.append(juliaString);
        valueContainer->setFitLog(results);
        setFitResults(x,hashOrder, n, p);
    }


private:
    valueContainerClass *valueContainer;
    hyperfineCalculations *hypFi;
    voigtProfile *AVoigt;
};

#endif // RELATIVEINTENSITYFITTER_H
