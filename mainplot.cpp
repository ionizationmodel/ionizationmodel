//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#include "mainplot.h"

mainPlot::mainPlot(QWidget *parent) :
    QwtPlot(parent)
{
    this->setAxisFont(QwtPlot::xBottom,QFont(QString("Arial"), 10, 0, false));
    this->setCanvasBackground(Qt::white);
    this->autoReplot();
    this->enableAxis(this->yRight);
    this->setParent(parent);

    //grid
    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->setPen(QPen(Qt::gray, 0.0, Qt::DotLine));
    grid->enableX(true);
    grid->enableXMin(true);
    grid->enableY(true);
    grid->enableYMin(false);
    grid->attach(this);

    //zoom
    QwtPlotMagnifier *zoom = new QwtPlotMagnifier(this->canvas());
    zoom->setAxisEnabled(0,true);
    zoom->setMouseButton(Qt::MidButton);

    //Pan
    QwtPlotPanner *pan=new QwtPlotPanner(this->canvas());
    pan->setMouseButton(Qt::LeftButton);

    QwtPlotPicker *picker=new QwtPlotPicker(QwtPlot::xBottom, QwtPlot::yRight,
                                            QwtPlotPicker::CrossRubberBand, QwtPicker::AlwaysOn,
                                            this->canvas());
    picker->setStateMachine(new QwtPickerTrackerMachine());
    picker->setRubberBand(QwtPicker::CrossRubberBand);
    //Plot curves
    calculatedProfile=new QwtPlotCurve();
    calculatedProfile->setRenderHint(QwtPlotItem::RenderAntialiased);
    calculatedProfile->attach(this);

    experimentalspectrum = new QwtPlotCurve();
    experimentalspectrum->setRenderHint(QwtPlotItem::RenderAntialiased);
    experimentalspectrum->setAxes(QwtPlot::xBottom,QwtPlot::yLeft);
    experimentalspectrum->attach(this);
    experimentalspectrum->setPen(QPen(Qt::magenta, 1));

    rateEquationsProfile= new QwtPlotCurve();
    rateEquationsProfile->setRenderHint(QwtPlotItem::RenderAntialiased);
    rateEquationsProfile->setPen(QPen(Qt::blue, 1));
    rateEquationsProfile->setAxes(QwtPlot::xBottom,QwtPlot::yLeft);
    rateEquationsProfile->attach(this);

    diagnosticsBarPlot=new QwtPlotMultiBarChart();
    diagnosticsBarPlot->setLayoutPolicy( QwtPlotMultiBarChart::AutoAdjustSamples );
    diagnosticsBarPlot->setSpacing( 0);
    diagnosticsBarPlot->setMargin( 0 );
    diagnosticsBarPlot->setAxes(QwtPlot::xBottom,QwtPlot::yLeft);
    diagnosticsBarPlot->attach( this );
    //single peaks
    singleRelativePeak=nullptr;
    singleRelativePeaks.append(singleRelativePeak);
    transitionLabel=nullptr;
    transitionLabels.append(transitionLabel);

}

mainPlot::~mainPlot(){
}

void mainPlot::setCalculatedProfile(QVector<double> calcXData_v, QVector<double> calcYData_v){
    calcXData=convertToPHz(calcXData_v);
    calcYData=calcYData_v;
    calculatedProfile->setSamples(calcXData, calcYData);

}


void mainPlot::setExperimentalProfile(QVector<double> expXData_v, QVector<double> expYData_v){
    expXData=convertToPHz(expXData_v);
    expYData=expYData_v;
    experimentalspectrum->setSamples(expXData, expYData);
}

void mainPlot::setCalculatedSinglePeaks(QHash<QString, QPair<QVector<double>, QVector<double> > > singleRelativePeakPairs_v){
    int transCounter=0;
    if(singleRelativePeaks.size()!=0){
        foreach(QwtPlotCurve *single, singleRelativePeaks ){
            if(single!=nullptr){
                single->detach();
                delete single;
                single=nullptr;
            }
        }
        singleRelativePeaks.clear();
    }
    if(transitionLabels.size()!=0){
        foreach(QwtPlotMarker *marker, transitionLabels  ){
            if(marker!=nullptr){
                marker->detach();
                delete marker;
                marker=nullptr;
            }
        }
        transitionLabels.clear();
    }
    //Curves
    calcSingleRelativePeakPairs=singleRelativePeakPairs_v;
    foreach(QString key, calcSingleRelativePeakPairs.keys()){
        QVector<double> singlePeakYData=calcSingleRelativePeakPairs[key].second;
        QVector<double> singlePeakXData=convertToPHz(calcSingleRelativePeakPairs[key].first);
        singleRelativePeak=new QwtPlotCurve();
        singleRelativePeak->setSamples(singlePeakXData, singlePeakYData);
        singleRelativePeak->setRenderHint(QwtPlotItem::RenderAntialiased);
        singleRelativePeak->setPen(QPen(Qt::red, 1, Qt::DashDotLine));
        singleRelativePeak->attach(this);
        singleRelativePeaks.append(singleRelativePeak);
        singleRelativePeak=nullptr;
        transCounter++;
        //Set the transition markers.
        QVector<double>::iterator max = std::max_element(singlePeakYData.begin(), singlePeakYData.end());
        int indexOfMax=singlePeakYData.indexOf(*max);
        double transitionFrequency=singlePeakXData.value(indexOfMax);
        transitionLabel=new QwtPlotMarker();
        QwtText transLabel;
        transLabel.setBackgroundBrush(Qt::white);
        transLabel.setFont(QFont(QString("Arial"), 11, 0, false));
        transLabel.setBorderPen(QPen(Qt::black));
        transLabel.setText(key);
        transitionLabel->setLabel(transLabel);
        transitionLabel->setValue(transitionFrequency,*max+*max/25);
        transitionLabel->attach(this);
        transitionLabels.append(transitionLabel);
        transitionLabel=nullptr;


    }
}


void mainPlot::setRateEquationsProfile(QVector<double> rateXData_v, QVector<double> rateYData_v){
    calcXData=convertToPHz(rateXData_v);
    rateYData=rateYData_v;
    rateEquationsProfile->setSamples(calcXData, rateYData);
}

void mainPlot::setDiagnosticsPlot(QVector<double> diagXData_v, QVector<double> diagYData_v, bool isBarPlot){
    if(isBarPlot){
        QVector<QVector<double> > newBarChartData;
        foreach (const double value, diagYData_v) {
            QVector<double> point;
            point.append(value);
            newBarChartData.append(point);
        }
        diagnosticsBarPlot->setSamples(newBarChartData);
    }
}


void mainPlot::resetView(){
    this->setAxisAutoScale(QwtPlot::xBottom);
    this->setAxisAutoScale(QwtPlot::yLeft);
    this->setAxisAutoScale(QwtPlot::yRight);
    this->replot();
}

////////
///SLOTS
////////


void mainPlot::setFullRelativeProfilePair(QPair<QVector<double>, QVector<double> > newFullRelativeProfilePair_v){
    setCalculatedProfile(newFullRelativeProfilePair_v.first, newFullRelativeProfilePair_v.second);
    replot();
}

void mainPlot::setRateEquationsProfilePair(QPair<QVector<double>, QVector<double> > newRateEquationsProfilePair_v){
    setRateEquationsProfile(newRateEquationsProfilePair_v.first, newRateEquationsProfilePair_v.second);
    replot();
}

void mainPlot::setSingleRelativePeakPairs(QHash<QString,QPair<QVector<double>,QVector<double> > > newSingleRelativePeakPairs_v){
    setCalculatedSinglePeaks(newSingleRelativePeakPairs_v);
    replot();
}

void mainPlot::setMeasuredProfilePair(QPair<QVector<double>, QVector<double> > newMeasuredProfilePair_v){
    setExperimentalProfile(newMeasuredProfilePair_v.first, newMeasuredProfilePair_v.second);
    replot();
}

void mainPlot::setNumberOfIntegrationStepsPlot(QPair<QVector<double>, QVector<double> > newIntegrationStepsPerFrequencyPointProfile_v){
    setDiagnosticsPlot(newIntegrationStepsPerFrequencyPointProfile_v.first, newIntegrationStepsPerFrequencyPointProfile_v.second, true);
    replot();
}

void mainPlot::hideCalculatedSpectra(bool hide){
    if(!hide){
        QVector<double> zero;
        zero.clear();
        setCalculatedProfile(zero, zero);
    }
    else{
        setCalculatedProfile(calcXData, calcYData);
    }

    replot();
}

void mainPlot::savePlot(){
    //Some tricks to make the image look nicer
    calculatedProfile->setPen(QPen(Qt::black, 5));
    foreach(QwtPlotCurve *single,singleRelativePeaks ){
        single->setPen(QPen(Qt::red, 5, Qt::DashDotLine));
    }
    foreach (QwtPlotMarker *transLabel, transitionLabels) {
        transLabel->setLinePen(QPen(Qt::red, 5, Qt::DashDotLine));
        QwtText lab=transLabel->label();
        lab.setFont(QFont(QString("Arial"), 25, 0, false));
        transLabel->setLabel(lab);

    }

    //image properties
    QString pathforPlot=QDir::homePath();
    QString fileName=pathforPlot+"/output.pdf";
    QPrinter printer( QPrinter::HighResolution );
    printer.setOutputFormat( QPrinter::PdfFormat );
    printer.setOutputFileName(fileName);
    qDebug()<<fileName;
    QwtPlotRenderer renderer;
    renderer.renderDocument(this, fileName, QSizeF(300, 200), 85);
    //and revert the changes
    calculatedProfile->setPen(QPen(Qt::black, 1));
    foreach(QwtPlotCurve *single,singleRelativePeaks ){
        single->setPen(QPen(Qt::red, 1, Qt::DashDotLine));
    }
    foreach (QwtPlotMarker *transLabel, transitionLabels) {
        transLabel->setLinePen(QPen(Qt::red, 1, Qt::DashDotLine));
        QwtText lab=transLabel->label();
        lab.setFont(QFont(QString("Arial"), 11, 0, false));
        transLabel->setLabel(lab);

    }

}
