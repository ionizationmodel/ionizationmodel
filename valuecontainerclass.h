//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef VALUECONTAINERCLASS_H
#define VALUECONTAINERCLASS_H

#include <QObject>
#include <QHash>
#include <QStringList>
#include <QPair>
#include <QHash>
#include <QVector>
#include <QDebug>
#include <iostream>
#include <unordered_map>

class valueContainerClass : public QObject
{
    Q_OBJECT
public:
    explicit valueContainerClass(QObject *parent = 0);

    /////
    //Get
    /////
    //UI Values
    double getInitialA();
    std::pair<double, double> getInitialALimits();
    double getInitialB();
    std::pair<double, double> getInitialBLimits();
    double getFinalA();
    std::pair<double, double> getFinalALimits();
    double getFinalB();
    std::pair<double, double> getFinalBLimits();
    double getCoG();
    std::pair<double, double> getCoGLimits();

    double getInitialJ();
    double getfinalJ();
    double getNucSpin();

    double getACoefficient_1();
    std::pair<double, double> getACoefficienLimits();
    double getModel_laserIntensity();
    std::pair<double, double> getModel_laserIntensityLimits();
    double getGaussianWidth();
    std::pair<double, double> getGaussianWidthLimits();
    double getLorentzianWidth();
    std::pair<double, double>  getLorentzianWidthLimits();
    double getLaserPulseWidth();

    double getIntensityScaler();
    std::pair<double, double> getIntensityScalerLimits();
    double getBackgroundScaler();
    std::pair<double, double> getBackgroundScalerLimits();

    double getAfAiRatio();
    double getBfBiRatio();

    double getVoigtFWHM();
    double getFanoParameter();
    double getAsymmetryParameter();

    //Get All Ui Values
    QHash<QString, double> getUiValues();

    //Fitter locks
    bool getInitialALock();
    bool getInitialBLock();
    bool getFinalALock();
    bool getFinalBLock();
    bool getCoGLock();

    bool getGaussianWidthLock();
    bool getLorentzianWidthLock();
    bool getACoefficientLock();
    bool getModel_laserIntensityLock();

    bool getIntensityScalerLock();
    bool getBackgroundScalerLock();

    bool getAfAiRatioLock();
    bool getBfBiRatioLock();

    bool getModel_backgroundScalerLock();
    bool getModel_intensityScalerLock();

    //Get All UI Locks
    QHash<QString, bool> getLocks();

    //Get Profile Data
    QPair<QVector<double>, QVector<double> > getMeasuredProfile();
    std::vector<double> getMeasuredProfileRange();
    std::vector<double> getMeasuredProfileValues();
    QPair<QVector<double>, QVector<double> > getFullRelativeProfile();
    QVector<QVector<double> > getFullRelativeProfileDerivatives();
    QHash<QString, QPair<QVector<double>, QVector<double> > > getSingleRelativePeaks();
    QPair<QVector<double>, QVector<double> > getRateEquationProfile();
    QPair<QVector<double>, QVector<double> > getResidual(QString profile_s="relative");
    QVector<double> getFrequencyRange();
    std::vector<double> getFrequencyRangeStd();
    double getFrequencyAtIndex(int i);

    //Get model diagnostics data
    QPair<QVector<double>, QVector<double> > getIntegrationStepsPerFrequencyPoint();

    //Get misc
    bool getSolveRateEquationsState();
    bool getShowIndividualPeaksState();
    int getStepperChoice();

    //Get fitter controls
    bool getEnforceLimits();
    bool getFreeIntensities();
    double getFtol();
    double getXtol();
    double getMaxIter();

    ////////
    //FITTER
    ////////
    //Common
    int getNumberOfDataPoints();
    std::vector<double> getFitProfile();
    std::vector<double> getFitFrequencyRange();
    std::vector<std::string> getHashOrder();
    std::vector<double> getOrderedRelativeTransitionIntensities();
    std::vector<double> getOrderedSpontaneousDecayRates_1();
    QHash<QString, double> getFittedFreeIntensities();
    //relativeProfile
    //Set functions for the fitter, this does not emit signals
    void setFittedProfileParameters(QHash<QString, double> fitVariablesHash_v);
    //Manually emit the valuehChanged signal
    void emitValuesChanged();  
    //Get various values required by the fitter
    int getNumberOfStaticVariables();
    std::vector<double> getParameterValues();
    std::vector<bool> getParameterLocks();
    std::vector<std::pair<double, double> > getParameterLimits();
    int getfitNumberOfLockedParameters();
    //Rate-equations profile
    void setModel_fittedProfileParameters(QHash<QString, double> model_fitVariablesHash_v);
    void emitModel_valuesChanged();
    int getModel_numberOfStaticVariables();
    std::vector<double> getModel_parameterValues();
    std::vector<bool> getModel_parameterLocks();
    std::vector<std::pair<double, double> > getModel_parameterLimits();
    int getModel_fitNumberOfLockedParameters();

    //////////////////////////
    //Get HyperFine parameters
    //////////////////////////
    std::unordered_map<std::string, double>getRelativeTransitionIntensities();
    std::unordered_map<std::string, double> getRelativeTransitionFrequencies();
    QHash<QString, int> getListOfInitialStateIds();
    QHash<QString, int> getListOfFinalStateIds();
    QStringList getListOfAllowedTransitions();
    int getNumberOfAllowedTransitions();
    int getNumberOfInitialFStates();
    int getNumberOfFinalFStates();

    ///////////////////////////////////
    //Get ionizing transition properties
    //////////////////////////////////
    double getWl_nr();                                              //[m]
    double getPW_nr();                                              //[s]
    double getI_nr();                                               //[muJ/cm^s]
    double getCS_nr();                                              //[cm^2]
    double getGW_nr();                                              //[MHz]
    double getLW_nr();                                              //[MHz]
    double getTimeOffset();                                         //[ns]
    bool getIsNonResonant();

    ///////////////
    //Get Constants
    ///////////////
    static constexpr double c= 2.99792458E8;                    //[m/s]
    static constexpr double pi = 3.14159265358979323846;
    static constexpr double hBar=6.6260755e-34/(2*pi);          //[Js]
    static constexpr double ln2=0.6931471805599453;
    static constexpr double k=1.3806488e-23;                    //[J/K]

    ////////////////////
    //Get SUNDIALS parameters
    ////////////////////
    double getSundials_integrationTimeScaler();
    double getSundials_absoluteTolerance();
    double getSundials_relativeTolerance();
    double getSundials_nlConvCoeff();
    int getSundials_maximumNumberOfIntegrationStep();
    bool getSundials_useJacobian();
    bool getSundials_useConstraints();
    int getSundials_TimePoints();

    ///////////////////////////
    //Get model control parameters
    ///////////////////////////
    bool getModel_spontaneousEmissionOn();
    double getModel_initialPopulationTemperature();
    double getModel_initialPopulationScaler();
    double getModel_backgroundScaler();
    std::pair<double, double> getModel_backgroundScalerLimits();
    double getModel_intensityScaler();
    std::pair<double, double> getModel_intensityScalerLimits();

    /////////////////////////////////
    //Get plotting related variables
    /////////////////////////////////
    int getSundials_StateToBePlotted();
    int getSundials_TimeOrFrequencyPointIndexToBePlotted();
    bool getSundials_PlotAsAFunctionOfTime();
    int getSundials_maximumTimeOrFrequencyIndex();
    int getSundials_CurrentTimeOrFrequencyIndex();
    double getSundials_CurrentTimeOrFrequencyValue();

private:
    //UI Values
    double initialA;                                //Initial F state A -factor and limits
    std::pair<double, double> initialALimits;
    double initialB;                                //Initial F state B -factor and limits
    std::pair<double, double> initialBLimits;
    double finalA;                                  //Final F state A -factor and limits
    std::pair<double, double> finalALimits;
    double finalB;                                  //Final F state B -factor and limits
    std::pair<double, double> finalBLimits;
    double CoG;                                     //Center of Gravity and limits
    std::pair<double, double> CoGLimits;

    double initialJ;                                //Spin of the initial J-state
    double finalJ;                                  //Spin of the final J-state
    double nucSpin;                                 //Nuclear spin

    double aCoefficient_1;                               //Sponatenous transition rate (Einsteins A coefficient)
    std::pair<double, double> aCoefficientLimits;
    double model_laserIntensity;                          //Laser intensity
    std::pair<double, double> model_laserIntensityLimits;
    double gaussianWidth;                           //Laser linewidth (Gaussian) and limits
    std::pair<double, double>  gaussianWidthLimits;
    double lorentzianWidth;                         //Additional broadening component (Lorentzian) and limits
    std::pair<double, double>  lorentzianWidthLimits;
    double laserPulseWidth;                         //The width of the laser pulse in ns

    double intensityScaler;                         //Scaling factor for the laser intensity and limits
    std::pair<double, double> intensityScalerLimits;
    double backgroundScaler;                        //Scaling factor for a background and limits
    std::pair<double, double> backgroundScalerLimits;
    double afAiRatio;                               //Ratio of the Af and Ai -values
    double bfBiRatio;                               //Ratio of the Bf and Bi -values

    double voigtFWHM;                               //Voigt FWHM
    double fanoParameter;                           //Fano parameter to bu used when dealing with A.I -states
    double asymmetryParameter;                      //Parameter to estimate asymmteric Lorentzian profiles

    QHash<QString, double> uIValuesHash;
    QHash<QString, double> fitVariablesHash;
    QHash<QString, double> model_fitVariablesHash;


    //Fitter locks
    bool initialALock;
    bool initialBLock;
    bool finalALock;
    bool finalBLock;
    bool CoGLock;

    bool gaussianWidthLock;
    bool lorentzianWidthLock;
    bool aCoefficientLock;
    bool model_laserIntensityLock;

    bool intensityScalerLock;
    bool backgroundScalerLock;

    bool afAiRatioLock;
    bool bfBiRatioLock;

    bool model_backgroundScalerLock;
    bool model_intensityScalerLock;

    QHash<QString, bool> locks;

    ///////////////
    //Profile data
    ///////////////
    QPair<QVector<double>, QVector<double> > measuredProfile;
    // QPair and Vectors storing the profile calculated using Relative intensities.
    QPair<QVector<double>, QVector<double> > fullRelativeProfile;
    //"initialA"
    //"initialB"
    //"finalA"
    //"finalB"
    //"CoG"
    //"gaussianWidth"
    //"lorentzianWidth"
    //"intensityScaler"
    //"backgroundScaler"
    //Per peak parameters, e.g. for free intensity
    //QHash storing the single peaks which form the full relative profile.
    QHash<QString, QPair<QVector<double>, QVector<double> > > singleRelativePeaks;
    // QPairs and Vectors storing the profile calculated using the rate equations.
    QPair<QVector<double>, QVector<double> > rateEquationProfile;

    ////////////////////////
    //Model diagnostics data
    ////////////////////////
    QPair<QVector<double>, QVector<double> > intergationStepsPerFrequencyPointProfile;

    ////////////////
    //Misc. UI parameters
    ////////////////
    bool showIndividualPeaks;

    //////////////////
    //Fitter controls
    //////////////////
    bool enforceLimits;
    bool freeIntensities;
    double ftol;    //Sets the tolerance for the norm of the vector function.
    double xtol;    //Sets the tolerance for the norm of the gradient of the error vector.
    double maxIter; //Maximum number of iterations

    ///////////////
    //Fitter output
    ///////////////
    QStringList fitResults;
    QHash<QString, double> fittedFreeIntensities;

    ///////////////
    //Fitter input
    ///////////////
    std::vector<std::string> hashOrder;

    ///////////////////////////////////////////////////////////////
    //Calculate Voigt FMWM, and Af/Ai and Bi/Bf from the given ratio
    ///////////////////////////////////////////////////////////////
    void calculateVoigtFWHM();
    double calculateAf();
    double calculateBf();

    ///////////////////////
    //HYPERFINE PARAMETERS
    //////////////////////
    //std::unordered_map<std::string, double>  where double is the frequency and QString
    //the transition identifier "x[n]->x[m]".
    std::unordered_map<std::string, double> relativeTransitionIntensities;
    std::unordered_map<std::string, double> relativeTransitionFrequencies;
    QHash<QString, int> listOfInitialStateIds;
    QHash<QString, int> listOfFinalStateIds;
    QStringList listOfAllowedTransitions;

    //Ionizing transitions parameters;
    double wL_nr;                                               //[nm]
    double pW_nr;                                               //[ns]
    double i_nr;                                                //[muJ/cm^s]
    double cS_nr;                                               //[m^2]
    double gW_nr;                                               //[MHz]
    double lW_nr;                                               //[MHz]
    double ionizingTransitionTimeOffset;                        //[ns]
    bool isNonResonant;

    //Sundials parameters
    double sundials_integrationTimeScaler;
    double sundials_absoluteTolerance;
    double sundials_relativeTolerance;
    double sundials_nlConvCoeff;
    int sundials_maximumNumberOfIntegrationSteps;
    bool sundials_useJacobian;
    bool sundials_useConstraints;
    int sundials_timePoints;

    //Model control parameters(Some can be used as fit parameters)
    bool model_spontaneousEmissionOn;
    double model_initialPopulationTemperature;                         //[K]
    double model_initialPopulationScaler;
    double model_backgroundScaler;
    std::pair<double, double> model_backgroundScalerLimits;
    double model_intensityScaler;
    std::pair<double, double> model_intensityScalerLimits;


    //Plotting related variables
    int sundials_stateToBePlotted;
    int sundials_timeOrFrequencyPointToBePlotted;
    bool sundials_plotAsAFunctionOfTime;
    int sundials_maximumTimeOrFrequencyIndex;
    int sundials_CurrentTimeOrFrequencyIndex;
    double sundials_CurrentTimeOrFrequencyValue;

signals:
    //UI Values
    void initialAValueChanged(double newInitialA_v);
    void initialBValueChanged(double newInitialB_v);
    void finalAValueChanged(double newFinalA_v);
    void finalBValueChanged(double newFinalB_v);
    void CoGValueChanged(double newCoG_v);

    void initialJValueChanged(double newInitialJ_v);
    void finalJValueChanged(double newFinalJ_v);
    void nucSpinValueChanged(double newNucSpin_v);

    void transRateValueChanged(double newTransRate_v);
    void laserIntensityValueChanged(double newLaserIntensity_v);
    void gaussianWidthValueChanged(double newGaussianWidth_v);
    void lorentzianWidthValueChanged(double newLorentzianWidth_v);
    void laserPulseWidthValueChanged(double newLaserPulseWidth_v);

    void intensityScalerValueChanged(double newIntensityScaler_v);
    void backgroundScalerValueChanged(double newBackgroundScaler_v);

    void model_intensityScalerValueChanged(double newModel_IntensityScaler_v);
    void model_backgroundScalerValueChanged(double newModel_BackgroundScaler_v);
    void model_laserIntensityValueChanged(double newModel_laserIntensity_v);

    void voigtFWHMValueChanged(double newVoigtFWHM_v);

    void afAiRatioValueChanged(double newAfAiRatio_v);
    void bfBiRatioValueChanged(double newBfBiRatio_v);

    //Fitter locks
    void initialALockStateChanged(bool newInitialALockState_b);
    void initialBLockStateChanged(bool newInitialBLockState_b);
    void finalALockStateChanged(bool newFinalALockState_b);
    void finalBLockStateChanged(bool newFinalBLockState_b);
    void CoGLockStateChanged(bool newCoGLockState_b);

    void gaussianWidthLockStateChanged(bool newGaussianWidthLockState_b);
    void lorentzianWidthLockStateChanged(bool newLorentzianWidthLockState_b);

    void intensityScalerLockStateChanged(bool newIntensityScalerLockState_b);
    void backgroundScalerLockStateChanged(bool newBackgroundScalerLockState_b);

    void model_intensityScalerLockStateChanged(bool newModel_intensityScalerLockState_b);
    void model_backgroundScalerLockStateChanged(bool newModel_backgroundScalerLockState_b);
    void model_laserIntensityLockStateChanged(bool newModel_laserIntensityLockState_b);

    void afAiRatioLockStateChanged(bool newAfAiRatioLockLockState_b);
    void bfBiRatioLockStateChanged(bool newBfBiRatioLockLockState_b);

    //Log
    void logValueChanged(QString line);


    //Ready to calculate
    void readyToCalculate();

    //Profile data changed
    void measuredProfileChanged(QPair<QVector<double>, QVector<double> > newMeasuredProfile_v);
    void relativeProfileChanged(QPair<QVector<double>, QVector<double> > newFullRelativeProfilePair_v);
    void singleRelativePeaksChanged(QHash<QString, QPair<QVector<double>, QVector<double> > > newSingleRelativePeaks_v);
    void rateEquationPofileChanged(QPair<QVector<double>, QVector<double> > newRateEquationPofilelePair_v);

    //Model diagnostics data changed
    void intergationStepsPerFrequencyPointChanged(QPair<QVector<double>, QVector<double> > newIntergationStepsPerFrequencyPointProfile_v);

    //Misc parameters changed
    void showIndividualPeaksChanged(bool newShowIndividualPeaks_b);

    /////////////////////////////
    //HyperFine parameter signals
    /////////////////////////////
    void relativeTransitionIntensitiesChanged(QHash<QString, double> newRelativeTransitionIntensities_v); //THese may change for example the fitting is peformed with free intensities

    ////////////////////////////////////
    //Plotting related variables signals
    ////////////////////////////////////
    void sundials_maximumTimeOrFrequencyIndexChanged(double newSundials_maximumTimeOrFrequencyIndex_v);
    void sundials_CurrentTimeOrFrequencyIndexChanged(double newSundials_CurrentTimeOrFrequencyIndex_v);
    void sundials_CurrentTimeOrFrequencyValueChanged(double newSundials_CurrentTimeOrFrequencyValue_v);

public slots:
    /////
    //Set
    /////

    ///////////
    //UI Values
    ///////////
    void setInitialA(double newInitialA_v);
    void setInitialALimits(std::pair<double, double> newInitialALimits_v);
    void setInitialB(double newInitialB_v);
    void setInitialBLimits(std::pair<double, double> newInitialBLimits_v);
    void setFinalA(double newFinalA_v);
    void setFinalALimits(std::pair<double, double> newFinalALimts_v);
    void setFinalB(double newFinalB_v);
    void setFinalBLimits(std::pair<double, double> newFinalBLimits_v);
    void setCoG(double newCoG_v);
    void setCoGLimits(std::pair<double, double> newCoGLimits_v);

    void setInitialJ(double newInitialJ_v);
    void setFinalJ(double newFinalJ_v);
    void setNucSpin(double newNucSpin_v);

    void setACoefficient_1(double newACoefficient_v);
    void setACoefficienLimits(std::pair<double, double> newACoefficienLimits_v);
    void setModel_laserIntensity(double newModel_laserIntensity_v);
    void setModel_laserIntensityLimits(std::pair<double, double>  newModel_laserIntensityLimits_v);
    void setGaussianWidth(double newGaussianWidth_v);
    void setGaussianWidthLimits(std::pair<double, double> newGaussianWidthLimits_v);
    void setLorentzianWidth(double newLorentzianWidth_v);
    void setLorentzianWidthLimits(std::pair<double, double> newLorentzianWidthLimits_v);
    void setLaserPulseWidth(double newLaserPulseWidth_v);

    void setFanoParameter(double newFanoParameter_v);
    void setAsymmetryParameter(double newAsymmetryParameter_v);

    void setIntensityScaler(double newIntensityScaler_v);
    void setIntensityScalerLimits(std::pair<double, double> newIntensityScalerLimits_v);
    void setBackgroundScaler(double newBackgroundScaler_v);
    void setBackgroundScalerLimits(std::pair<double, double> newBackgroundScalerLimits_v);

    void setAfAiRatio(double newAfAiRatio_v);
    void setBfBiRatio(double newBfBiRatio_v);

    //////////////
    //Set profiles
    //////////////
    void setMeasuredProfile(QPair<QVector<double>, QVector<double> > newMeasuredProfile_v);
    void setFullRelativeProfile(std::pair<std::vector<double>, std::vector<double>> newFullRelativeProfilePair_v);
    void setSingleRelativePeaks(std::unordered_map<std::string, std::pair<std::vector<double>, std::vector<double> > > newSingleRelativePeaks_v);
    void setRateEquationProfile(QPair<QVector<double>, QVector<double> > newRateEquationProfile_v);

    //Set model diagnostics data
    void setIntegrationStepsPerFrequencyPoint(QPair<QVector<double>, QVector<double> > newIntergationStepsPerFrequencyPoint_v);

    /////////
    //Set Log
    /////////
    void setLog(std::unordered_map<std::string, std::string> log_v);
    void setFitLog(QStringList log_v);
    //////////
    //Set misc
    //////////
    void setShowIndividualPeaks(bool newShowIndividualPeaksLockState_b);

    /////////////////////
    //Set fitter controls
    /////////////////////
    void setEnforceLimits(bool newEnforceLimitsState_b);
    void setFreeIntensities(bool newFreeIntensitiesState_b);
    void setFtol(double newFtol_v);
    void setXtol(double newXtol_v);
    void setMaxIter(double newMaxIter_v);

    /////////////////////
    //Set fitter input
    /////////////////////
    void setHashOrder(std::vector<std::string> newFitHashOrder_v);

    /////////////////////
    //Set fitter output
    /////////////////////
    void setFittedFreeIntensities(QHash<QString, double> newFittedFreeIntensities_v);

    //////////////
    //Fitter locks
    //////////////
    void setInitialALock(bool newInitialALockState_b);
    void setInitialBLock(bool newInitialBLockState_b);
    void setFinalALock(bool newFinalALockState_b);
    void setFinalBLock(bool newFinalBLockState_b);
    void setCoGLock(bool newCoGLockState_b);

    void setGaussianWidthLock(bool newGaussianWidthLockState_b);
    void setLorentzianWidthLock(bool newLorentzianWidthLockState_b);
    void setACoefficientLock(bool newACoefficientLock_b);
    void setModel_laserIntensityLock(bool newModel_laserIntensityLock_b);

    void setIntensityScalerLock(bool newIntensityScalerLockState_b);
    void setBackgroundScalerLock(bool newBackgroundScalerLockState_b);

    void setAfAiRatioLock(bool newAfAiRatioLockLockState_b);
    void setBfBiRatioLock(bool newBfBiRatioLockLockState_b);

    void setModel_backgroundScalerLock(bool newModel_backgroundScalerLock_b);
    void setModel_intensityScalerLock(bool newModel_intensityScalerLock_b);

    //////////////////////////
    //Set HyperFine parameters
    //////////////////////////
    void setRelativeTransitionIntensities(std::unordered_map<std::string, double> newRelativeTransitionIntensities_v);
    void setRelativeTransitionFrequencies(std::unordered_map<std::string, double> newRelativeTransitionFrequencies_v);
    void setListOfInitialStateIds(QHash<QString, int> newListOfInitialStateIds_v);
    void setListOfFinalStateIds(QHash<QString, int> newListOfFinalStateIds_v);
    void setListOfAllowedTransitions(QStringList newListOfAllowedTransitions_v);

    ///////////////////////////////////
    //Set ionizing transition properties
    //////////////////////////////////
    void setWl_nr(double newWl_nr);                                               //[m]
    void setPW_nr(double newPW_nr);                                               //[s]
    void setI_nr(double newI_nr);                                                 //[muJ/cm^s]
    void setCS_nr(double newCS_nr);                                               //[cm^2]
    void setGW_nr(double newGW_nr);                                               //[MHz]
    void setLW_nr(double newlW_nr);                                               //[MHz]
    void setTimeOffset(double newTimeOffset);                                      //[ns]
    void setIsNonResonant(bool newIsNonResonant);

    ////////////////////
    //SET Sundials parameters
    ////////////////////
    void setSundials_integrationTimeScaler(double newSundials_integrationTimeScaler_v);
    void setSundials_absoluteTolerance(double newSundials_absoluteTolerance_v);
    void setSundials_relativeTolerance(double newSundials_relativeTolerance_v);
    void setSundials_nlConvCoeff(double newSundials_nlConvCoeff_v);
    void setSundials_maximumNumberOfIntegrationStep(double newSundials_maximumNumberOfIntegrationStep_v);
    void setSundials_userJacobian(bool newSundials_useJacobian_b);
    void setSundials_useConstraints(bool newSundials_useConstraints_b);
    void setSundials_TimePoints(double newSundials_timePoints_v);

    ///////////////////////////
    //Set model parameters
    ///////////////////////////
    void setModel_spontanousEmissionOn(bool newModel_spontaneousEmissionOn_b);
    void setModel_initialPopulationScaler(double newModel_initialPopulationScaler_v);
    void setModelInitial_populationTemperature(double newModel_initialPopulationTemperature_v);
    double setModel_backgroundScaler(double newModel_backgroudScaler_v);
    void setModel_backgroundScalerLimits(std::pair<double, double> newModel_backgroundScalerLimits_v);
    double setModel_intensityScaler(double newModel_intensityScaler_v);
    void setModel_intensityScalerLimits(std::pair<double, double> newModel_intensityScalerLimits_v);


    /////////////////////////////////
    //Set plotting related variables
    /////////////////////////////////
    void setSundials_StateToBePlotted(int newSundials_StateToBePlotted_v);
    void setSundials_TimeOrFrequencyPointToBePlotted(double newSundials_TimePointToBePlotted_v);
    void setSundials_PlotAsAFunctionOfTime(bool newSundials_PlotAsAFunctionOfTime_b);
    void setSundials_maximumTimeOrFrequencyIndex(int newSundials_maximumTimeOrFrequencyIndex_v);
    void setSundias_CurrentTimeOrFrequencyIndex(double newSundials_CurrentTimeOrFrequencyIndex_v);
    void setSundials_CurrentTimeOrFrequencyValue(double newSundials_CurrentTimeOrFrequencyValue_v);
};

#endif // VALUECONTAINERCLASS_H
