//Author Mikael Reponen (mikael.reponen@mykolab.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#include "ionizationmodel.h"
#include "ui_ionizationmodel.h"
#include <qwt_counter.h>

ionizationModel::ionizationModel(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ionizationModel)
{
    ui->setupUi(this);
    // UI Values Container
    valueContainer = new valueContainerClass(this);
    //Class instance for calculating the hyperfine parameters
    hyperFine = new hyperfineCalculations(this, valueContainer);
    //Relative Fitter classe
    relativeFitter=std::make_shared<relativeIntensityFitter>(valueContainer, hyperFine);
    //Relative profile
    relProfile= new relativeProfile(this, hyperFine, valueContainer, relativeFitter);
    //Class instance for calculating hyperfine profiles
    rateqProfile =new rateEquationsProfile(this, valueContainer, hyperFine, relProfile);
    //Rate-equations Fitter
    rateFitter=std::make_shared<rateEquationsFitter>(valueContainer, hyperFine, rateqProfile);
    //Setup the plotwidget
    hfsPlot=new mainPlot(this);
    hfsPlot->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    ui->mainPlotFulLo->addWidget(hfsPlot);
    //setup the diagnostics plot widget
    diagnosticsPlot=new mainPlot(this);
    diagnosticsPlot->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    ui->diagnosticsPlotLo->addWidget(diagnosticsPlot);
    //Setup default session
    ui->log->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
    //OpenSpectrum dialog
    //Fitter test mode
    bool testMode=false;
    if(testMode){
        openSpec=new openSpectrumDialog(this,valueContainer, testMode);
    }
    else{
        openSpec=new openSpectrumDialog(this,valueContainer, testMode);
    }
    //Set-up default values for UI
    //Remove the second transition tab for now
    ui->laserParameterTab->removeTab(2);

    ui->initialAC->setValue(valueContainer->getInitialA());                             //MHz
    ui->initialAC->setSingleStep(1);                                                    //MHz
    ui->initialALowerLimitC->setValue(valueContainer->getInitialALimits().first);       //MHz
    ui->initialAUpperLimitC->setValue(valueContainer->getInitialALimits().second);      //MHz
    ui->initialALowerLimitC->setSingleStep(1);                                          //Mhz
    ui->initialAUpperLimitC->setSingleStep(1);                                          //MHz

    ui->initialBC->setValue(valueContainer->getInitialB());                             //MHz
    ui->initialBC->setSingleStep(1);                                                    //MHz
    ui->initialBLowerLimitC->setValue(valueContainer->getInitialBLimits().first);       //MHz
    ui->initialBUpperLimitC->setValue(valueContainer->getInitialBLimits().second);      //MHz
    ui->initialBLowerLimitC->setSingleStep(1);                                          //Mhz
    ui->initialBUpperLimitC->setSingleStep(1);                                          //MHz

    ui->finalAC->setValue(valueContainer->getFinalA());                                 //MHz
    ui->finalAC->setSingleStep(1);                                                      //MHz
    ui->finalALowerLimitC->setValue(valueContainer->getFinalALimits().first);           //MHz
    ui->finalAUpperLimitC->setValue(valueContainer->getFinalALimits().second);          //MHz
    ui->finalALowerLimitC->setSingleStep(1);                                            //Mhz
    ui->finalAUpperLimitC->setSingleStep(1);                                            //MHz

    ui->finalBC->setValue(valueContainer->getFinalB());                                 //MHz
    ui->finalBC->setSingleStep(1);                                                      //MHz
    ui->finalBLowerLimitC->setValue(valueContainer->getFinalBLimits().first);           //MHz
    ui->finalBUpperLimitC->setValue(valueContainer->getFinalBLimits().second);          //MHz
    ui->finalBLowerLimitC->setSingleStep(1);                                            //Mhz
    ui->finalBUpperLimitC->setSingleStep(1);                                            //MHz

    ui->CoGC->setValue(valueContainer->getCoG());                                       //PHz
    ui->CoGC->setSingleStep(1E-9);                                                      //PHz
    ui->CoGC->setMinimum(-1E-30);                                                       //PHz
    ui->CoGC->setMaximum(1E30);                                                         //PHz
    ui->CoGLowerLimitC->setValue(valueContainer->getCoGLimits().first);                 //PHz
    ui->CoGUpperLimitC->setValue(valueContainer->getCoGLimits().second);                //PHz
    ui->CoGLowerLimitC->setSingleStep(1E-9);                                            //Phz
    ui->CoGUpperLimitC->setSingleStep(1E-9);                                            //PHz

    ui->model_laserIntensityC->setValue(valueContainer->getModel_laserIntensity());                 //microJoules per cm^2
    ui->model_laserIntensityC->setSingleStep(1);                                                      //PHz
    ui->model_laserIntensityC->setMinimum(-1E-30);                                                       //PHz
    ui->model_laserIntensityC->setMaximum(1E30);                                                         //PHz
    ui->model_laserIntensityLowerLimitC->setValue(valueContainer->getModel_laserIntensityLimits().first);                 //PHz
    ui->model_laserIntensityUpperLimitC->setValue(valueContainer->getModel_laserIntensityLimits().second);                //PHz
    ui->model_laserIntensityLowerLimitC->setSingleStep(1E-9);                                            //Phz
    ui->model_laserIntensityUpperLimitC->setSingleStep(1E-9);                                            //PHz

    ui->spontTransC->setValue(valueContainer->getACoefficient_1());                          //Einstein coefficients
    ui->initialJC->setValue(valueContainer->getInitialJ());                             //hbar
    ui->finalJC->setValue(valueContainer->getfinalJ());                                 //hbar
    ui->NucSpinC->setValue(valueContainer->getNucSpin());                               //hbar

    ui->gaussianWidthC->setValue(valueContainer->getGaussianWidth());                   //MHz
    ui->gaussianWidthLowerLimitC->setValue(valueContainer->getGaussianWidthLimits().first);//MHz
    ui->gaussianWidthUpperLimitC->setValue(valueContainer->getGaussianWidthLimits().second);//MHz
    ui->gaussianWidthLowerLimitC->setSingleStep(1);                                      //Mhz
    ui->gaussianWidthUpperLimitC->setSingleStep(1);                                      //MHz

    ui->lorentzianWidthC->setValue(valueContainer->getLorentzianWidth());               //MHz
    ui->lorentzianWidthLowerLimitC->setValue(valueContainer->getLorentzianWidthLimits().first);//MHz
    ui->lorentzianWidthUpperLimitC->setValue(valueContainer->getLorentzianWidthLimits().second);//MHz
    ui->lorentzianWidthLowerLimitC->setSingleStep(1);                                      //Mhz
    ui->lorentzianWidthUpperLimitC->setSingleStep(1);                                      //MHz

    ui->FWHMC->setValue(valueContainer->getVoigtFWHM());                                //MHz
    ui->fanoParameterC->setValue(valueContainer->getFanoParameter());
    ui->asymmetryParameterC->setValue(valueContainer->getAsymmetryParameter());

    ui->pulseWidthC->setValue(valueContainer->getLaserPulseWidth());                    //ns

    ui->intensityScalerC->setValue(valueContainer->getIntensityScaler());               //arb.
    ui->intensityScalerC->setRange(0.0, 1E9);                                     //arb.
    ui->intensityScalerC->setSingleStep(0.1);                                           //arb
    ui->intensityScalerLowerLimitC->setValue(valueContainer->getIntensityScalerLimits().first);//arb.
    ui->intensityScalerUpperLimitC->setValue(valueContainer->getIntensityScalerLimits().second);//arb.
    ui->intensityScalerLowerLimitC->setSingleStep(1);                                      //arb.
    ui->intensityScalerUpperLimitC->setSingleStep(1);                                      //arb.

    ui->backgroundScalerC->setValue(valueContainer->getBackgroundScaler());             //arb.
    ui->backgroundScalerC->setRange(-9999999999999999, 9999999999999999);                             //arb.
    ui->backgroundScalerC->setSingleStep(0.1);                                          //arb.
    ui->backgroundScalerLowerLimitC->setValue(valueContainer->getBackgroundScalerLimits().first);//arb.
    ui->backgroundScalerUpperLimitC->setValue(valueContainer->getBackgroundScalerLimits().second);//arb.
    ui->backgroundScalerLowerLimitC->setSingleStep(1);                                      //arb.

    ui->model_intensityScalerC->setValue(valueContainer->getModel_intensityScaler());               //arb.
    ui->model_intensityScalerC->setRange(0.0, 1E20);                                     //arb.
    ui->model_intensityScalerC->setSingleStep(0.1);                                           //arb
    ui->model_intensityScalerLowerLimitC->setValue(valueContainer->getModel_intensityScalerLimits().first);//arb.
    ui->model_intensityScalerUpperLimitC->setValue(valueContainer->getModel_intensityScalerLimits().second);//arb.
    ui->model_intensityScalerLowerLimitC->setSingleStep(1);                                      //arb.
    ui->model_intensityScalerUpperLimitC->setSingleStep(1);                                      //arb.

    ui->model_backgroundScalerC->setValue(valueContainer->getModel_backgroundScaler());             //arb.
    ui->model_backgroundScalerC->setRange(-9999999999999999, 9999999999999999);                             //arb.
    ui->model_backgroundScalerC->setSingleStep(0.1);                                          //arb.
    ui->model_backgroundScalerLowerLimitC->setValue(valueContainer->getModel_backgroundScalerLimits().first);//arb.
    ui->model_backgroundScalerUpperLimitC->setValue(valueContainer->getModel_backgroundScalerLimits().second);//arb.
    ui->model_backgroundScalerLowerLimitC->setSingleStep(1);                                      //arb.

    ui->enforceLimitsB->setChecked(valueContainer->getEnforceLimits());
    ui->freeIntensitiesB->setChecked(valueContainer->getFreeIntensities());
    ui->ftolC->setValue(valueContainer->getFtol());
    ui->ftolC->setMaximum(1E60);
    ui->ftolC->setMinimum(1E-60);
    ui->xtolC->setValue(valueContainer->getXtol());
    ui->xtolC->setMaximum(1E60);
    ui->xtolC->setMinimum(1E-60);
    ui->maxIterationsC->setValue(valueContainer->getMaxIter());

    //Ionizing step default values
    ui->thirdLaserWLC->setValue(valueContainer->getWl_nr());
    ui->thirdLaserpulseWidthC->setValue(valueContainer->getPW_nr());
    ui->thirdLaserIntensityC->setValue(valueContainer->getI_nr());
    ui->nonResonantCrossSectionC->setValue(valueContainer->getCS_nr());
    ui->thirdLaserGaussianWidthC->setValue(valueContainer->getGW_nr());
    ui->thirdLaserLorentzianWidthC->setValue(valueContainer->getLW_nr());
    ui->thirdLaserTimingOffsetC->setValue(valueContainer->getTimeOffset());
    ui->isNonresoanntB->setChecked(valueContainer->getIsNonResonant());


    //set default lock states
    ui->AiLock->setChecked(valueContainer->getInitialALock());
    ui->BiLock->setChecked(valueContainer->getInitialBLock());
    ui->AfLock->setChecked(valueContainer->getFinalALock());
    ui->BfLock->setChecked(valueContainer->getFinalBLock());
    ui->COGLock->setChecked(valueContainer->getCoGLock());

    ui->GwLock->setChecked(valueContainer->getGaussianWidthLock());
    ui->LwLock->setChecked(valueContainer->getLorentzianWidthLock());

    ui->model_backgroundLockRb->setChecked(valueContainer->getModel_backgroundScalerLock());
    ui->model_intensityLockRb->setChecked(valueContainer->getModel_intensityScalerLock());
    ui->model_laserIntensityLockRb->setChecked(valueContainer->getModel_laserIntensityLock());

    //ui->solveRateqsB->setChecked(valueContainer->getSolveRateEquationsState());
    ui->singlePeakB->setChecked(valueContainer->getShowIndividualPeaksState());

    //set default IDA parameters
    ui->sundialsIntergationTimeScalerC->setValue(valueContainer->getSundials_integrationTimeScaler());
    ui->sundialsAbsoluteToleranceC->setValue(valueContainer->getSundials_absoluteTolerance());
    ui->sundialsRelativeToleranceC->setValue(valueContainer->getSundials_relativeTolerance());
    ui->sundialsCovCoefC->setValue(valueContainer->getSundials_nlConvCoeff());
    ui->sundialsMaxIterC->setValue(valueContainer->getSundials_maximumNumberOfIntegrationStep());
    ui->sundialsUseJacobianRB->setChecked(valueContainer->getSundials_useJacobian());
    ui->sundialsConstrainRB->setChecked(valueContainer->getSundials_useConstraints());
    ui->sundialsProgess->setValue(0);
    ui->sundialsTimePointsC->setValue(valueContainer->getSundials_TimePoints());

    //Set default model state
    ui->isSpontanouesEmissionOn->setChecked(valueContainer->getModel_spontaneousEmissionOn());
    ui->initialPopulationScalerC->setValue(valueContainer->getModel_initialPopulationScaler());
    ui->initialTemperatureC->setValue(valueContainer->getModel_initialPopulationTemperature());

    //Connect ui slots
    connect(ui->actionExit, &QAction::triggered, this, &ionizationModel::close);
    connect(ui->actionSave_Plot,&QAction::triggered, hfsPlot, &mainPlot::savePlot);
    connect(ui->actionOpen_Plot,&QAction::triggered, openSpec, &openSpectrumDialog::showUpUi);
    //connect(ui->actionSave_Data,&QAction::triggered, this,&ionizationModel::saveDataToFile);

    //Connect UI dataSlots
    //Get changes from the Uiainittest(
    //connect(ui->initialAC, SIGNAL(valueChanged(double)), this, SLOT(ainittest(double)));
    //connect(ui->initialAC, SIGNAL(valueChanged(double)),this, &ionizationModel::ainittest);

    connect(ui->initialAC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setInitialA(double)));
    connect(ui->initialALowerLimitC, SIGNAL(valueChanged(double)), this, SLOT(setInitialALimits()));
    connect(ui->initialAUpperLimitC, SIGNAL(valueChanged(double)), this, SLOT(setInitialALimits()));
    connect(this, &ionizationModel::initialALimitsChanged, valueContainer, &valueContainerClass::setInitialALimits);

    connect(ui->initialBC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setInitialB(double)));
    connect(ui->initialBLowerLimitC, SIGNAL(valueChanged(double)), this, SLOT(setInitialBLimits()));
    connect(ui->initialBUpperLimitC, SIGNAL(valueChanged(double)), this, SLOT(setInitialBLimits()));
    connect(this, &ionizationModel::initialBLimitsChanged, valueContainer, &valueContainerClass::setInitialBLimits);

    connect(ui->finalAC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setFinalA(double)));
    connect(ui->finalALowerLimitC, SIGNAL(valueChanged(double)), this, SLOT(setFinalALimits()));
    connect(ui->finalAUpperLimitC, SIGNAL(valueChanged(double)), this, SLOT(setFinalALimits()));
    connect(this, &ionizationModel::finalALimitsChanged, valueContainer, &valueContainerClass::setFinalALimits);

    connect(ui->finalBC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setFinalB(double)));
    connect(ui->finalBLowerLimitC, SIGNAL(valueChanged(double)), this, SLOT(setFinalBLimits()));
    connect(ui->finalBUpperLimitC, SIGNAL(valueChanged(double)), this, SLOT(setFinalBLimits()));
    connect(this, &ionizationModel::finalBLimitsChanged, valueContainer, &valueContainerClass::setFinalBLimits);

    connect(ui->CoGC,static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), valueContainer, &valueContainerClass::setCoG);
    connect(ui->CoGLowerLimitC, SIGNAL(valueChanged(double)), this, SLOT(setCoGLimits()));
    connect(ui->CoGUpperLimitC, SIGNAL(valueChanged(double)), this,SLOT(setCoGLimits()));
    connect(this, &ionizationModel::CoGLimitsChanged, valueContainer, &valueContainerClass::setCoGLimits);

    connect(ui->model_laserIntensityC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setModel_laserIntensity(double)));
    connect(ui->model_laserIntensityLowerLimitC, SIGNAL(valueChanged(double)), this, SLOT(setModel_laserIntensityLimits()));
    connect(ui->model_laserIntensityUpperLimitC, SIGNAL(valueChanged(double)), this, SLOT(setModel_laserIntensityLimits()));
    connect(this, &ionizationModel::model_laserIntensityLimitsChanged, valueContainer, &valueContainerClass::setModel_laserIntensityLimits);

    connect(ui->spontTransC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setACoefficient_1(double)));
    connect(ui->initialJC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setInitialJ(double)));
    connect(ui->finalJC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setFinalJ(double)));
    connect(ui->NucSpinC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setNucSpin(double)));

    connect(ui->gaussianWidthC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setGaussianWidth(double)));
    connect(ui->gaussianWidthLowerLimitC,SIGNAL(valueChanged(double)), this, SLOT(setGaussianWidthLimits()));
    connect(ui->gaussianWidthUpperLimitC,SIGNAL(valueChanged(double)), this, SLOT(setGaussianWidthLimits()));
    connect(this, &ionizationModel::gaussianWidthLimitsChanged, valueContainer, &valueContainerClass::setGaussianWidthLimits);

    connect(ui->lorentzianWidthC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setLorentzianWidth(double)));
    connect(ui->lorentzianWidthLowerLimitC,SIGNAL(valueChanged(double)), this, SLOT(setLorentzianWidthLimits()));
    connect(ui->lorentzianWidthUpperLimitC,SIGNAL(valueChanged(double)), this, SLOT(setLorentzianWidthLimits()));
    connect(this, &ionizationModel::lorentzianWidthLimitsChanged, valueContainer, &valueContainerClass::setLorentzianWidthLimits);

    connect(ui->asymmetryParameterC,SIGNAL(valueChanged(double)), valueContainer, SLOT(setAsymmetryParameter(double)));
    connect(ui->fanoParameterC,SIGNAL(valueChanged(double)), valueContainer, SLOT(setFanoParameter(double)));

    connect(ui->pulseWidthC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setLaserPulseWidth(double)));

    connect(ui->intensityScalerC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setIntensityScaler(double)));
    connect(ui->intensityScalerLowerLimitC, SIGNAL(valueChanged(double)), this, SLOT(setIntensityScalerLimits()));
    connect(ui->intensityScalerUpperLimitC, SIGNAL(valueChanged(double)), this, SLOT(setIntensityScalerLimits()));
    connect(this, &ionizationModel::intensityScalerLimitsChanged, valueContainer, &valueContainerClass::setIntensityScalerLimits);

    connect(ui->backgroundScalerC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setBackgroundScaler(double)));
    connect(ui->backgroundScalerLowerLimitC, SIGNAL(valueChanged(double)), this, SLOT(setBackgroundScalerLimits()));
    connect(ui->backgroundScalerUpperLimitC, SIGNAL(valueChanged(double)), this, SLOT(setBackgroundScalerLimits()));
    connect(this, &ionizationModel::backgroundScalerLimitsChanged, valueContainer, &valueContainerClass::setBackgroundScalerLimits);

    connect(ui->model_intensityScalerC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setModel_intensityScaler(double)));
    connect(ui->model_intensityScalerLowerLimitC, SIGNAL(valueChanged(double)), this, SLOT(setModel_intensityScalerLimits()));
    connect(ui->model_intensityScalerUpperLimitC, SIGNAL(valueChanged(double)), this, SLOT(setModel_intensityScalerLimits()));
    connect(this, &ionizationModel::model_intensityScalerLimitsChanged, valueContainer, &valueContainerClass::setModel_intensityScalerLimits);

    connect(ui->model_backgroundScalerC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setModel_backgroundScaler(double)));
    connect(ui->model_backgroundScalerLowerLimitC, SIGNAL(valueChanged(double)), this, SLOT(setModel_backgroundScalerLimits()));
    connect(ui->model_backgroundScalerUpperLimitC, SIGNAL(valueChanged(double)), this, SLOT(setModel_backgroundScalerLimits()));
    connect(this, &ionizationModel::model_backgroundScalerLimitsChanged, valueContainer, &valueContainerClass::setModel_backgroundScalerLimits);

    connect(ui->aRatioC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setAfAiRatio(double)));
    connect(ui->bRatioC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setBfBiRatio(double)));
    //Set Ui values
    connect(valueContainer, &valueContainerClass::initialAValueChanged, ui->initialAC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::initialBValueChanged, ui->initialBC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::finalAValueChanged, ui->finalAC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::finalBValueChanged, ui->finalBC, &QwtCounter::setValue);

    connect(valueContainer, &valueContainerClass::CoGValueChanged, ui->CoGC, &QDoubleSpinBox::setValue);
    connect(valueContainer, &valueContainerClass::laserIntensityValueChanged, ui->model_laserIntensityC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::transRateValueChanged, ui->spontTransC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::initialJValueChanged, ui->initialJC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::finalJValueChanged, ui->finalJC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::nucSpinValueChanged, ui->NucSpinC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::gaussianWidthValueChanged, ui->gaussianWidthC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::lorentzianWidthValueChanged, ui->lorentzianWidthC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::laserPulseWidthValueChanged, ui->pulseWidthC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::intensityScalerValueChanged, ui->intensityScalerC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::backgroundScalerValueChanged, ui->backgroundScalerC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::model_intensityScalerValueChanged, ui->model_intensityScalerC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::model_backgroundScalerValueChanged, ui->model_backgroundScalerC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::model_laserIntensityValueChanged, ui->model_laserIntensityC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::voigtFWHMValueChanged, ui->FWHMC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::afAiRatioValueChanged, ui->aRatioC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::bfBiRatioValueChanged, ui->bRatioC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::logValueChanged, ui->log, &QPlainTextEdit::appendPlainText);

    //connect locks to container
    connect(ui->AiLock, &QRadioButton::toggled, valueContainer, &valueContainerClass::setInitialALock);
    connect(ui->BiLock, &QRadioButton::toggled, valueContainer, &valueContainerClass::setInitialBLock);
    connect(ui->AfLock, &QRadioButton::toggled, valueContainer, &valueContainerClass::setFinalALock);
    connect(ui->BfLock, &QRadioButton::toggled, valueContainer, &valueContainerClass::setFinalBLock);
    connect(ui->COGLock,  &QRadioButton::toggled, valueContainer, &valueContainerClass::setCoGLock);
    connect(ui->GwLock, &QRadioButton::toggled, valueContainer, &valueContainerClass::setGaussianWidthLock);
    connect(ui->LwLock, &QRadioButton::toggled, valueContainer, &valueContainerClass::setLorentzianWidthLock);
    connect(ui->ILock, &QRadioButton::toggled, valueContainer, &valueContainerClass::setIntensityScalerLock);
    connect(ui->BGLock, &QRadioButton::toggled, valueContainer, &valueContainerClass::setBackgroundScalerLock);
    connect(ui->model_intensityLockRb, &QRadioButton::toggled, valueContainer, &valueContainerClass::setModel_intensityScalerLock);
    connect(ui->model_backgroundLockRb, &QRadioButton::toggled, valueContainer, &valueContainerClass::setModel_backgroundScalerLock);
    connect(ui->aRatioB, &QRadioButton::toggled, valueContainer, &valueContainerClass::setAfAiRatioLock);
    connect(ui->bRatioB, &QRadioButton::toggled, valueContainer, &valueContainerClass::setBfBiRatioLock);
    connect(ui->model_laserIntensityLockRb, &QRadioButton::toggled, valueContainer, &valueContainerClass::setModel_laserIntensityLock);

    //Connect fitter control slots
    connect(ui->enforceLimitsB, &QRadioButton::toggled, valueContainer, &valueContainerClass::setEnforceLimits);
    connect(ui->freeIntensitiesB, &QRadioButton::toggled, valueContainer, &valueContainerClass::setFreeIntensities);
    connect(ui->ftolC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setFtol(double)));
    connect(ui->xtolC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setXtol(double)));
    connect(ui->maxIterationsC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setMaxIter(double)));

    //Connect fitter input slots
    connect(hyperFine,&hyperfineCalculations::emitHashOrder, valueContainer, &valueContainerClass::setHashOrder);

    //Connect misc slots
    connect(ui->singlePeakB, &QPushButton::toggled, valueContainer, &valueContainerClass::setShowIndividualPeaks);

    //Connect slots related to opening experimental data
    connect(openSpec, &openSpectrumDialog::outputScanSet, valueContainer ,&valueContainerClass::setMeasuredProfile);
    connect(valueContainer, &valueContainerClass::measuredProfileChanged, hfsPlot, &mainPlot::setMeasuredProfilePair);
    connect(ui->clearSpectraB, &QPushButton::clicked, this, &ionizationModel::clearSpectraClicked);

    //The calculation of the hyperfine profile goes as follows:
    //1. If the spins are changed, the program emits a call which is connected to a slot
    //(&hyperfineCalculations::calculateHyperfineTransitions)
    //which initiates the calculation of the hyperfine transitions. This includes freqeuncies and the
    //relative intensities for the allowed transitions.
    //2. A(&hyperfineCalculations::calculationsDone) signal is emitted whichis connected to a slot ( &valueContainerClass::setLog)
    //on the valueContainer to store a log about the hyperfine calculations.
    //3. After the Signals from 1, a (readyToCalculate) -signal is emitted. It prompts the parsing (&hyperfineCalculations::parseHyperfineParameters)
    //of the results from the hyperfine calculations, and the storage of those to the valueContainer. The results are emitted to
    // the valuecontainer class via signals in the hyperfineCalculations class.
    //4. Once the parsing and storing has been finished, a signal(&hyperfineCalculations::parsingDone) is emitted
    //which initiates the calculation of the relative hyperfine profile via slot (&relativeProfile::calculateProfile).

    //Connect hyperFine slots,  hyperfine calculations need to be  done only when the spins change;
    connect(valueContainer, &valueContainerClass::initialJValueChanged, hyperFine, &hyperfineCalculations::calculateHyperfineTransitions);
    connect(valueContainer, &valueContainerClass::finalJValueChanged, hyperFine, &hyperfineCalculations::calculateHyperfineTransitions);
    connect(valueContainer, &valueContainerClass::nucSpinValueChanged, hyperFine, &hyperfineCalculations::calculateHyperfineTransitions);
    connect(hyperFine, &hyperfineCalculations::calculationsDone,valueContainer, &valueContainerClass::setLog);

    //Connect relativeProfile slots, a new relative profile is calculated each time a numerical ui value is changed.
    connect(valueContainer, &valueContainerClass::readyToCalculate, hyperFine, &hyperfineCalculations::parseHyperfineParameters);
    connect(hyperFine, &hyperfineCalculations::parsingDone, relProfile, &relativeProfile::calculateProfile);
    connect(valueContainer, &valueContainerClass::showIndividualPeaksChanged,relProfile, &relativeProfile::calculateSinglePeaks);
    connect(relProfile, &relativeProfile::fullRelativeProfileReady, valueContainer, &valueContainerClass::setFullRelativeProfile);
    connect(relProfile, &relativeProfile::singleRelativePeaksReady, valueContainer, &valueContainerClass::setSingleRelativePeaks);
    connect(valueContainer, &valueContainerClass::relativeProfileChanged, hfsPlot,&mainPlot::setFullRelativeProfilePair);
    connect(valueContainer, &valueContainerClass::singleRelativePeaksChanged, hfsPlot,&mainPlot::setSingleRelativePeakPairs);

    ////////////////////////////////////////////////
    //IDAS
    ////////////////////////////////////////////////
    connect(ui->runIdas, &QPushButton::clicked, rateqProfile, &rateEquationsProfile::solveRateEquations_IDAS);

    ////////////////////////////////////////////////
    //CVODES
    ////////////////////////////////////////////////
    connect(ui->runCvodes, &QPushButton::clicked, rateqProfile, &rateEquationsProfile::solveRateEquations_CVODES);

    ////////////////////////////////////////////////
    //Connect SUNDIALS parameter slots
    ////////////////////////////////////////////////
    connect(ui->sundialsIntergationTimeScalerC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setSundials_integrationTimeScaler(double)));
    connect(ui->sundialsAbsoluteToleranceC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setSundials_absoluteTolerance(double)));
    connect(ui->sundialsRelativeToleranceC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setSundials_relativeTolerance(double)));
    connect(ui->sundialsCovCoefC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setSundials_nlConvCoeff(double)));
    connect(ui->sundialsMaxIterC, SIGNAL(valueChanged(double)), valueContainer,SLOT(setSundials_maximumNumberOfIntegrationStep(double)));
    connect(ui->sundialsUseJacobianRB, &QPushButton::toggled, valueContainer, &valueContainerClass::setSundials_userJacobian);
    connect(ui->sundialsConstrainRB, &QPushButton::toggled, valueContainer, &valueContainerClass::setSundials_useConstraints);
    connect(ui->sundialsTimePointsC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setSundials_TimePoints(double)));

    ////////////////////////////////////////////////
    //Connect SUNDIALS plotting slots
    ////////////////////////////////////////////////
    connect(rateqProfile, &rateEquationsProfile::rateEquationsSolved_SUNDIALS, this, &ionizationModel::statesReadyForPlotting);
    connect(rateqProfile, &rateEquationsProfile::plotDataReady_SUNDIALS, valueContainer, &valueContainerClass::setRateEquationProfile);
    connect(valueContainer, &valueContainerClass::rateEquationPofileChanged, hfsPlot, &mainPlot::setRateEquationsProfilePair);
    connect(ui->sundialsTimeS, SIGNAL(valueChanged(double)), valueContainer, SLOT(setSundials_TimeOrFrequencyPointToBePlotted(double)));
    connect(ui->plotStatePopulationSelectorCB, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), valueContainer, &valueContainerClass::setSundials_StateToBePlotted);
    connect(ui->plotAsAFunctionOfTimeB,&QPushButton::toggled, valueContainer, &valueContainerClass::setSundials_PlotAsAFunctionOfTime);
    connect(ui->plotAsAFunctionOfTimeB,&QPushButton::toggled, rateqProfile, &rateEquationsProfile::plot_SUNDIALS);
    connect(ui->plotAsAFunctionOfTimeB,&QPushButton::toggled, this, &ionizationModel::IDASplotAsAFunctionOfTIme);
    connect(valueContainer, &valueContainerClass::sundials_maximumTimeOrFrequencyIndexChanged, ui->sundialsTimeS, &QwtSlider::setUpperBound);
    connect(valueContainer, &valueContainerClass::sundials_CurrentTimeOrFrequencyValueChanged, ui->sundialsTimeC, &QwtCounter::setValue);
    connect(valueContainer, &valueContainerClass::intergationStepsPerFrequencyPointChanged, diagnosticsPlot, &mainPlot::setNumberOfIntegrationStepsPlot);
    connect(ui->plotStatePopulationSelectorCB,static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), rateqProfile, &rateEquationsProfile::plot_SUNDIALS);
    connect(ui->sundialsTimeS, SIGNAL(valueChanged(double)), rateqProfile, SLOT(plot_SUNDIALS()));
    connect(ui->hideCalculatedSpectrumB, &QPushButton::clicked, hfsPlot, &mainPlot::hideCalculatedSpectra);
    connect(rateqProfile, &rateEquationsProfile::plotAsAFunctionOfTime,ui->sundialsTimeL, &QLabel::setText);

    ////////////////////////////////////////////////
    //Connect SUNDIALS other slots
    ////////////////////////////////////////////////
    connect(ui->sundialsClear, &QPushButton::clicked, rateqProfile, &rateEquationsProfile::clearRateProfile);
    connect(rateqProfile, &rateEquationsProfile::solverProgress_SUNDIALS, ui->sundialsProgess, &QProgressBar::setValue);

    ////////////////////////////////////////////////
    //Connect second, and ionizing transition parameter slots
    ////////////////////////////////////////////////
    connect(ui->thirdLaserWLC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setWl_nr(double)));
    connect(ui->thirdLaserpulseWidthC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setPW_nr(double)));
    connect(ui->thirdLaserIntensityC, SIGNAL(valueChanged(double)), valueContainer,SLOT(setI_nr(double)));
    connect(ui->nonResonantCrossSectionC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setCS_nr(double)));
    /////////////////////
    //Ionizing transition
    /////////////////////
    connect(ui->thirdLaserGaussianWidthC, SIGNAL(valueChanged(double)), valueContainer,SLOT(setGW_nr(double)));
    connect(ui->thirdLaserLorentzianWidthC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setLW_nr(double)));
    connect(ui->thirdLaserTimingOffsetC, SIGNAL(valueChanged(double)), valueContainer, SLOT(setTimeOffset(double)));
    connect(ui->isNonresoanntB, &QRadioButton::toggled, valueContainer, &valueContainerClass::setIsNonResonant);

    ////////////////////////////////////////////////
    //Connect model control slots
    ////////////////////////////////////////////////
    connect(ui->isSpontanouesEmissionOn, &QRadioButton::toggled, valueContainer, &valueContainerClass::setModel_spontanousEmissionOn);
    connect(ui->initialPopulationScalerC, SIGNAL(valueChanged(double)),valueContainer, SLOT(setModel_initialPopulationScaler(double)));
    connect(ui->initialTemperatureC, SIGNAL(valueChanged(double)),valueContainer, SLOT(setModelInitial_populationTemperature(double)));

    ////////////////////////////////////////////////
    ////////////////////////////////////////////////

    ///////////////////////////
    //Connect slots for fitting
    ///////////////////////////
    connect(ui->fitRelB, &QPushButton::clicked, relProfile, &relativeProfile::fitRelativeProfile);
    connect(ui->fitRateB, &QPushButton::clicked, this, &ionizationModel::runRateEquationsFitter);
    ////////////////////////////////////////////////
    //Connect slots for storing hyperfine parameters
    /////////////////////////////////////////////////
    connect(hyperFine, &hyperfineCalculations::emitRelativeTransitionIntensities, valueContainer, &valueContainerClass::setRelativeTransitionIntensities);
    connect(hyperFine, &hyperfineCalculations::emitRelativeTransitionFrequencies, valueContainer, &valueContainerClass::setRelativeTransitionFrequencies);

    //Saving Data
    connect(ui->actionSave_Data, &QAction::triggered, this, &ionizationModel::saveData);

    //first run:
    emit valueContainer->initialJValueChanged(0.5);
    emit valueContainer->readyToCalculate();
    if(testMode){
        openSpec->testModeFileOpen();
    }

}

ionizationModel::~ionizationModel()
{
    //delete ui;
    //delete valueContainer;
}

//Keypresses:
void ionizationModel::keyPressEvent(QKeyEvent *event ){
    if(event->key()==Qt::Key_Escape){
        hfsPlot->resetView();
    }
}


//Private slots

void ionizationModel::setInitialALimits(){
    double lower=ui->initialALowerLimitC->value();
    double upper=ui->initialAUpperLimitC->value();
    std::pair<double, double> limits;
    if(lower<=upper){
        limits.first=lower;
        limits.second=upper;
    }
    else{
        limits.first=upper;
        limits.second=lower;
    }
    emit initialALimitsChanged(limits);
}

void ionizationModel::setInitialBLimits(){
    double upper=ui->initialBLowerLimitC->value();
    double lower=ui->initialBUpperLimitC->value();
    std::pair<double, double> limits;
    if(lower<=upper){
        limits.first=lower;
        limits.second=upper;
    }
    else{
        limits.first=upper;
        limits.second=lower;
    }
    qDebug()<<"initialblimits";
    emit initialBLimitsChanged(limits);
}

void ionizationModel::setFinalALimits(){
    double upper=ui->finalALowerLimitC->value();
    double lower=ui->finalAUpperLimitC->value();
    std::pair<double, double> limits;
    if(lower<=upper){
        limits.first=lower;
        limits.second=upper;
    }
    else{
        limits.first=upper;
        limits.second=lower;
    }
    emit finalALimitsChanged(limits);
}

void ionizationModel::setFinalBLimits(){
    double upper=ui->finalBLowerLimitC->value();
    double lower=ui->finalBUpperLimitC->value();
    std::pair<double, double> limits;
    limits.first=lower;
    limits.second=upper;
    emit finalBLimitsChanged(limits);
}

void ionizationModel::setCoGLimits(){
    double upper=ui->CoGLowerLimitC->value();
    double lower=ui->CoGUpperLimitC->value();
    std::pair<double, double> limits;
    if(lower<=upper){
        limits.first=lower;
        limits.second=upper;
    }
    else{
        limits.first=upper;
        limits.second=lower;
    }
    emit CoGLimitsChanged(limits);
}

void ionizationModel::setGaussianWidthLimits(){
    double upper=ui->gaussianWidthLowerLimitC->value();
    double lower=ui->gaussianWidthUpperLimitC->value();
    std::pair<double, double> limits;
    if(lower<=upper){
        limits.first=lower;
        limits.second=upper;
    }
    else{
        limits.first=upper;
        limits.second=lower;
    }
    emit gaussianWidthLimitsChanged(limits);
}

void ionizationModel::setLorentzianWidthLimits(){
    double upper=ui->lorentzianWidthLowerLimitC->value();
    double lower=ui->lorentzianWidthUpperLimitC->value();
    std::pair<double, double> limits;
    if(lower<=upper){
        limits.first=lower;
        limits.second=upper;
    }
    else{
        limits.first=upper;
        limits.second=lower;
    }
    emit lorentzianWidthLimitsChanged(limits);
}

void ionizationModel::setIntensityScalerLimits(){
    double upper=ui->intensityScalerLowerLimitC->value();
    double lower=ui->intensityScalerUpperLimitC->value();
    std::pair<double, double> limits;
    if(lower<=upper){
        limits.first=lower;
        limits.second=upper;
    }
    else{
        limits.first=upper;
        limits.second=lower;
    }
    emit intensityScalerLimitsChanged(limits);
}

void ionizationModel::setBackgroundScalerLimits(){
    double upper=ui->backgroundScalerLowerLimitC->value();
    double lower=ui->backgroundScalerUpperLimitC->value();
    std::pair<double, double> limits;
    if(lower<=upper){
        limits.first=lower;
        limits.second=upper;
    }
    else{
        limits.first=upper;
        limits.second=lower;
    }
    emit backgroundScalerLimitsChanged(limits);
}
void ionizationModel::setModel_intensityScalerLimits(){
    double upper=ui->model_intensityScalerLowerLimitC->value();
    double lower=ui->model_intensityScalerUpperLimitC->value();
    std::pair<double, double> limits;
    if(lower<=upper){
        limits.first=lower;
        limits.second=upper;
    }
    else{
        limits.first=upper;
        limits.second=lower;
    }
    emit model_intensityScalerLimitsChanged(limits);
}
void ionizationModel::setModel_backgroundScalerLimits(){
    double upper=ui->model_backgroundScalerLowerLimitC->value();
    double lower=ui->model_backgroundScalerUpperLimitC->value();
    std::pair<double, double> limits;
    if(lower<=upper){
        limits.first=lower;
        limits.second=upper;
    }
    else{
        limits.first=upper;
        limits.second=lower;
    }
    emit model_backgroundScalerLimitsChanged(limits);
}

void ionizationModel::setModel_laserIntensityLimits(){
    double upper=ui->model_laserIntensityC->value();
    double lower=ui->model_laserIntensityC->value();
    std::pair<double, double> limits;
    if(lower<=upper){
        limits.first=lower;
        limits.second=upper;
    }
    else{
        limits.first=upper;
        limits.second=lower;
    }
    emit  model_laserIntensityLimitsChanged(limits);
}

void ionizationModel::statesReadyForPlotting(int noOfInitialStates, int noOfFinalStates, int ionizingStateNo, int lastState){
    ui->plotStatePopulationSelectorCB->blockSignals(true);
    ui->plotStatePopulationSelectorCB->clear();
    int idx=0;
    int size=noOfInitialStates+noOfFinalStates;
    for(idx; idx<size; idx++){
        ui->plotStatePopulationSelectorCB->addItem(QString::number(idx), idx);
    }
    if(size<lastState){
        ui->plotStatePopulationSelectorCB->setCurrentIndex(ionizingStateNo);
    }
    else{
        ui->plotStatePopulationSelectorCB->setCurrentIndex(lastState);
    }
    ui->plotStatePopulationSelectorCB->blockSignals(false);
    ui->plotStatePopulationSelectorCB->addItem(QString::number(ionizingStateNo), ionizingStateNo);
    ui->plotStatePopulationSelectorCB->setCurrentIndex(ionizingStateNo);
}

void ionizationModel::IDASplotAsAFunctionOfTIme(bool plot){
    if(plot){
        ui->plotAsAFunctionOfTimeB->setText("Plot in frequency");
    }
    else{
        ui->plotAsAFunctionOfTimeB->setText("Plot in time");
    }
}

void ionizationModel::clearSpectraClicked(){
    QVector<double> null;
    null.clear();
    QPair<QVector<double>, QVector<double> > nullSpectra;
    nullSpectra.first=null;
    nullSpectra.second=null;
    valueContainer->setMeasuredProfile(nullSpectra);
}

void ionizationModel::runRateEquationsFitter(){
//    boost::timer::auto_cpu_timer t;
    if(!valueContainer->getMeasuredProfile().first.isEmpty()){
        rateFitter->fit();
    }
}
void ionizationModel::saveData(){
    std::vector<double> freqX=valueContainer->getFrequencyRangeStd();
    std::vector<double> relY=valueContainer->getFullRelativeProfile().second.toStdVector();
    std::vector<double> ratY=valueContainer->getRateEquationProfile().second.toStdVector();
    //std::vector<double> expY=valueContainer->getMeasuredProfileValues();
    int size=relY.size();
    std::string line;
    std::string path("dataOut.txt");
    std::ofstream outfile;
    std::cout<<size<<"asd"<<std::endl;

    outfile.open(path, std::ios_base::app);
    for(int idx=0;idx<size;idx++){
        line.append(std::to_string(freqX[idx]));
        line.append("\t");
        line.append(std::to_string(relY[idx]));
        line.append("\t");
        line.append(std::to_string(ratY[idx]));
        //line.append("\t");
        //line.append(std::to_string(expY[idx]));
        line.append("\n");
        //std::cout<<line<<std::endl;
        outfile << line;
    }

}

void ionizationModel::ainittest(double value){
    qDebug()<<value;
    qDebug()<<"jhasskjladwlkadslkj";

}

